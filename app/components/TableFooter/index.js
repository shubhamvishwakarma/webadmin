import React from 'react';
import Prev from 'images/prev.png';
import Next from 'images/next.png';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Action from 'images/icon-dropdown.svg';

class TableFooter extends React.PureComponent { //eslint-disable-line
  constructor(props) {
    super(props);
    this.state = {
      count: 10,
    };
    this.countChange = this.countChange.bind(this);
  }

  countChange(event, index, value) {
    this.setState({ count: value });
  }

  render() {
    const { nextPage, page } = this.props;
    return (
      <div className="row ">
        <div className="col-12 col-m-12 col-sm-12 tbl-wrapper">
          <div className="row table-footer">

            <div className="col-12 col-m-12 col-sm-12 rows-per-page-lbl pull-right" style={{ paddingRight: '8px' }}>
              <div className="inline">
                Rows per page
              </div>
              <div className="inline">
                <SelectField
                  className="inline-block"
                  value={this.state.count}
                  onChange={this.countChange}
                  style={{ width: '90px', marginTop: '-15px', color: 'red' }}
                  labelStyle={{ height: '35px', color: '#9b9b9b' }}
                  selectedMenuItemStyle={{ color: '#8dc640' }}
                  dropDownMenuProps={{
                    iconButton: <img alt="ic" src={Action} />,
                  }}
                >
                  <MenuItem value={10} primaryText="10" />
                  <MenuItem value={20} primaryText="20" />
                  <MenuItem value={50} primaryText="50" />
                  <MenuItem value={100} primaryText="100" />
                </SelectField>
              </div>
              <div className="inline">
                page: {page}
              </div>
              <div className="inline">
                <img alt="prev" className="table_icon menu_icon left" src={Prev} onTouchTap={() => nextPage('previous', this.state.count)} />
                <img alt="next" className="table_icon menu_icon" src={Next} onTouchTap={() => nextPage('next', this.state.count)} />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

TableFooter.propTypes = {
  nextPage: React.PropTypes.func,
  page: React.PropTypes.number,
};

export default TableFooter;
