import React from 'react';
// import { initialScript } from 'containers/Offerings/scripts';

class MobileSort extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      active: null,
    };
  }

  componentDidMount() {
    // initialScript();
  }

  sortItem(item, i) {
    this.setState({ active: i });
    this.props.sort(item);
  }

  render() {
    const { filter } = this.props;
    if (filter && filter.length > 0) {
      return (
        <div className="mobile_sort_section">
          {
            filter.map((item, i) => (
              <div className={this.state.active === i ? 'filter active' : 'filter'} key={i}>
                <div className="check_box inline">
                  <input
                    type="checkbox" className="check active_check" value="active" checked={this.state.active === i}
                    onChange={() => this.sortItem(item, i)}
                  />
                </div>
                <div className="filter_label inline">{item.title}</div>
              </div>
             ))
            }
        </div>
      );
    }
    return (null);
  }
}

MobileSort.propTypes = {
  filter: React.PropTypes.any,
  sort: React.PropTypes.func,
};

export default MobileSort;
