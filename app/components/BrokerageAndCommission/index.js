import React from 'react/lib/React';
import { Field } from 'redux-form/immutable';
import { TextField } from 'redux-form-material-ui';

class Brokerages extends React.Component { // eslint-disable-line

  validateCommission(value, preVal) {
    if (value > 1 || value.match(/[A-Za-z]/)) {
      return preVal;
    }
    return value;
  }

  brokerageList() {
    const { data } = this.props;
    if (data && data.length > 0) {
      return (
        data.map((item, i) => (
          <div className="row" key={i}>
            <div className="col-6">
              <div className="info-section">
                <div className="info-titles">{item.name}</div>
              </div>
            </div>
            <div className="col-6">
              <Field
                name={`brokerDealer.${item.broker_dealer_id}`}
                component={TextField}
                floatingLabelText="Commission"
                autoComplete="off"
                type="text"
                required
                normalize={this.validateCommission}
                hintText="%"
                fullWidth
                floatingLabelFocusStyle={{ color: '#8dc73f' }}
                underlineFocusStyle={{ borderColor: '#8dc73f' }}
                hintStyle={{ paddingLeft: '10px' }}
              />
            </div>
          </div>
        ))
      );
    } return (<h3 className="blue">No brokerages found</h3>);
  }

  render() {
    const { data } = this.props;
    if (data) {
      return (
        <div className="row">
          <div className="col-8 col-sm-12">
            <h3 className="green">Brokerages</h3>
            <div className="row">
              <div className="col-6"><h4 className="blue">Brokerage name</h4></div>
              <div className="col-6"><h4 className="blue">Commission percentage</h4></div>
            </div>
            {
            this.brokerageList()
            }
          </div>
        </div>
      );
    } return (null);
  }
}

Brokerages.propTypes = {
  data: React.PropTypes.any,
};


export default Brokerages;
