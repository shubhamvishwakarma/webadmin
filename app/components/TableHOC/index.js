import React from 'react';
import { orderBy } from 'lodash';
import SortableTable from 'components/SortableTable';
import TableFooter from 'components/TableFooter';

let urlId;
export default function TableHOC(InnerComponent, headers, tableClassName) {
  class EnhancedTable extends React.Component {
    constructor() {
      super();
      this.state = {
        data: [],
        edit: null,
        page: 1,
        showFooter: false,
      };
      this.sortAscending = this.sortAscending.bind(this);
      this.sortDescending = this.sortDescending.bind(this);
      this.nextPage = this.nextPage.bind(this);
    }

    componentWillMount() {
      if (window.location.pathname.includes('offering-detail')) {
        this.setState({ showFooter: true });
      }
    }

    componentDidMount() {
      if (this.props.orders) {
      this.setState({ data: this.props.orders }); // eslint-disable-line
      }
      const url = new URL(window.location.href);
      urlId = url.searchParams.get('id');
    }

    componentWillReceiveProps(newProps) {
      if (newProps.orders && newProps.orders.length > 0) {
        this.setState({ data: newProps.orders });
      }
      if (newProps.sort.type === 'asc') {
        this.sortAscending(newProps.sort.field);
      } else if (newProps.sort.type === 'desc') {
        this.sortDescending(newProps.sort.field);
      } else if (newProps.sort.type === 'reset') {
        this.reset();
      }
    }

    sortAscending(field) {
      this.setState({ data: orderBy(this.props.orders, [field], ['asc']) });
    }

    sortDescending(field) {
      this.setState({ data: orderBy(this.props.orders, [field], ['desc']) });
    }

    reset() {
      this.setState({ data: this.props.orders });
    }

    nextPage(dir, count) {
      if (dir === 'next') {
        const pageCount = this.state.page + 1;
        this.setState({ page: pageCount });
        this.props.getCustomers(urlId, 'nextpage', count, pageCount);
      } else if (dir === 'previous' && this.state.page !== 1) {
        const pageCount = this.state.page - 1;
        this.setState({ page: pageCount });
        this.props.getCustomers(urlId, 'nextpage', count, pageCount);
      }
    }

    render() {
      if (this.state.data && this.state.data.length > 0) {
        return (
          <div>
            <SortableTable
              headers={headers}
              tableClass={tableClassName}
              sortAscending={this.sortAscending}
              sortDescending={this.sortDescending}
            >
              <InnerComponent
                {...this.props}
                {...this.state}
              />
            </SortableTable>
            {this.state.showFooter &&
              <TableFooter nextPage={this.nextPage} page={this.state.page} />
            }
          </div>
        );
      } else if (this.state.data && this.state.data.length === 0) {
        return (
          <div className="name" style={{ paddingTop: '30px', paddingLeft: '20px' }}>
            No data found
          </div>
        );
      }
      return (null);
    }
}
  EnhancedTable.propTypes = InnerComponent.propTypes;

  return EnhancedTable;
}
