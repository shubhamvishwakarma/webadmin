import React from 'react';
import Dropzone from 'react-dropzone';
import RaisedButton from 'material-ui/RaisedButton';

const renderDropzoneInput = (field) => {
  const files = field.input.value;
  return (
    <div>
      {files && Array.isArray(files) && (
      <ul style={{ paddingTop: '35px' }}>
        { files.map((file, i) => <img alt="img" width="30%" key={i} src={file.preview} />) }
      </ul>
     )}
      <Dropzone
        name={field.name}
        accept="image/jpeg, image/png, image/jpg"
        multiple={false}
        onDrop={(filesToUpload) => { field.input.onChange(filesToUpload); }}
        style={{ width: 'auto', borderStyle: 'none', height: '10px' }}
      >
        <RaisedButton type="button" style={{ padding: '0px 10px' }}>
          {field.placeholder ? field.placeholder : 'Upload file'}
        </RaisedButton>
      </Dropzone>
      {field.meta.touched &&
        field.meta.error &&
        <span className="error">{field.meta.error}</span>}
      {files && Array.isArray(files) && (
        <ul style={{ paddingTop: '35px' }}>
          { files.map((file, i) => <li key={i}>{file.name}</li>) }
        </ul>
      )}
    </div>
  );
};

export default renderDropzoneInput;
