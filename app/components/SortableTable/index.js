import React from 'react';
import $ from 'jquery';
import Edit from 'images/edit.png';

class SortableTable extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor() {
    super();
    this.handleSort = this.handleSort.bind(this);
  }

  handleSort = (event, field, i) => {
    this.props.headers.forEach((data, index) => {
      if (index !== i) {
        if ($(`#${index}`).children().is('div.ascending')) {
          $(`#${index}`).children().removeClass('ascending');
        } else if ($(`#${index}`).children().is('div.descending')) {
          $(`#${index}`).children().removeClass('descending');
        }
      }
    });
    $(event.target).toggleClass(() => {
      if ($(event.target).is('div.ascending')) {
        $(event.target).removeClass('ascending');
        this.props.sortDescending(field);
        return 'descending';
      } else if ($(event.target).is('div')) {
        $(event.target).removeClass('descending');
        this.props.sortAscending(field);
        return 'ascending';
      }
      return '';
    });
  }

  render() {
    const { children, headers, tableClass } = this.props;
    if (headers) {
      return (
        <div className="row">
          <div className="col-12 col-m-12 col-sm-12 tbl-wrapper">
            <table className={`tbl ${tableClass}`}>
              <thead>
                <tr>
                  {
                    headers.map((item, i) => {
                      if (!item.editable) {
                        return (
                          <th key={i} id={i} className={item.class} onTouchTap={(event) => { this.handleSort(event, item.field, i); }}><div id={i}>{item.title}</div></th>
                        );
                      }
                      return (
                        <th key={i} id={i} className={item.class} onTouchTap={(event) => { this.handleSort(event, item.field, i); }}><div id={i}>{item.title}<span className="edit-header">editable<img alt="edit" className="table_icon_small" src={Edit} /></span></div></th>
                      );
                    })
                 }
                </tr>
              </thead>
              {
                  children
              }
            </table>
          </div>
        </div>
      );
    } return (null);
  }
}

SortableTable.propTypes = {
  headers: React.PropTypes.any,
  children: React.PropTypes.any,
  sortAscending: React.PropTypes.func,
  sortDescending: React.PropTypes.func,
  tableClass: React.PropTypes.string,
};

export default SortableTable;
