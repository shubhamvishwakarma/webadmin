import React from 'react';
import Helmet from 'react-helmet';
import { logout } from 'containers/App/Helpers';

const AdminWrapper = ({ children, title, header,search }) => (
  <div className="outer_wrapper">
    <Helmet
      title={title}
    />
    <div className="filter_section large-hidden offering_detail">

      <div className="logout_section">
        <div className="filter_head">ACCOUNT</div>
        <div className="filter_label inline" onTouchTap={logout}>Logout</div>
      </div>
    </div>

    <div className="content inline">
      <div className="top_bar">
        {header}
        <div className="menu_icon"></div>
        <div className="top_logo"></div>
        {(search)? search:<div></div>}
        <div className="user inline open_account">
          <div className="user_drop">
            <ul>
              <li className="filter_menu">Filter</li>
              <li onTouchTap={logout}>Logout</li>
            </ul>
          </div>
        </div>
      </div>
      {children}
    </div>
  </div>
);

AdminWrapper.propTypes = {
  children: React.PropTypes.any,
  title: React.PropTypes.string,
  header: React.PropTypes.any,
};

export default AdminWrapper;
