import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import Logo from 'images/logo.svg';
import { Link } from 'react-router-dom';

import { makeSelectCurrentUser } from 'containers/App/selectors';
import { logout } from 'containers/App/Helpers';
let page = 'offering';
class Header extends React.Component { //eslint-disable-line

  constructor(props) {
    super(props);
    this.state = {
      view: 'offering',
    };
  }


  activeView = (view) => {
    this.setState({ view });
  }

  render() {
    const { user } = this.props;
    if (window.location.pathname.includes('investor')) {
      page = 'investors';
    } else if (window.location.pathname.includes('offering')) {
      if (window.location.pathname.includes('admin_offerings')) {
        page = 'admin_offerings';
      } else {
        page = 'offering';
      }
    } else if (window.location.pathname.includes('users')) {
      page = 'users';
    } else if (window.location.pathname.includes('allocation')) {
      page = 'allocation';
    } else if (window.location.pathname.includes('admin_messages')) {
      page = 'messaging';
    }else if (window.location.pathname.includes('intact_file_upload')) {
      page = 'intact_file_upload';
    }else if (window.location.pathname.includes('add_new_admin')) {
      page = 'add_new_admin';
    }else if (window.location.pathname.includes('connectedcustomer')) {
      page = 'connectedcustomer';
    }

    return (
      <div  className={"inline "+((page === 'admin_offerings' || page === 'allocation' || page ==='add_new_admin') ? 'topMenuHeader' : 'side_bar')} style={{paddingTop: '0px'}}>
        {(page !== 'admin_offerings' && page !== 'allocation' && page !=='add_new_admin')&& <div>
          <div className="logo">
            <a href="/offerings">
              <img alt="click ipo" src={Logo} width="70%" />
            </a>
          </div>
          <div className="profile">
            <div className="profile_icon"></div>
          </div>
        </div>}
        {/* { user.role === 'broker_dealer' &&
          <div className="menu">
            <ul>
              <li className={page === 'offering' ? 'active' : 'no-class'}><a href="/offerings">Offerings</a></li>
            </ul>
            <ul>
              <li className={page === 'investors' ? 'active' : 'no-class'}><a href="/investors">Investors</a></li>
            </ul>
          </div>
        } */}
        { user.role === 'super_admin' &&
          <div>
          {(page === 'admin_offerings' || page === 'allocation' || page ==='add_new_admin') && <div >
            <div className="row" style={{padding:'0px 24px 0px'}}>
                <div className="col-sm-2">
                    <div className="logo">
                      <a href="/offerings">
                        <img alt="click ipo" src={Logo} />
                      </a>
                    </div>
                </div>
                <div className="col-sm-8 text_center" >
                    <div className="row" >
                        <ul className="topheaderMenu">
                          <li className={page === 'admin_offerings' ? 'active' : 'no-class'}><Link to="/admin_offerings">Offerings</Link></li>
                          <li className={page === 'allocation' ? 'active' : 'no-class'}><Link to="/allocations">Allocations</Link></li>
                          <li className={page === 'add_new_admin' ? 'active' : 'no-class'}>
                          <Link to="/add_new_admin">Admin</Link></li>
                          <li className={page === 'users' ? 'active' : 'no-class'}><Link to="/users">Users</Link></li>

                          <li  className={page === 'broker_dealer' ? 'active' : 'no-class'}> <Link to="/">Broker Dealer</Link></li>
                          <li  className={page === 'underwriter' ? 'active' : 'no-class'}>
                          <Link to="/"> Underwriter</Link></li>


                          {/*<li className={page === 'messaging' ? 'active' : 'no-class'}><Link to="/admin_messages">Messages</Link></li>
                          <li className={page === 'intact_file_upload' ? 'active' : 'no-class'}>
                            <Link to="/intact_file_upload">Intact File Upload</Link></li>

                          <li className={page === 'connectedcustomer' ? 'active' : 'no-class'}>
                            <Link to="/connectedcustomer">Connected Customers</Link></li>*/}
                        </ul>
                    </div>
                </div>
                <div className="col-sm-2">
                        <ul className="topheaderMenu pull-right" >
                          <li onTouchTap={logout}>Logout</li>
                        </ul>
                </div>
            </div>

          </div>}
            {(page !== 'admin_offerings' && page !== 'allocation' && page !=='add_new_admin') && <div className="menu">
              <ul>
                <li className={page === 'users' ? 'active' : 'no-class'}><Link to="/users">Users</Link></li>
              </ul>
              <ul>
                <li className={page === 'allocation' ? 'active' : 'no-class'}><Link to="/allocations">Allocations</Link></li>
              </ul>
              <ul>
                <li className={page === 'admin_offerings' ? 'active' : 'no-class'}><Link to="/admin_offerings">Offerings</Link></li>
              </ul>
              <ul>
                <li className={page === 'messaging' ? 'active' : 'no-class'}><Link to="/admin_messages">Messages</Link></li>
              </ul>
              <ul>
                <li className={page === 'intact_file_upload' ? 'active' : 'no-class'}>
                <Link to="/intact_file_upload">Intact File Upload</Link></li>
              </ul>
              <ul>
                <li className={page === 'add_new_admin' ? 'active' : 'no-class'}>
                <Link to="/add_new_admin">Admin</Link></li>
              </ul>
              <ul>
                <li className={page === 'connectedcustomer' ? 'active' : 'no-class'}>
                <Link to="/connectedcustomer">Connected Customers</Link></li>
              </ul>
            </div>}
          </div>
        }
      </div>


    );
  }
}

Header.propTypes = {
  user: React.PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  user: makeSelectCurrentUser(),
});

export default connect(mapStateToProps, null)(Header);
