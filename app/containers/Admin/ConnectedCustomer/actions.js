import {
  FETCH_CUSTOMER_LIST,
  FETCH_CUSTOMER_LIST_SUCCESS,
} from './constants';


export function fetchBrokerDealersList() {
  return {
    type: FETCH_CUSTOMER_LIST,
  };
}

export function fetchCustomerListSuccess(data) {
  return {
    type: FETCH_CUSTOMER_LIST_SUCCESS,
    data,
  };
}

