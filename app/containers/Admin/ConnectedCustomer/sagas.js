

import { call, put, all, takeLatest, select } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { BASE_URL } from 'containers/App/constants';
import { showGlobalLoader, closeGlobalLoader } from 'containers/App/actions';
import request from 'utils/request';
import * as actions from './actions';
import * as actionTypes from './constants';
import Cookies from 'universal-cookie';

const cookies = new Cookies();
const getToken = (state) => state.getIn(['global', 'currentUser', 'token']);



function* handleFetchCustomerList() {
  console.log(27)
  const TOKEN = yield select(getToken);
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/web_admin/active_customers`;
  let userData;
  try {
    userData = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    if (userData) {
      yield put(actions.fetchCustomerListSuccess(userData));
    }
  } catch (err) {
    console.log(err);
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
  } finally {
    yield put(closeGlobalLoader());
  }
}
function* watcherFetchCustomerList() {
  yield takeLatest(actionTypes.FETCH_CUSTOMER_LIST, handleFetchCustomerList);
}


// Bootstrap sagas
export default function* allocationSagas() {
  yield all([
    watcherFetchCustomerList()
  ]);
}
