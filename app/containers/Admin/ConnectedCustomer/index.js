/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Switch, Route } from 'react-router-dom';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { makeSelectCurrentUser } from 'containers/App/selectors';
import { fetchBrokerDealersList } from './actions';
import saga from './sagas';
import reducer from './reducer';
import UsersList from './list/usersList';

class ConnectedCustomer extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {
    this.props.getBrokerDealers();
  }

  render() {
    const { match } = this.props;
    return (
      <Switch>
        <Route exact path={match.url} component={UsersList} />
      </Switch>
    );
  }
}

ConnectedCustomer.propTypes = {
  match: PropTypes.object,
  currentUser: PropTypes.object,
  getBrokerDealers: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    getBrokerDealers: () => dispatch(fetchBrokerDealersList()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'adminUsers', reducer });
const withSaga = injectSaga({ key: 'adminUsers', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(ConnectedCustomer);
