/*
 * Investor Reducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import {
  FETCH_CUSTOMER_LIST_SUCCESS,
} from './constants';

const initialState = fromJS({
  customerList: null,
});


function adminUsersReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_CUSTOMER_LIST_SUCCESS:
      return state.set('customerList', action.data);
    default:
      return state;
  }
}


export default adminUsersReducer;
