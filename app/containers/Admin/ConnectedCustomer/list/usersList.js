/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Link } from 'react-router-dom';

import { makeSelectCurrentUser } from 'containers/App/selectors';
import RaisedButton from 'material-ui/RaisedButton';
import AdminWrapper from 'components/AdminWrapper';
import { makeCustomerList } from '../selectors';

import {DataTable} from 'primereact/components/datatable/DataTable';
import {Column} from 'primereact/components/column/Column';
import {Button} from 'primereact/components/button/Button';
import {InputText} from 'primereact/components/inputtext/InputText';
import {ColumnGroup} from 'primereact/components/columngroup/ColumnGroup';
import {Row} from 'primereact/components/row/Row';
class UsersList extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      searching:''
    };
    this.searchOffering=this.searchOffering.bind(this);
  }

  searchOffering(event){
      this.setState({
        searching:event.target.value
      })
  }

  render() {
    const { customerList } = this.props;
    const Header = (<div className="offer_head">Connected Customer   { customerList ? ('( Total Count = ' + customerList.length + ')'):' '}</div>);
    const Search =(<div className="search inline">
                    <div className="search_icon"></div>
                    <input id="searchInput" type="text" placeholder="Search" onChange={this.searchOffering} />
                    <div className="search_close" onTouchTap={this.getAllOfferings}></div>
                </div>)

    const customerFilterData = this.state.searching? customerList.filter(row => (row.first_name+" "+row.last_name).toLowerCase().indexOf(this.state.searching.toLowerCase()) > -1) : customerList;
    return (
      <AdminWrapper title="Admin" header={Header} search={Search}>
        <div className="info">
            <div className="tab">
              <div className="row">
                <div className="col-12"  style={{paddingLeft:30,paddingRight:20,paddingTop:15,marginBottom:80}}>

                  <DataTable value={customerFilterData} editable={false}  responsive={true}   scrollable={true} scrollHeight="90rem" resizableColumns={true}  columnResizeMode="expand">
                        <Column className="dataTableNameColumn" field="email" header="Email Address"  sortable={true} />
                        <Column className="tableCols"   field="first_name" header="Customer First Name"  sortable={true} />
                        <Column  className="tableCols"   field="last_name" header="Customer Last Name"  sortable={true} />
                  </DataTable>
                </div>
              </div>
            </div>
        </div>
      </AdminWrapper>

    );
  }
}

UsersList.propTypes = {
  customerList: PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  customerList: makeCustomerList(),
});


export default connect(mapStateToProps, null)(UsersList);
