import { createSelector } from 'reselect';

const selectUsers = (state) => state.get('adminUsers');

const makeCustomerList = () => createSelector(
  selectUsers,
  (adminState) => {
    const list = adminState.get('customerList');
    return list;
  }
);


export {
  selectUsers,
  makeCustomerList,
};
