/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { makeSelectMessages } from '../selectors';
import MessageTable from './messageTable';


class MessageList extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      sort: {
        type: null,
        field: null,
      },
    };
  }

  render() {
    const { messageList } = this.props;
    return (
      <div className="info">
        <div className="tabs">
          <input type="radio" name="tabs" id="tab-broker-dealers" defaultChecked="checked" />
          <label htmlFor="tab-broker-dealers" className="tab-left-label">MESSAGE LIST</label>
          <div className="tab">
            <div className="row">
              <div className="col-12">
                {messageList &&<MessageTable orders={messageList} sort={this.state.sort} />}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

MessageList.propTypes = {
  messageList: React.PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  messageList: makeSelectMessages(),
});


export default connect(mapStateToProps, null)(MessageList);
