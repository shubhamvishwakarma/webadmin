import React from 'react';
import { Link } from 'react-router-dom';
import TableHOC from 'components/TableHOC';

const Headers = [
  { title: 'Name', field: 'name', class: 'ta-left' },
];

const tableClassName = 'tbl-investors-closed';

function MessageList(props) {
  const { data } = props;
  return (
    <tbody>
      {
          data.map((item) => (
            <tr key={item.id}>
              <td className="ta-left"><div className="td-data">
                <Link to={`/admin_messages/${item.id}`} style={{ color: '#8DC73F' }} >{item.name}</Link>
              </div></td>
            </tr>
          ))
        }
    </tbody>
  );
}

MessageList.propTypes = {
  orders: React.PropTypes.any, // eslint-disable-line
  data: React.PropTypes.any,
  sortAscending: React.PropTypes.func, // eslint-disable-line
  sortDescending: React.PropTypes.func, // eslint-disable-line
};

export default TableHOC(MessageList, Headers, tableClassName);
