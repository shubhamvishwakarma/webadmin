/**
 * API calls for Ola
 */

import { call, put, all, takeLatest, select } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { BASE_URL } from 'containers/App/constants';
import { showGlobalLoader, closeGlobalLoader } from 'containers/App/actions';
import request from 'utils/request';
import { fetchMessageListSuccess, fetchMessageDetailsSuccess, fetchMessageList, saveMessageFormSuccess } from './actions';
import { FETCH_MESSAGE_LIST, FETCH_MESSAGE_DETAILS, SAVE_MESSAGE_FORM } from './constants';
import Cookies from 'universal-cookie';

const cookies = new Cookies();
const getToken = (state) => state.getIn(['global', 'currentUser', 'token']);



function* handleGetMessageList() {
  const TOKEN = yield select(getToken);
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/emails`;
  let response;
  try {
    response = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    yield put(fetchMessageListSuccess(response));
  } catch (err) {
    console.log(err);
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
  } finally {
    yield put(closeGlobalLoader());
  }
}
function* fetchMessageListWatcher() {
  yield takeLatest(FETCH_MESSAGE_LIST, handleGetMessageList);
}

function* handleGetMessageDetails(action) {
  const TOKEN = yield select(getToken);
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/emails/${action.id}`;
  let response;
  try {
    response = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    yield put(fetchMessageDetailsSuccess(response));
  } catch (err) {
    console.log(err);
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
  } finally {
    yield put(closeGlobalLoader());
  }
}
function* fetchMessageDetailsWatcher() {
  yield takeLatest(FETCH_MESSAGE_DETAILS, handleGetMessageDetails);
}

function* handleSaveMessageForm(action) {
  const TOKEN = yield select(getToken);
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const actionData = action.data.toJS();
  const BODY = JSON.stringify({
    ...actionData,
  });
  const URL = `${BASE_URL}/emails/${actionData.id}`;
  try {
    yield call(request, URL, {
      method: 'PUT',
      headers: ajaxRequestHeaders,
      body: BODY,
    });
    yield put(saveMessageFormSuccess());
    yield put(fetchMessageList());
    yield put(push('/admin_messages'));
  } catch (err) {
    console.log(err);
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
  } finally {
    yield put(closeGlobalLoader());
  }
}
function* submitMessageFormWatcher() {
  yield takeLatest(SAVE_MESSAGE_FORM, handleSaveMessageForm);
}


// Bootstrap sagas
export default function* messagesSagas() {
  yield all([
    fetchMessageListWatcher(),
    fetchMessageDetailsWatcher(),
    submitMessageFormWatcher(),
  ]);
}
