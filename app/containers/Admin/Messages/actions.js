import {
  FETCH_MESSAGE_LIST,
  FETCH_MESSAGE_LIST_SUCCESS,
  SAVE_MESSAGE_FORM,
  FETCH_MESSAGE_DETAILS,
  FETCH_MESSAGE_DETAILS_SUCCESS,
  SAVE_MESSAGE_FORM_SUCCESS,
} from './constants';


export function fetchMessageList() {
  return {
    type: FETCH_MESSAGE_LIST,
  };
}

export function fetchMessageListSuccess(data) {
  return {
    type: FETCH_MESSAGE_LIST_SUCCESS,
    data,
  };
}

export function saveMessageForm(data) {
  return {
    type: SAVE_MESSAGE_FORM,
    data,
  };
}

export function saveMessageFormSuccess() {
  return {
    type: SAVE_MESSAGE_FORM_SUCCESS,
  };
}

export function fetchMessageDetails(id) {
  return {
    type: FETCH_MESSAGE_DETAILS,
    id,
  };
}

export function fetchMessageDetailsSuccess(data) {
  return {
    type: FETCH_MESSAGE_DETAILS_SUCCESS,
    data,
  };
}
