/*
 * Investor Reducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import {
  FETCH_MESSAGE_LIST,
  FETCH_MESSAGE_LIST_SUCCESS,
  FETCH_MESSAGE_DETAILS_SUCCESS,
  SAVE_MESSAGE_FORM_SUCCESS,
} from './constants';

const initialState = fromJS({
  messageList: null,
  messageDetail: {},
});


function adminMessagesReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_MESSAGE_LIST_SUCCESS:
      return state.set('messageList', action.data);
    case FETCH_MESSAGE_DETAILS_SUCCESS:
      return state.set('messageDetail', action.data);
    case FETCH_MESSAGE_LIST:
      return state.set('messageDetail', {});
    case SAVE_MESSAGE_FORM_SUCCESS:
      return state.set('messageList', null);
    default:
      return state;
  }
}


export default adminMessagesReducer;
