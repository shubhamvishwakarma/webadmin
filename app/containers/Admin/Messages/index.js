/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Switch, Route } from 'react-router-dom';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { intercomBoot } from 'intercom';
import { makeSelectCurrentUser } from 'containers/App/selectors';
import AdminWrapper from 'components/AdminWrapper';
import MessageForm from './detail/form';
import { fetchMessageList } from './actions';
import saga from './sagas';
import reducer from './reducer';
import MessageList from './list/index';


class Messages extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function


  componentDidMount() {
    //intercomBoot(this.props.currentUser);
    this.props.getMessageList();
  }

  setPageHeader(header) {
    this.setState({ header });
  }

  render() {
    const { match } = this.props;
    const Header = (<div className="offer_head" style={{ width: '200px', textAlign: 'left' }}>Messages</div>);
    return (

      <AdminWrapper title="Admin" header={Header} >
        <div className="info">
          <Switch>
            <Route exact path={match.url} component={MessageList} />
            <Route exact path={`${match.url}/:id`} component={MessageForm} />
          </Switch>
        </div>
      </AdminWrapper>

    );
  }
}

Messages.propTypes = {
  currentUser: React.PropTypes.object,
  match: React.PropTypes.object,
  getMessageList: React.PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    getMessageList: () => dispatch(fetchMessageList()),
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'adminMessages', reducer });
const withSaga = injectSaga({ key: 'adminMessages', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(Messages);
