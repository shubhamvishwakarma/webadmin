export default function validate(values) {
  const errors = {};
  if (!values.get('name')) {
    errors.name = 'Please enter name';
  }
  if (!values.get('subject')) {
    errors.subject = 'Please enter subject';
  }
  if (!values.get('body')) {
    errors.message = 'Please enter a message';
  }
  return errors;
}
