import React from 'react/lib/React';
import { reduxForm, Field } from 'redux-form/immutable';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import TextField from 'redux-form-material-ui/lib/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import validate from './formValidation';
import { makeSelectMessageDetail } from '../selectors';
import { fetchMessageDetails, saveMessageForm } from '../actions';

const fields = ['name', 'subject', 'template', 'message'];

class MessageForm extends React.Component { // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {
    const { getMessageDetail, match } = this.props;
    getMessageDetail(match.params.id);
  }

  render() {
    const { handleSubmit, submitting, handleFormSubmit, initialValues } = this.props;
    return (
      <form className="row" onSubmit={handleSubmit(handleFormSubmit)} style={{ padding: '0px 25px 25px 25px' }}>
        <div className="row">
          <div className="col-4 col-sm-12">
            <Field
              name="name"
              component={TextField}
              floatingLabelText="Name of campaign"
              autoComplete="off"
              hintText="Name of campaign"
              fullWidth
              type="text"
              floatingLabelFocusStyle={{ color: '#8dc73f' }}
              underlineFocusStyle={{ borderColor: '#8dc73f' }}
              hintStyle={{ paddingLeft: '10px' }}
            />
          </div>
          <div className="col-4 col-sm-12" style={{ marginLeft: '30px' }}>
            <div className="info-section">
              <div className="info-titles">ID:</div>
              <div className="info-titles-value">{initialValues ? initialValues.toJS().short_code : ''}</div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-12 col-sm-12">
            <Field
              name="subject"
              component={TextField}
              floatingLabelText="Subject"
              autoComplete="off"
              hintText="Subject"
              fullWidth
              type="text"
              floatingLabelFocusStyle={{ color: '#8dc73f' }}
              underlineFocusStyle={{ borderColor: '#8dc73f' }}
              hintStyle={{ paddingLeft: '10px' }}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-12 col-sm-12">
            <Field
              name="body"
              component={TextField}
              floatingLabelText="Body"
              autoComplete="off"
              multiLine
              rows={10}
              hintText="Message"
              hintStyle={{ top: '73px', left: '20px', fontSize: '13px' }}
              floatingLabelFixed
              floatingLabelStyle={{ fontSize: '20px', marginTop: '20px' }}
              textareaStyle={{ border: '1.8px solid #E6E6E6', marginTop: '60px', borderRadius: '6px', marginBottom: '30px', padding: '10px' }}
              underlineShow={false}
              errorStyle={{ paddingTop: '0px' }}
              fullWidth
            />
          </div>
        </div>

        <div className="row">
          <div className="col-12 columns">
            <RaisedButton type="submit" backgroundColor="#8DC73F" labelColor="#fff" style={{ marginTop: '30px' }} disabled={submitting} label="Save" />
          </div>
        </div>
      </form>
    );
  }
}

MessageForm.propTypes = {
  handleSubmit: React.PropTypes.func,
  submitting: React.PropTypes.bool,
  handleFormSubmit: React.PropTypes.func,
  match: React.PropTypes.object,
  getMessageDetail: React.PropTypes.func,
  initialValues: React.PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  initialValues: makeSelectMessageDetail(),
});

function mapDispatchToProps(dispatch) {
  return {
    getMessageDetail: (id) => dispatch(fetchMessageDetails(id)),
    handleFormSubmit: (data) => dispatch(saveMessageForm(data)),
  };
}

const ClickIPOCreateBrokerDealerForm = reduxForm({
  form: 'clickIPOMessageForm',
  fields,
  validate,
  destroyOnUnmount: false,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
  touchOnBlur: false,
})(MessageForm);

export default connect(mapStateToProps, mapDispatchToProps)(ClickIPOCreateBrokerDealerForm);
