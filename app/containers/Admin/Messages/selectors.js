import { createSelector } from 'reselect';

const selecMessages = (state) => state.get('adminMessages');

const makeSelectMessages = () => createSelector(
  selecMessages,
  (adminState) => adminState.get('messageList')
);

const makeSelectMessageDetail = () => createSelector(
  selecMessages,
  (adminState) => {
    const detail = adminState.get('messageDetail');
    return detail;
  }
);


export {
  selecMessages,
  makeSelectMessages,
  makeSelectMessageDetail,
};
