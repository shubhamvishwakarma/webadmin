/**
 * API calls for Ola
 */

import { call, put, all, takeLatest, select } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { BASE_URL } from 'containers/App/constants';
import { showGlobalLoader, closeGlobalLoader } from 'containers/App/actions';
import request from 'utils/request';
import * as actions from './action';
import * as actionTypes from './constant';
import {store} from '../../../app'
import axios from 'axios';

import Cookies from 'universal-cookie';

const cookies = new Cookies();
const getToken = (state) => state.getIn(['global', 'currentUser', 'token']);





function* handleUploadIntactFile(action) {
  console.log(1)

  yield put(showGlobalLoader());


  const TOKEN = yield select(getToken);


 try{

          let ajaxRequestHeaders_FILE = new Headers({
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'Authorization': TOKEN,
          });
          let URL_FILE=`${BASE_URL}/offerings/intact_file_upload`;
          let Body_File=JSON.stringify({
                ext_id:'',
                ipo_or_secondary: 2
          })
          let FileUpload=yield call(request,URL_FILE,{
              method:"POST",
              headers:ajaxRequestHeaders_FILE,
              body:Body_File
          })
          if(FileUpload)
          {
            alert("Success Fully Uploaded Intact File")
          }


 }
 catch(err){
        if(err.response.status==401){
            cookies.remove('currentUser');
            cookies.remove('email');
            cookies.remove('role');
            window.location.href = '/login';
        }
        console.log("err")
 }
 finally{
     yield put(closeGlobalLoader());
 }



}


function* watcherUploadOffering() {
  yield takeLatest(actionTypes.CALL_INTACT_FILE_UPLOAD, handleUploadIntactFile);
}

// Bootstrap sagas
export default function* IntactSaga() {
  yield all([
    watcherUploadOffering()
  ]);
}
