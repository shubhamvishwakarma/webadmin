import React,{Component} from 'react';
import AdminWrapper from 'components/AdminWrapper';
import { connect } from 'react-redux';
import {callIntactUpload} from './action';
import { compose } from 'redux';
import { Switch, Route } from 'react-router-dom';
import reducer from './reducer';
import saga from './saga';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { createStructuredSelector } from 'reselect';
const Header = (<div style={{textAlign:"center",fontSize:"25",color:"#254C7D"}}>Intact File Upload</div>);
export  class IntactFile extends Component{
        constructor(){
            super();
            this.handleSubmit=this.handleSubmit.bind(this);
        }

        handleSubmit(e){
            e.preventDefault();
           this.props.callIntactUpload()
        }

    render(){

        return(
            <AdminWrapper title="Admin" header={Header}>
                <div className="info">
                <div style={{margin:"20%"}}>
                    <form onSubmit={this.handleSubmit}>
                        <input className="btn btn-success" style={{fontSize:20,fontWeight:"900",width:"250px",height:"60px"}} type="submit" value="Upload Intact File"/>
                    </form>
                </div>
                </div >
             </AdminWrapper>
        )
    }
}



function mapDispatchToProps(dispatch) {
    return {
        callIntactUpload: () => {
        dispatch(callIntactUpload());
      },
     
    };
  }
  
  const mapStateToProps = createStructuredSelector({
    
  });
  
  const withConnect = connect(mapStateToProps, mapDispatchToProps);

    const withReducer = injectReducer({ key: 'IntactFile', reducer });
    const withSaga = injectSaga({ key: 'IntactFile', saga });

    export default compose(
    withReducer,
    withSaga,
    withConnect,
    )(IntactFile);
  
 