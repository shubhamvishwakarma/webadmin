import { fromJS } from 'immutable';

import {
CALL_INTACT_FILE_UPLOAD,
} from './constant';

const initialState = fromJS({
  success: null,

});


function IntactFileReducer(state = initialState, action) {
  switch (action.type) {
    case CALL_INTACT_FILE_UPLOAD:
      return state.set('allocationList', action.data);
    
    default:
      return state;
  }
}


export default IntactFileReducer;
