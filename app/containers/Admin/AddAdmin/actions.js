import {
  FETCH_BROKERADMIN_LIST,
  FETCH_BROKERADMIN_LIST_SUCCESS,
  SUBMIT_CREATE_BROKER_ADMIN_FORM,
  CREATE_BROKER_ADMIN_SUCCESS,
  CREATE_BROKER_ADMIN_FAILED,
  SUBMIT_CREATE_UNDERWRITER_ADMIN_FORM,
  CREATE_UNDERWRITER_ADMIN_SUCCESS,
  CREATE_UNDERWRITER_ADMIN_FAILED,
  FETCH_OFFERING_LIST,
  FETCH_OFFERING_LIST_SUCCESS,
  FETCH_OFFERING_ORDERS,
  FETCH_OFFERING_ORDERS_SUCCESS,
  FETCH_CUSTOMER_LIST_SUCCESS,
  FETCH_CUSTOMER_LIST,
  SEND_NOTIFICATION_CUSTOMER_LIST,
  SEND_NOTIFICATION_CUSTOMER_LIST_SUCCESS,
  FETCH_ALL_CUSTOMER_LIST,
  FETCH_ALL_CUSTOMER_LIST_SUCCESS,
  DELETE_CUSTOMER_FROM_LIST_SUCCESS,
  DELETE_CUSTOMER_FROM_LIST
} from './constants';


export function fetchBrokerAdminList() {
  return {
    type: FETCH_BROKERADMIN_LIST,
  };
}


export function fetchBrokerAdminListSuccess(data) {
  return {
    type: FETCH_BROKERADMIN_LIST_SUCCESS,
    data,
  };
}
export function fetchOfferingList() {
  return {
    type: FETCH_OFFERING_LIST,
  };
}
export function fetchOfferingListSuccess(data) {
  return {
    type: FETCH_OFFERING_LIST_SUCCESS,
    data,
  };
}

export function submitCreateBrokeradminForm(data) {
  return {
    type: SUBMIT_CREATE_BROKER_ADMIN_FORM,
    data,
  };
}

export function createBrokerAdminSuccess() {
  return {
    type: CREATE_BROKER_ADMIN_SUCCESS,
  };
}

export function createBrokerAdminFailed() {
  return {
    type: CREATE_BROKER_ADMIN_FAILED,
  };
}

export function submitCreateUnderwriteradminForm(data) {
  return {
    type: SUBMIT_CREATE_UNDERWRITER_ADMIN_FORM,
    data,
  };
}

export function createUnderwriterAdminSuccess() {
  return {
    type: CREATE_UNDERWRITER_ADMIN_SUCCESS,
  };
}

export function createUnderwriterAdminFailed() {
  return {
    type: CREATE_UNDERWRITER_ADMIN_FAILED,
  };
}
export function fetchOfferingOrders(id) {
  return {
    type: FETCH_OFFERING_ORDERS,
    id
  };
}

export function fetchOfferingOrdersSuccess(data) {
  return {
    type: FETCH_OFFERING_ORDERS_SUCCESS,
    data,
  };
}

export function fetchCustomerList() {
  return {
    type: FETCH_CUSTOMER_LIST,
  };
}


export function fetchCustomerListSuccess(data) {
  return {
    type: FETCH_CUSTOMER_LIST_SUCCESS,
    data,
  };
}

export function fetchSendNotification(data) {
  return {
    type: SEND_NOTIFICATION_CUSTOMER_LIST,
    data,
  };
}

export function sendNotificationSuccess(data) {
  return {
    type: SEND_NOTIFICATION_CUSTOMER_LIST_SUCCESS,
    data,
  };
}



export function fetchAllCustomerList() {
  return {
    type: FETCH_ALL_CUSTOMER_LIST,
  };
}

export function fetchAllCustomerListSuccess(data) {
  return {
    type: FETCH_ALL_CUSTOMER_LIST_SUCCESS,
    data,
  };
}


export function fetchDeleteCustomer(email) {
  return {
    type: DELETE_CUSTOMER_FROM_LIST,
    email
  };
}

export function fetchDeleteCustomerSuccess(data) {
  return {
    type: DELETE_CUSTOMER_FROM_LIST_SUCCESS,
    data,
  };
}








