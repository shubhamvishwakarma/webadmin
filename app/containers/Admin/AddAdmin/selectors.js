import { createSelector } from 'reselect';

const selectUsers = (state) => state.get('adminUsers');

const makeSelectBroderDealers = () => createSelector(
  selectUsers,
  (adminState) => {
    const list = adminState.get('brokerDealerList');
    if (list) {
      list.map((item) => {
        if (item.blocker_code === '77') {
          item.blocker_code = 'Full Allocation'; // eslint-disable-line
        } else if (item.blocker_code === '74') {
          item.blocker_code = 'X-Clearing'; // eslint-disable-line
        }
        return item;
      });
    }
    return list;
  }
);
const makeOfferingList = () => createSelector(
  selectUsers,
  (adminState) => {
    const list = adminState.get('offeringList');
    if (list) {
      list.map((item) => {
        return item;
      });
    }
    return list;
  }
);
const makeOfferingOrderList = () => createSelector(
  selectUsers,
  (adminState) => {
    const list = adminState.get('offeringOrdersList');
    if (list) {
      list.map((item) => {
        return item;
      });
    }
    return list;
  }
);

const makeCustomerList = () => createSelector(
  selectUsers,
  (adminState) => {
    const list = adminState.get('customerList');
    return list;
  }
);

const makeNotificationResponse = () => createSelector(
  selectUsers,
  (adminState) => {
    const list = adminState.get('notificationSuccess');
    return list;
  }
);

const makeAllCustomersList = () => createSelector(
  selectUsers,
  (adminState) => {
    const list = adminState.get('allCustomerList');
    return list;
  }
);

const makeDeleteCustomerRes = () => createSelector(
  selectUsers,
  (adminState) => {
    const list = adminState.get('deleteCustomer');
    return list;
  }
);




export {
  selectUsers,
  makeSelectBroderDealers,
  makeOfferingList,
  makeOfferingOrderList,
  makeCustomerList,
  makeNotificationResponse,
  makeAllCustomersList,
  makeDeleteCustomerRes,
};
