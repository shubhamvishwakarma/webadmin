/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Switch, Route } from 'react-router-dom';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { makeSelectCurrentUser } from 'containers/App/selectors';
import { fetchBrokerAdminList ,fetchOfferingList,fetchCustomerList} from './actions';
import saga from './sagas';
import reducer from './reducer';
import UsersList from './list/usersList';
import CreateBrokerAdmin from './detail/createbrokeradmin/index';
import CreateUnderwriterAdmin from './detail/createunderwriteradmin/index'

class AddNewAdmin extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {
    this.props.getBrokerDealers();
    this.props.getOfferingList();
    this.props.getCustomerList();
  }

  render() {
    const { match} = this.props;
    return (
      <Switch>
        <Route exact path={match.url} component={UsersList} />
        <Route path={`${match.url}/create_broker_admin`} component={CreateBrokerAdmin} />
        <Route path={`${match.url}/create_underwriter_admin`} component={CreateUnderwriterAdmin} />
      </Switch>
    );
  }
}

AddNewAdmin.propTypes = {
  match: PropTypes.object,
  currentUser: PropTypes.object,
  getOfferingList: PropTypes.func,
  getBrokerDealers: PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    getOfferingList: () => dispatch(fetchOfferingList()),
    getBrokerDealers: () => dispatch(fetchBrokerAdminList()),
    getCustomerList:()=>dispatch(fetchCustomerList()),

  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'adminUsers', reducer });
const withSaga = injectSaga({ key: 'adminUsers', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect,
)(AddNewAdmin);
