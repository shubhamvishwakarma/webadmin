/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Link } from 'react-router-dom';

import { makeSelectCurrentUser } from 'containers/App/selectors';
import RaisedButton from 'material-ui/RaisedButton';
import AdminWrapper from 'components/AdminWrapper';
import { makeSelectBroderDealers,makeOfferingList ,makeOfferingOrderList,makeCustomerList ,makeNotificationResponse,makeAllCustomersList,makeDeleteCustomerRes} from '../selectors';
import OfferingOrderList from './offeringorders';
import * as actions from '../actions';
import  PerformanceModal from './performanceModel';
import NotificationModal from './notificationModal';
import ConfirmationModal from './confirmationModal.js'

import {DataTable} from 'primereact/components/datatable/DataTable';
import {Column} from 'primereact/components/column/Column';
import {Button} from 'primereact/components/button/Button';
import {InputText} from 'primereact/components/inputtext/InputText';
import {ColumnGroup} from 'primereact/components/columngroup/ColumnGroup';
import {Row} from 'primereact/components/row/Row';


class UsersList extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      sort: {
        type: null,
        field: null,
        notificationshow: null,
        notificationType:'',
        selectedCustomerList:{},
        confirmModal: null,
        confirmationType: '',
        customerInfo:'',
        searching:null,
        searchingConnectedCust:null,
        filterByConnCus:null,
        filterByAllCus: null
      }
    };
    this.handlefetchOffering=this.handlefetchOffering.bind(this);
    this.onSelection=this.onSelection.bind(this);
    this.displaySelection=this.displaySelection.bind(this);
    this.actionTemplate = this.actionTemplate.bind(this);
    this.openConfirmModal=this.openConfirmModal.bind(this);
    this.searchCustomer=this.searchCustomer.bind(this);
    this.searchConnectedCustomer=this.searchConnectedCustomer.bind(this);
    this.dropDownSearchByConnCus=this.dropDownSearchByConnCus.bind(this);
    this.dropDownSearchByAllCus=this.dropDownSearchByAllCus.bind(this);

  }

    componentWillMount() {
        this.props.fetchAllCustomerList();
    }
    handlefetchOffering() {
        this.props.fetchOfferingOrders(this.selectOffering.value);
    }

    toggleModal = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    toggleNotificationModal = () => {
        this.setState({
            notificationshow: !this.state.notificationshow
        });
    }
    openModal() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    openConfirmModal(userInfo, confirmtype) {
        this.setState({
            confirmModal: true,
            confirmationType: confirmtype,
            customerInfo: userInfo
        })
    }
    closeConfirmModal() {
        this.setState({
            confirmModal: false
        })
    }
    confirmModaltoggle = () => {
        this.setState({
            confirmModal: !this.state.confirmModal
        });
    }

    openSendEmail() {
        this.setState({
            notificationshow: true,
            notificationType: 'EMAIL'
        });
    }
    openSendPush() {
        this.setState({
            notificationshow: true,
            notificationType: 'PUSH'
        });
    }

    onSelection(items) {
        this.setState({
            selectedCustomerList: items.data
        })

    }

    displaySelection(items) {
        if (items) {
            if (items.checked === true) {
                items.checked = false;
            } else {
                items.checked = true;
            }
        }
    }
    actionTemplate(rowData, column) {
        return <div>
            <button className="btn btn-reset-paswordbtn" onClick={()=>this.openConfirmModal(rowData,'password')}>Reset Password</button>
            <button className="btn btn-delete" onClick={()=>this.openConfirmModal(rowData,'delete')}> <i className="glyphicon glyphicon-trash" ></i></button>
        </div>;
    }

    searchCustomer(event) {
        this.setState({
            searching: event.target.value
        })
    }
    searchConnectedCustomer(event) {
        this.setState({
            searchingConnectedCust: event.target.value
        })
    }
    dropDownSearchByConnCus(event) {
        this.setState({
            filterByConnCus: event.target.value
        })
    }
    dropDownSearchByAllCus(event) {
        this.setState({
            filterByAllCus: event.target.value
        })
    }


  render() {
    const {brokerdealers, offeringList ,offeringOrdersList,customerList ,notificationRes,allCustomersList,deleteCustomersResponse} = this.props;
    let performanceBtn=[];
    const {filterByConnCus,filterByAllCus} =this.state;


    if(deleteCustomersResponse !== undefined){
      this.state.confirmModal=false;
      this.state.filterByAllCus='';
       allCustomersList.splice(allCustomersList.indexOf(allCustomersList.find(row => row.email == deleteCustomersResponse)), 1);
    }
    // const filterData = this.state.searching? allCustomersList.filter(row => (row.first_name.toLowerCase()+" "+row.last_name.toLowerCase()).indexOf(this.state.searching.toLowerCase()) > -1) : allCustomersList;


      let  filterData =[];

      if(filterByAllCus=='email'){
          filterData = this.state.searching? allCustomersList.filter(row => (row.email.toLowerCase()).indexOf(this.state.searching.toLowerCase()) > -1) : allCustomersList;
      }else if(filterByAllCus=='fname'){
          filterData = this.state.searching? allCustomersList.filter(row => (row.first_name.toLowerCase()).indexOf(this.state.searching.toLowerCase()) > -1) : allCustomersList;
      }else if(filterByAllCus=='lname'){
          filterData = this.state.searching? allCustomersList.filter(row => (row.last_name.toLowerCase()).indexOf(this.state.searching.toLowerCase()) > -1) : allCustomersList;
      }else {
         filterData = this.state.searching? allCustomersList.filter(row => (row.first_name.toLowerCase() +" "+row.last_name.toLowerCase()).indexOf(this.state.searching.toLowerCase()) > -1) : allCustomersList;
      }


      let filterConnectedCustomers=[];
      if(this.state.filterByConnCus=='email'){
        filterConnectedCustomers= this.state.searchingConnectedCust? customerList.filter(row => (row.email.toLowerCase()).indexOf(this.state.searchingConnectedCust.toLowerCase()) > -1) : customerList;

      } else if(this.state.filterByConnCus=='fname'){
          filterConnectedCustomers= this.state.searchingConnectedCust? customerList.filter(row => (row.first_name.toLowerCase()).indexOf(this.state.searchingConnectedCust.toLowerCase()) > -1) : customerList;
      }else if(this.state.filterByConnCus=='lname'){
          filterConnectedCustomers= this.state.searchingConnectedCust? customerList.filter(row => (row.last_name.toLowerCase()).indexOf(this.state.searchingConnectedCust.toLowerCase()) > -1) : customerList;
      }else {
          filterConnectedCustomers= this.state.searchingConnectedCust? customerList.filter(row => (row.first_name.toLowerCase() +" "+row.last_name.toLowerCase()).indexOf(this.state.searchingConnectedCust.toLowerCase()) > -1) : customerList;
      }



    if(this.selectOffering){
      if(offeringList && offeringList.length>0){
          for(let items of offeringList){
              if(this.selectOffering.value==items.id){
                if(items.status=="closed"){
                   performanceBtn=<RaisedButton onClick={this.toggleModal} label="Performance" backgroundColor="#8DC73F" labelColor="#fff" style={{ marginTop: 20 }} />
                }else{
                  performanceBtn=[];
                }
              }
          }
      }

    }
    let options=[];
     if(offeringList && offeringList.length>0){
          options=offeringList.map((item) =>{
            return(
            <option style={{color: item.status=='cancelled' || item.status=='closed' ? '#FF0000':'#000'}}  value={item.id} key={item.id}>
              {item.name}
            </option>
            )
          })

      }

    const Header = (<div className="offer_head">Create New Admin</div>);
    return (
    <div>
     <div className="main_outer_wrapper">
                  <div className="row offeringSubheader">
                      <div className="col-sm-6">
                          <div className="offering_head">Create New Admin</div>
                      </div>
                      <div className="col-sm-6">
                      </div>

                  </div>

        <div className="info" style={{marginTop:40}}>
          <div className="tabs" style={{margin:25}}>
            <input type="radio" name="tabs" id="tab-broker-admin" defaultChecked="checked" />
            <label htmlFor="tab-broker-admin" className="tab-left-label">ADD BROKER ADMIN</label>
            <div className="tab">
              <div className="row">
                <div className="col-10">

                </div>
                <div className="col-2">
                  <Link to={'/add_new_admin/create_broker_admin'}>
                    <RaisedButton label="New Broker " backgroundColor="#8DC73F" labelColor="#fff" style={{ marginTop: '30px' }} />
                  </Link>
                </div>
              </div>
            </div>
            <input type="radio" name="tabs" id="tab-underwriters-admin" />
            <label htmlFor="tab-underwriters-admin" className="tab-label">ADD UNDERWRITERS ADMIN</label>
            <div className="tab">
              <div className="row">
                <div className="col-10">
                  {/* <UsersList orders={[]} /> */}
                </div>
                <div className="col-2">
                  <Link to={'/add_new_admin/create_underwriter_admin'}>
                    <RaisedButton label="New Underwriter" backgroundColor="#8DC73F" labelColor="#fff" style={{ marginTop: '30px' }} />
                  </Link>
                </div>
              </div>
            </div>

            <input type="radio" name="tabs" id="tab-offering-list" />
            <label htmlFor="tab-offering-list" className="tab-label">Orders</label>
            <div className="tab">
              <div className="row">
                <div className="col-12">
                  <div className="col-sm-3 col-md-6">
                  </div>
                  <div className="col-sm-6 col-md-6">
                    <select style={{border:"1px solid #ccc !important",height:41,marginTop:15}} ref={(Elmnt)=>{this.selectOffering=Elmnt}} onChange={this.handlefetchOffering}  >
                      <option style={{opacity:0.2,cursor:"none"}}>Select one of the Offering</option>
                      {options}
                    </select>
                  </div>
                  <div className="col-sm-3 col-md-3">

                     { performanceBtn}
                  </div>
                </div>
                {/* offering Listing*/}
                  { offeringOrdersList && <OfferingOrderList orders={offeringOrdersList} sort={this.state.sort}/>}

                <div>
                  <PerformanceModal  show={this.state.isOpen}  onClose={this.toggleModal} ></PerformanceModal>
                </div>


              </div>
            </div>
              <input type="radio" name="tabs" id="tab-customers" />
              <label htmlFor="tab-customers" className="tab-label">Connected Customers</label>
              <div className="tab">
               <div className="row">
                <div className="col-md-5" >
                    <div className="totalCustomers">Total number of customers : {customerList &&<span> {customerList.length}</span>}</div>
                </div>
                <div className="col-md-7" >
                  <div className="connectedCustomer">
                        <div className="filterbyContent">Search By</div>
                        <select className="selectField"  value={this.state.filterByConnCus} onChange={this.dropDownSearchByConnCus} >
                            <option value="">Select</option>
                            <option value="email">Email</option>
                            <option value="fname">First Name</option>
                            <option value="lname">Last Name</option>
                        </select>
                      <input id="searchInput" type="text" placeholder="Search" onChange={this.searchConnectedCustomer} />
                  </div>
                </div>
              </div>
                <div className="row">
                  <div className="col-12"  style={{paddingLeft:30,paddingRight:20,paddingTop:15}}>
                    <DataTable value={filterConnectedCustomers}   selection={this.state.selectedCustomerList}  onSelectionChange={(e) => this.setState({selectedCustomerList: e.data})} editable={false}  responsive={true}   scrollable={true} scrollHeight="90rem"  resizableColumns={true}  columnResizeMode="expand" sortMode="multiple">
                          <Column selectionMode="multiple" style={{width:'2em'}}/>
                          <Column className="dataTableNameColumn" field="email" header="Email Address"  sortable={true} />
                          <Column className="tableCols"   field="first_name" header="Customer First Name"  sortable={true} />
                          <Column  className="tableCols"   field="last_name" header="Customer Last Name"  sortable={true} />
                    </DataTable>
                    <div className="customerTablefooter">
                      <button className="btn sendNotificationBtn" onClick={()=>{this.openSendEmail()}}>Send Email</button>
                      <button className="btn sendNotificationBtn" onClick={()=>{this.openSendPush()}}>Send Push Notification</button>
                    </div>
                   </div>

                </div>

              </div>

              <input type="radio" name="tabs" id="tab-allcustomers" />
              <label htmlFor="tab-allcustomers" className="tab-label">All Customers</label>
              <div className="tab">
              <div className="row">
                <div className="col-md-5" >
                    <div className="totalCustomers">Total number of customers : {allCustomersList &&<span> {allCustomersList.length}</span>}</div>
                </div>
                <div className="col-md-7" >
                  <div className=" connectedCustomer">
                      <div className="filterbyContent">Search By</div>
                      <select className="selectField"  value={this.state.filterByAllCus} onChange={this.dropDownSearchByAllCus} >
                          <option value="">Select</option>
                          <option value="email">Email</option>
                          <option value="fname">First Name</option>
                          <option value="lname">Last Name</option>
                      </select>
                      <input id="searchInput" type="text" placeholder="Search" onChange={this.searchCustomer} />
                  </div>
                </div>
              </div>
                <div className="row">
                  <div className="col-12"  style={{paddingLeft:30,paddingRight:20,paddingTop:15}}>
                    <DataTable value={filterData}    editable={false}  responsive={true}   scrollable={true} scrollHeight="90rem"  resizableColumns={true}  columnResizeMode="expand" sortMode="multiple">
                          <Column className="dataTableNameColumn"  field="email" header="Email Address"  sortable={true} />
                          <Column className="tableCols"    field="first_name" header="Customer First Name"  sortable={true} />
                          <Column  className="tableCols"   field="last_name" header="Customer Last Name"  sortable={true} />
                          <Column  className="tableCols"   body={this.actionTemplate} />
                    </DataTable>
                  </div>

                </div>

              </div>


          </div>
           <NotificationModal notificationType={this.state.notificationType} notificationshow={this.state.notificationshow}  onClose={this.toggleNotificationModal} sendNotification={this.props.fetchSendNotifications} customerListing={this.state.selectedCustomerList}></NotificationModal>
           <ConfirmationModal confirmationType={this.state.confirmationType} show={this.state.confirmModal} onClose={this.confirmModaltoggle} customerInfo={this.state.customerInfo} deleteCustomer={this.props.deleteCustomer}></ConfirmationModal>

        </div>
       </div>
     </div>

    );
  }
}

UsersList.propTypes = {
  brokerdealers: PropTypes.any,
  offeringList: PropTypes.any,
  customerList: PropTypes.any,
  notificationRes: PropTypes.any,
  allCustomersList:PropTypes.any,
  deleteCustomersResponse:PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
  brokerdealers: makeSelectBroderDealers(),
  offeringList :makeOfferingList(),
  offeringOrdersList:makeOfferingOrderList(),
  customerList: makeCustomerList(),
  notificationRes: makeNotificationResponse(),
  allCustomersList:makeAllCustomersList(),
  deleteCustomersResponse:makeDeleteCustomerRes(),


});


function mapDispatchToProps(dispatch) {
  return {
    fetchOfferingOrders:(id)=>dispatch(actions.fetchOfferingOrders(id)),
    fetchSendNotifications:(data)=>dispatch(actions.fetchSendNotification(data)),
    fetchAllCustomerList:()=>dispatch(actions.fetchAllCustomerList()),
    deleteCustomer:(email)=>dispatch(actions.fetchDeleteCustomer(email)),
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(UsersList);
