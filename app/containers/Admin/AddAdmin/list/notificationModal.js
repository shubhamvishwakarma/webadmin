import React ,{ Component } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { connect } from 'react-redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import moment from 'moment';

class NotificationModal extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      isSubmit:null
    }
  }

  sendNotifications(){

    console.log(this.props.customerListing)
    let usersMail=[];
    for(let items of this.props.customerListing){
      usersMail.push(items.email);
    }

    let sendRequest={
      type: this.props.notificationType,
      trigger_group: usersMail,
      message_title: "Title Test",
      message_body: "tstingmnm"
    }
    this.props.sendNotification(sendRequest)
  }
  render() {
    const {notificationshow,notificationType}=this.props;

    const margin = {top: 20, right: 20, bottom: 30, left: 40};
    if(!this.props.notificationshow) {
      return null;
    }


      return (

        <div className="backdrop" style={{height:"100%", position: 'fixed',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: 'rgba(0,0,0,0.3)',
            padding: 10,
            zIndex:1}}>
        <div style={{height:"100%"}}  onClick={this.props.onClose}>
        </div>

          <div className="modal" style={{height:500 ,backgroundColor: '#fff',
            borderRadius: 5,
            maxWidth: 500,
            minHeight: 500,
            position:'absolute',
            margin: 'auto',
            padding: 15,display: 'block',top: '15px'}}>
              <div className="modalheader">
                <p style={{float:'left',color:"rgb(103, 136, 153)",fontSize:18}}> </p>
                <button className="closeBtn float_right" onClick={this.props.onClose}>
                    <span aria-hidden="true" >&times;</span>
                </button>
              </div>
            <div style={{margin:20}}>
               {notificationType && <div className="row notificationModalhead" >
                    {notificationType=='EMAIL' && <p className="nomagnin text_center">EMAIL Notification </p>}
                    {notificationType=='PUSH' && <p className="nomagnin text_center">PUSH Notification</p>}
                </div>}
                <div className="sendNotificationContainer">
                     <div>
                      <label  className="update-label labelheader"> Title </label>
                      <input className="update-input" type="text"  />
                    </div>
                    <div >
                      <label  className="update-label labelheader" >Message </label>
                      <textarea className="update-input notificationTextarea" />
                    </div>

                    <button className="btn sendMailBtn" onClick={()=>{this.sendNotifications()}}> Send </button>


                </div>

            </div>
          </div>
        </div>
      )
    }
}

NotificationModal.propTypes = {
  onClose : PropTypes.func.isRequired,
  notificationshow: PropTypes.bool,
  notificationType:PropTypes.any,
  handleMouseOver: PropTypes.func,
  handleMouseLeave: PropTypes.func,
  sendNotification:PropTypes.func,
  customerListing: PropTypes.any
};
export default NotificationModal;


