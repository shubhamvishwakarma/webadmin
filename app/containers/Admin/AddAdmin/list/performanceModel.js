import React ,{ Component } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { connect } from 'react-redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import moment from 'moment';
//import BarChart from 'react-bar-chart';
//var ReactD3 = require('react-d3');
import { Recharts, BarChart, Line, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';

// var BarChart = require('react-d3/barchart').BarChart;


class PerformanceModal extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      isSubmit:null,
      width:300
    }
  }
  render() {
    const {show}=this.props;

    const data = [
        {name: 'Requested Shares',  Shares: 1600,  amt: 2400},
        {name: 'Allocated Shares',   Shares: 1200, amt: 2400}
    ];
    const margin = {top: 20, right: 20, bottom: 30, left: 40};
    if(!this.props.show) {
      return null;
    }


      return (

        <div className="backdrop" style={{height:"100%", position: 'fixed',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: 'rgba(0,0,0,0.3)',
            padding: 10,
            zIndex:1}}>
        <div style={{height:"100%"}}  onClick={this.props.onClose}>
        </div>

          <div className="modal" style={{height:500 ,backgroundColor: '#fff',
            borderRadius: 5,
            maxWidth: 500,
            minHeight: 500,
            position:'absolute',
            margin: 'auto',
            padding: 15,display: 'block',top: '15px'}}>
              <div className="modalheader">
                <p style={{float:'left',color:"rgb(103, 136, 153)",fontSize:18}}> </p>
                <button className="closeBtn float_right" onClick={this.props.onClose}>
                    <span aria-hidden="true" >&times;</span>
                </button>
              </div>
            <div style={{margin:20}}>
               <div className="row performanceModelhead" >
                    <p className="nomagnin text_center">Performance Graph </p>
                </div>
                <div>
                <BarChart width={400} height={300} data={data}
                      margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                <CartesianGrid strokeDasharray="3 3"/>
                <XAxis dataKey="name"/>
                <YAxis/>
                <Tooltip/>
                <Legend />
                <Bar dataKey="Shares" fill="#8884d8" background={{ fill: '#eee' }}/>
                </BarChart>

                 {/*  <BarChart data={data1}  width={400}  height={400} margin={{top: 10, bottom: 50, left: 50, right: 10}}/> */}
                </div>

            </div>
          </div>
        </div>
      )
    }
}

PerformanceModal.propTypes = {
  onClose : PropTypes.func.isRequired,
  show: PropTypes.bool,
  handleMouseOver: PropTypes.func,
  handleMouseLeave: PropTypes.func,
};
export default PerformanceModal;


