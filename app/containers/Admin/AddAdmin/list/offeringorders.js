import React from 'react';
import PropTypes from 'prop-types';

import TableHOC from 'components/TableHOC';

const Headers = [
  { title: 'Investor Name', field: 'user_name', class: 'ta-left' },
  { title: 'Email', field: 'user_email', class: 'ta-left' },
  { title: 'Broker Dealer', field: 'broker_dealer', class: 'ta-left' },
  { title: 'Amount Requested', field: 'requested_amount', class: 'ta-left' },
  { title: 'Share Requested', field: 'allocated_shares', class: 'ta-left' },
  { title: 'Status', field: 'status', class: 'ta-left' },
];

const tableClassName = 'tbl-order-list tbl-orderlisting-header';

function OfferingOrderList(props) {
  const { data } = props;
  return (
    <tbody>
      {
          data.map((item,i) => (
            <tr key={i}>
              <td className="ta-left"><div className="td-data"><div style={{ color: '#8DC73F' }} >{item.user_name}</div></div></td>
              <td className="ta-left"><div className="td-data">{item.user_email}</div></td>
              <td className="ta-left"><div className="td-data">{item.broker_dealer}</div></td>
              <td className="ta-left"><div className="td-data">{item.requested_amount}</div></td>
              <td className="ta-left"><div className="td-data">{item.allocated_shares}</div></td>
              <td className="ta-left">
                <div className="td-data" style={{ color:item.status==="cancelled" ?'#FF0000':''}}>{item.status}</div>
              </td>
            </tr>
          ))
        }
    </tbody>
  );
}

OfferingOrderList.propTypes = {
  orders: PropTypes.any, // eslint-disable-line
  data: PropTypes.any,
  sortAscending: PropTypes.func, // eslint-disable-line
  sortDescending: PropTypes.func, // eslint-disable-line
};

export default TableHOC(OfferingOrderList, Headers, tableClassName);
