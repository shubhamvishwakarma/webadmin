import React ,{ Component } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { connect } from 'react-redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import moment from 'moment';


class ConfirmationModal extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      isSubmit:null,
    }
  }

  deleteCustomer(cus_email){
    this.props.deleteCustomer(cus_email)
  }
  render() {
    const {show,confirmationType,customerInfo}=this.props;
    if(!this.props.show) {
      return null;
    }
      return (

        <div className="backdrop" style={{height:"100%", position: 'fixed',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: 'rgba(0,0,0,0.3)',
            padding: 10,
            zIndex:1}}>
        <div style={{height:"100%"}}  onClick={this.props.onClose}>
        </div>

          <div className="modal" style={{height:250 ,backgroundColor: '#fff',
            borderRadius: 5,
            maxWidth: 400,
            minHeight: 250,
            position:'absolute',
            margin: 'auto',
            padding: 15,display: 'block',top: '15px'}}>
              <div className="modalheader">
                <p style={{float:'left',color:"rgb(103, 136, 153)",fontSize:18}}> </p>
                <button className="closeBtn float_right" onClick={this.props.onClose}>
                    <span aria-hidden="true" >&times;</span>
                </button>
              </div>
            <div style={{margin:20}}>
               <div className="row performanceModelhead" >
                    {confirmationType=="password" && <p className="nomagnin text_center">Reset Password</p>}
                    {confirmationType=="delete" && <p className="nomagnin text_center">Delete Customer</p>}
                </div>
                <div  className="text_center row">
                  {confirmationType=="password" && <p className="confirmmationContent">
                    Are you sure you want to send password recovery email to
                      <span > {customerInfo.email} ?</span>
                  </p>}
                  {confirmationType=="delete" && <p className="confirmmationContent">
                    Are you sure you want to delete user <span > {customerInfo.email} ?</span>
                  </p>}
                  <div className="row">
                    {confirmationType=="password" &&<button className="btn btn-reset-paswordbtn" style={{fontSize:20}} onClick={this.props.onClose} >Send</button>}
                    {confirmationType=="delete" &&<button className="btn btn-delete" style={{fontSize:20}}onClick={()=>{this.deleteCustomer(customerInfo.email)}}>Delete</button>}
                    <button className=" btn btn-warning" onClick={this.props.onClose} style={{fontSize:20}}>Cancel</button>
                  </div>
                </div>

            </div>
          </div>
        </div>
      )
    }
}

ConfirmationModal.propTypes = {
  onClose : PropTypes.func.isRequired,
  show: PropTypes.bool,
  handleMouseOver: PropTypes.func,
  handleMouseLeave: PropTypes.func,
  customerInfo: PropTypes.any,
  confirmationType: PropTypes.any,
  deleteCustomer: PropTypes.func,
};
export default ConfirmationModal;


