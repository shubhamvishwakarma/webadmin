import React from 'react';
import PropTypes from 'prop-types';

import TableHOC from 'components/TableHOC';

const Headers = [
  { title: 'Name', field: 'name', class: 'ta-left' },
  { title: 'ID', field: 'clearing_account_id', class: 'ta-right' },
  { title: 'MPID', field: 'mpid', class: 'ta-right' },
  { title: 'Type', field: 'blocker_code', class: 'ta-right' },
  { title: 'Commission', field: 'commission_percentage', class: 'ta-right' },
];

const tableClassName = 'tbl-investors-closed';

function UsersList(props) {
  const { data } = props;
  return (
    <tbody>
      {
          data.map((item) => (
            <tr key={item.id}>
              <td className="ta-left"><div className="td-data"><div style={{ color: '#8DC73F' }} >{item.name}</div></div></td>
              <td className="ta-right"><div className="td-data">{item.clearing_account_id}</div></td>
              <td className="ta-right"><div className="td-data">{item.mpid}</div></td>
              <td className="ta-right"><div className="td-data">{item.blocker_code}</div></td>
              <td className="ta-right"><div className="td-data">{item.commission_percentage}</div></td>
            </tr>
          ))
        }
    </tbody>
  );
}

UsersList.propTypes = {
  orders: PropTypes.any, // eslint-disable-line
  data: PropTypes.any,
  sortAscending: PropTypes.func, // eslint-disable-line
  sortDescending: PropTypes.func, // eslint-disable-line
};

export default TableHOC(UsersList, Headers, tableClassName);
