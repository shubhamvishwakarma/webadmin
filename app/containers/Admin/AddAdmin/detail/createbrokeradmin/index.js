/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { connect } from 'react-redux';
import AdminWrapper from 'components/AdminWrapper';
import { submitCreateBrokeradminForm } from '../../actions';
import CreateBrokeradminForm from './forms';

function CreateBrokerAdmin(props) {
  const { goBack, handleFormSubmit } = props;
  const Header = (
    <div>
      <div className="offer_detail_head">
        <div onTouchTap={goBack} >
            Create new broker admin
        </div>
      </div>
      <div onTouchTap={goBack} >
        <div className="back_icon"></div>
      </div>
    </div>
    );
  return (
    <AdminWrapper title="Admin" header={Header} >
      <div className="info">
        <CreateBrokeradminForm handleFormSubmit={handleFormSubmit} initialValues={{ blocker_code: '74' }} />
      </div>
    </AdminWrapper>
  );
}

CreateBrokerAdmin.propTypes = {
  goBack: React.PropTypes.func,
  handleFormSubmit: React.PropTypes.func,
};


function mapDispatchToProps(dispatch, ownProps) {
  return {
    goBack: () => { ownProps.history.goBack(); },
    handleFormSubmit: (data) => dispatch(submitCreateBrokeradminForm(data)),
  };
}

export default connect(null, mapDispatchToProps)(CreateBrokerAdmin);
