import React from 'react/lib/React';
import { reduxForm, Field } from 'redux-form/immutable';
import TextField from 'redux-form-material-ui/lib/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import RadioButtonGroup from 'redux-form-material-ui/lib/RadioButtonGroup';
import RadioButton from 'material-ui/RadioButton';
import RadioButtonChecked from 'material-ui/svg-icons/action/done';
import renderDropzoneInput from 'components/FileField';
import validate from './formValidation';

const fields = ['name', 'logo','address_attributes.address','address_attributes.address_2','address_attributes.city', 'address_attributes.state', 'address_attributes.zipcode', 'address_attributes.country','address_attributes.latitude','address_attributes.longitude'];


function CreateUnderwriteradminForm(props) {
  const { handleSubmit, submitting, handleFormSubmit } = props;
  return (
    <form className="row" onSubmit={handleSubmit(handleFormSubmit)} style={{ padding: '0px 25px 25px 25px' }}>
      <div className="row">
        <div className="col-4 col-sm-12">
          <Field
            name="name"
            component={TextField}
            floatingLabelText="Name"
            autoComplete="off"
            hintText="Underwriter name"
            fullWidth
            type="text"
            floatingLabelFocusStyle={{ color: '#8dc73f' }}
            underlineFocusStyle={{ borderColor: '#8dc73f' }}
            hintStyle={{ paddingLeft: '10px' }}
          />
        </div>
      </div>
      <div className="row">
        <div className="col-4 col-sm-12">
          <Field
            name="address_attributes.address"
            component={TextField}
            floatingLabelText="Address"
            hintText="Address"
            autoComplete="off"
            fullWidth
            type="text"
            floatingLabelFocusStyle={{ color: '#8dc73f' }}
            underlineFocusStyle={{ borderColor: '#8dc73f' }}
            hintStyle={{ paddingLeft: '10px' }}
          />
        </div>
        <div className="col-4 col-sm-12">
          <Field
            name="address_attributes.address_2"
            component={TextField}
            floatingLabelText="Second address"
            hintText="Second Address"
            autoComplete="off"
            fullWidth
            type="text"
            floatingLabelFocusStyle={{ color: '#8dc73f' }}
            underlineFocusStyle={{ borderColor: '#8dc73f' }}
            hintStyle={{ paddingLeft: '10px' }}
          />
        </div>
      </div>
      <div className="row">
        <div className="col-3 col-sm-12">
          <Field
            name="address_attributes.city"
            component={TextField}
            floatingLabelText="City"
            hintText="City"
            autoComplete="off"
            fullWidth
            type="text"
            floatingLabelFocusStyle={{ color: '#8dc73f' }}
            underlineFocusStyle={{ borderColor: '#8dc73f' }}
            hintStyle={{ paddingLeft: '10px' }}
          />
        </div>
        <div className="col-3 col-sm-12" style={{ marginLeft: '20px' }}>
          <Field
            name="address_attributes.state"
            component={TextField}
            floatingLabelText="State"
            autoComplete="off"
            fullWidth
            type="text"
            hintText="State"
            floatingLabelFocusStyle={{ color: '#8dc73f' }}
            underlineFocusStyle={{ borderColor: '#8dc73f' }}
            hintStyle={{ paddingLeft: '10px' }}
          >
          </Field>
        </div>
        <div className="col-3 col-sm-12" style={{ marginLeft: '20px' }}>
          <Field
            name="address_attributes.country"
            component={TextField}
            floatingLabelText="Country"
            autoComplete="off"
            fullWidth
            type="text"
            hintText="Country"
            floatingLabelFocusStyle={{ color: '#8dc73f' }}
            underlineFocusStyle={{ borderColor: '#8dc73f' }}
            hintStyle={{ paddingLeft: '10px' }}
          >
          </Field>
        </div>
      </div>
        <div className="row">
          <div className="col-4 col-sm-12" >
            <Field
              name="address_attributes.zipcode"
              component={TextField}
              floatingLabelText="Postal Code"
              hintText="Postal Code"
              autoComplete="off"
              fullWidth
              type="text"
              floatingLabelFocusStyle={{ color: '#8dc73f' }}
              underlineFocusStyle={{ borderColor: '#8dc73f' }}
              hintStyle={{ paddingLeft: '10px' }}
            />
          </div>
      </div>

      <div className="row">
        <div className="col-4 col-sm-12" style={{ margin: '30px 0px 0px 0px', paddingBottom: '20px' }}>
          <Field
            name="logo"
            component={renderDropzoneInput}
          />
        </div>
      </div>




      <div className="row">
        <div className="col-12 columns">
          <RaisedButton type="submit" backgroundColor="#8DC73F" labelColor="#fff" style={{ marginTop: '30px' }} disabled={submitting} label="Create new user" />
        </div>
      </div>
    </form>
  );
}

CreateUnderwriteradminForm.propTypes = {
  handleSubmit: React.PropTypes.func,
  submitting: React.PropTypes.bool,
  handleFormSubmit: React.PropTypes.func,
};

const ClickIPOCreateUnderwriteradminForm = reduxForm({
  form: 'clickIPOCreateUnderwriteradminForm',
  fields,
  validate,
  destroyOnUnmount: false,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
  touchOnBlur: false,
})(CreateUnderwriteradminForm);

export default ClickIPOCreateUnderwriteradminForm;
