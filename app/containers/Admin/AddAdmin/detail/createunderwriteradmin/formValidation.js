
export default function validate(values) {
  const errors = {};
  errors.address_attributes = {};

  if (!values.get('name')) {
    errors.name = 'Please enter a name';
  }
  if (!values.getIn(['address_attributes', 'address'])) {
    errors.address_attributes.address = 'Please enter a address';
  }
  if (!values.getIn(['address_attributes', 'city'])) {
    errors.address_attributes.city = 'Please enter a city';
  }
  if (!values.getIn(['address_attributes', 'state'])) {
    errors.address_attributes.state = 'Please select a state';
  }
  if (!values.getIn(['address_attributes', 'country'])) {
    errors.address_attributes.country = 'Please select a country';
  }
  if (!values.getIn(['address_attributes', 'zipcode'])) {
    errors.address_attributes.zipcode = 'Please enter a zipcode';
  }
  // if (!values.get('mobile')) {
  //   errors.mobile = 'Please enter mobile number';
  // }
  if (!values.get('commission_percentage')) {
    errors.commission_percentage = 'Please enter commission';
  }
  if (!values.get('clearing_account_id')) {
    errors.clearing_account_id = 'Please enter clearing account ID';
  }
  if (!values.get('mpid')) {
    errors.mpid = 'Please enter mpid';
  }
  return errors;
}
