/*
 * Investor Reducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import {
  CREATE_BROKER_DEALER_SUCCESS,
   FETCH_OFFERING_LIST_SUCCESS,
   FETCH_OFFERING_ORDERS,
   FETCH_OFFERING_ORDERS_SUCCESS,
   FETCH_CUSTOMER_LIST_SUCCESS,
   SEND_NOTIFICATION_CUSTOMER_LIST_SUCCESS,
   FETCH_ALL_CUSTOMER_LIST_SUCCESS,
   DELETE_CUSTOMER_FROM_LIST_SUCCESS,
} from './constants';

const initialState = fromJS({
  brokerDealerList: null,
});


function adminUsersReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_OFFERING_LIST_SUCCESS:
      return state.set('offeringList', action.data);
    case CREATE_BROKER_DEALER_SUCCESS:
      return state.set('brokerDealerList', null);
    case FETCH_OFFERING_ORDERS_SUCCESS:
      return state.set('offeringOrdersList', action.data);
    case FETCH_CUSTOMER_LIST_SUCCESS:
      return state.set('customerList', action.data);
    case SEND_NOTIFICATION_CUSTOMER_LIST_SUCCESS :
      return state.set('notificationSuccess', action.data);
    case FETCH_ALL_CUSTOMER_LIST_SUCCESS :
      return state.set('allCustomerList', action.data);
    case DELETE_CUSTOMER_FROM_LIST_SUCCESS :
      return state.set('deleteCustomer', action.data);

    default:
      return state;
  }
}


export default adminUsersReducer;
