

import { call, put, all, takeLatest, select } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { BASE_URL } from 'containers/App/constants';
import { showGlobalLoader, closeGlobalLoader } from 'containers/App/actions';
import {ToastContainer, ToastStore} from 'react-toasts';
import request from 'utils/request';
import * as actions from './actions';
import * as actionTypes from './constants';
import Cookies from 'universal-cookie';
import { LogStaging } from '../../App/Helpers'
import axios from 'axios';
const cookies = new Cookies();


function sendNotification(url,data, config ){
  LogStaging(33)
    return axios.put(url,data, config );
}

const getToken = (state) => state.getIn(['global', 'currentUser', 'token']);



function* handleFetchBrokerAdminList() {
  LogStaging(28)
  const TOKEN = yield select(getToken);
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/broker_dealers/get_broker_dealer`;
  let userData;
  try {
    userData = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    if (userData) {
      yield put(actions.fetchBrokerAdminListSuccess(userData));
    }
  } catch (err) {
    console.log(err);
    if(err.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
  } finally {
    yield put(closeGlobalLoader());
  }
}
function* watcherFetchBrokerAdminList() {
  yield takeLatest(actionTypes.FETCH_BROKERADMIN_LIST, handleFetchBrokerAdminList);
}

function* handleCreateBrokerAdmin(action) {
  LogStaging(29)
  yield put(showGlobalLoader());
  const TOKEN = yield select(getToken);
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const actionData = action.data.toJS();
  if (actionData.logo) {
    actionData.logo = {
      lastModified: actionData.logo[0].lastModified,
      lastModifiedDate: actionData.logo[0].lastModifiedDate,
      name: actionData.logo[0].name,
      size: actionData.logo[0].size,
      type: actionData.logo[0].type,
      webkitRelativePath: actionData.logo[0].webkitRelativePath,
    };
  }
  const BODY = JSON.stringify({
    ...actionData,
  });
  const URL = `${BASE_URL}/broker_dealers/create_broker_dealer`;
  try {
    yield call(request, URL, {
      method: 'POST',
      headers: ajaxRequestHeaders,
      body: BODY,
    });
    yield put(actions.createBrokerAdminSuccess());
    //yield put(actions.fetchBrokerAdminList());
    ToastStore.success("Successfully Added");
    window.location=window.location.origin+"/add_new_admin";
    // yield put(push('/add_new_admin'));
  } catch (err) {
    console.log(err);
    if(err.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
    yield put(actions.createBrokerAdminFailed());
  } finally {
    // yield put(closeGlobalLoader());
  }
}
function* watcherCreateBrokerAdmin() {
  yield takeLatest(actionTypes.SUBMIT_CREATE_BROKER_ADMIN_FORM, handleCreateBrokerAdmin);
}


function* handleCreateUnderwriterAdmin(action) {
  LogStaging(30)
  yield put(showGlobalLoader());
  const TOKEN = yield select(getToken);
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const actionData = action.data.toJS();
  if (actionData.logo) {
    actionData.logo = {
      lastModified: actionData.logo[0].lastModified,
      lastModifiedDate: actionData.logo[0].lastModifiedDate,
      name: actionData.logo[0].name,
      size: actionData.logo[0].size,
      type: actionData.logo[0].type,
      webkitRelativePath: actionData.logo[0].webkitRelativePath,
    };
  }
  const BODY = JSON.stringify({
    ...actionData,
  });
  const URL = `${BASE_URL}/underwriters`;
  try {
    yield call(request, URL, {
      method: 'POST',
      headers: ajaxRequestHeaders,
      body: BODY,
    });
    yield put(actions.createUnderwriterAdminSuccess());
    ToastStore.success("Successfully Added");
    window.location=window.location.origin+"/add_new_admin";
  } catch (err) {
    console.log(err);
    if(err.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
    yield put(actions.createUnderwriterAdminFailed());
  } finally {
    // yield put(closeGlobalLoader());
  }
}
function* watcherCreateUnderwriterAdmin() {
  yield takeLatest(actionTypes.SUBMIT_CREATE_UNDERWRITER_ADMIN_FORM, handleCreateUnderwriterAdmin);
}

function* handleFetchOfferingList() {
  LogStaging(31)
  yield put(showGlobalLoader());
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  const TOKEN = yield select(getToken);
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/offerings/get_name`;
  let response;
  try {
    response = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    yield put(actions.fetchOfferingListSuccess(response));
  } catch (err) {
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
    console.log(err);
  } finally {
    // yield put(closeGlobalLoader());
  }
}
function* watcherFetchOfferingList() {
  yield takeLatest(actionTypes.FETCH_OFFERING_LIST, handleFetchOfferingList);
}
function* handleFetchOfferingOrdersList(action) {
  LogStaging(32)
  yield put(showGlobalLoader());
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  const TOKEN = yield select(getToken);
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/web_admin/${action.id}/all_orders`;
  let response;
  try {
    response = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    yield put(actions.fetchOfferingOrdersSuccess(response));
  } catch (err) {
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
    console.log(err);
  } finally {
    // yield put(closeGlobalLoader());
  }
}
function* watcherFetchOfferingOrderList() {
  yield takeLatest(actionTypes.FETCH_OFFERING_ORDERS, handleFetchOfferingOrdersList);
}





function* handleFetchCustomerList() {
  console.log(23)
  yield put(showGlobalLoader());
  const TOKEN = yield select(getToken);
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/web_admin/active_customers`;
  let userData;
  try {
    userData = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    if (userData) {
      yield put(actions.fetchCustomerListSuccess(userData));
    }
  } catch (err) {
    console.log(err);
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
  } finally {
    // yield put(closeGlobalLoader());
  }
}
function* watcherFetchCustomerList() {
  yield takeLatest(actionTypes.FETCH_CUSTOMER_LIST, handleFetchCustomerList);
}






function* handleSendNotification(action) {
  LogStaging(24)
  console.log(action);
  yield put(showGlobalLoader());
  const TOKEN = yield select(getToken);
  // let ajaxRequestHeaders = new Headers({
  //   'Content-Type': 'application/json',
  //   Accept: 'application/json',
  // });

  // ajaxRequestHeaders.append('Authorization', `${TOKEN}`);

  const BODY = JSON.stringify({
    title: "Notification",
    type: action.data.type,
    trigger_group: action.data.trigger_group,
    message_title : action.data.message_title,
    message_body: action.data.message_body,
  });
  console.log(BODY);
        yield put(showGlobalLoader());
        const URL = `${BASE_URL}/users/send_nofications`;

        let data = new FormData();
        data.append('title', "Notification")
        data.append('type', action.data.type)
        data.append('trigger_group', JSON.stringify(action.data.trigger_group))
        data.append('message_title', action.data.message_title)
        data.append('message_body', action.data.message_body)
        var url=`${BASE_URL}/users/send_nofications`;
        var config = {
          headers: {
            'accept': 'application/json',
            'accept-language': 'en_US',
            'Authorization':TOKEN,
            'content-type': `multipart/form-data; boundary=${data._boundary} `
          }
        };
        let response;
        try {
           response = yield call(sendNotification,url,BODY, config)
          console.log(response);
           if(response.status==200){
              yield put(actions.sendNotificationSuccess(true));
              yield put(actions.sendNotificationSuccess(null));
           }

        } catch (err) {
          console.log(err.response.status);


          if(err.response.status==401){
              cookies.remove('currentUser');
              cookies.remove('email');
              cookies.remove('role');
              window.location.href = '/login';
          }
        } finally {

          // yield put(closeGlobalLoader());
        }
//  let response;
//   try {
//     response = yield call(request, URL, {
//       method: 'POST',
//       headers: ajaxRequestHeaders,
//       body:BODY
//     });

//     console.log(response)
//     // yield call(request, URL, {
//     //   method: 'POST',
//     //   headers: ajaxRequestHeaders,
//     //   body: BODY,
//     // });
//     // yield put(actions.sendNotificationSuccess());
//     ToastStore.success("Successfully Added");
//   } catch (err) {
//     console.log(err);
//     if(err.status==401){
//         cookies.remove('currentUser');
//         cookies.remove('email');
//         cookies.remove('role');
//         window.location.href = '/login';
//     }
//     // yield put(actions.createUnderwriterAdminFailed());
//   } finally {
//     yield put(closeGlobalLoader());
//   }
}
function* watcherSendNotification() {
  yield takeLatest(actionTypes.SEND_NOTIFICATION_CUSTOMER_LIST, handleSendNotification);
}



function* handleFetchAllCustomerList() {
  console.log(26)
  yield put(showGlobalLoader());
  const TOKEN = yield select(getToken);
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/users`;
  let userData;
  try {
    userData = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    if (userData) {
      yield put(actions.fetchAllCustomerListSuccess(userData));
    }
  } catch (err) {
    console.log(err);
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
  } finally {
    // yield put(closeGlobalLoader());
  }
}

function* watcherFetchAllCustomerList() {
  yield takeLatest(actionTypes.FETCH_ALL_CUSTOMER_LIST, handleFetchAllCustomerList);
}




function* handleDeleteCustomer(action) {
  console.log(27)
  yield put(showGlobalLoader());
  const TOKEN = yield select(getToken);
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/users/delete_user?email=${action.email}`;
  let userData;
  try {
    userData = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    if (userData) {
      yield put(actions.fetchDeleteCustomerSuccess(action.email));
      yield put(actions.fetchDeleteCustomerSuccess(undefined));
    }
  } catch (err) {
    console.log(err);
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
  } finally {
    // yield put(closeGlobalLoader());
  }
}

function* watcherDeleteCustomer() {
  yield takeLatest(actionTypes.DELETE_CUSTOMER_FROM_LIST, handleDeleteCustomer);
}




// Bootstrap sagas
export default function* allocationSagas() {
  yield all([
    watcherFetchBrokerAdminList(),
    watcherCreateBrokerAdmin(),
    watcherCreateUnderwriterAdmin(),
    watcherFetchOfferingList(),
    watcherFetchOfferingOrderList(),
    watcherFetchCustomerList(),
    watcherSendNotification(),
    watcherFetchAllCustomerList(),
    watcherDeleteCustomer(),
  ]);
}
