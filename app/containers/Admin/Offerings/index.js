

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Switch, Route,withRouter } from 'react-router-dom';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { makeSelectCurrentUser } from 'containers/App/selectors';
import { fetchCurrentOfferings, fetchOfferingTypes, fetchIndustries } from './actions';
import saga from './sagas';
import reducer from './reducer';
import OfferingList from './list/index';
import CreateNewOffering from './detail/index';
import EditNewOffering from  './Update/index';
import OrderListing from './orderlist/index'

class OfferingsAdmin extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {
    this.props.fetchInitialData();
  }

  render() {
    const { match } = this.props;
    return (
      <Switch>
        <Route exact path={match.url} component={OfferingList} />
        <Route path={`${match.url}/create`} component={CreateNewOffering} />
        <Route  path={`${match.url}/edit/:id`} component={EditNewOffering} />
         <Route  path={`${match.url}/orders/:id`} component={OrderListing} />
      </Switch>
    );
  }
}

OfferingsAdmin.propTypes = {
  currentUser: React.PropTypes.object,
  match: PropTypes.object,
  fetchInitialData: React.PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    fetchInitialData: () => {
      dispatch(fetchCurrentOfferings());
      dispatch(fetchOfferingTypes());
      dispatch(fetchIndustries());
    },
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'adminOfferings', reducer });
const withSaga = injectSaga({ key: 'adminOfferings', saga });

export default withRouter(compose(
  withReducer,
  withSaga,
  withConnect,
)(OfferingsAdmin));
