import  moment from 'moment';
export default function validate(values) {
  const errors = {};
  if (!values.name) {
    errors.name = 'Please enter name';
  }
  if (!values.ticker_symbol) {
    errors.ticker_symbol = 'Please enter ticker symbol';
  }

  if (!values.ext_id) {
    errors.ext_id = 'Please enter file/ext id';
  }
  // if (!values.offering_type_id) {
  //   errors.offering_type_id = 'Please enter offering type';
  // }


  if(!values.lastClosing){
      if (!values.min_price) {
        errors.min_price = 'Please enter min price';
      }
  }

  if (!values.max_price) {
      errors.max_price = 'Please enter max price';
  }

  // if (!values.max_date) {
  //   errors.max_date = 'Please enter Approximate Date/Time';
  // }
  if(values.max_date){
    var pattern=/(([0-9]{1}[0-9]{1})|([1-9]{1}))([/]{1})(([0-9]{1}[0-9]{1})|([1-9]{1}))([/]{1})(([0-9]{4})[ ]{1}([0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}))|([TBD]{3})/
    if(!pattern.test(values.max_date))
    {
      errors.max_date = 'Please enter correct date and time or TBD';
    }
  }
  if(values.max_date){
    if (values.isApproxDateChecked !==true) {
        var errorVal=moment(values.max_date,'MM/DD/YYYY hh:mm:ss').isValid();
        var pattern=/(([0-9]{1}[0-9]{1})|([1-9]{1}))([/]{1})(([0-9]{1}[0-9]{1})|([1-9]{1}))([/]{1})(([0-9]{4})[ ]{1}([0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}))|([TBD]{3})/
        if(!pattern.test(values.max_date)){
          errors.max_date = 'Please enter correct date and time ';
        }
    }
    // else if((values.max_date!=="TBD")) {
    //   {
    //     if(errorVal==false)
    //     {
    //       errors.max_date = 'Please enter valid date and time';
    //     }
    //   }
    // }

  }else {
    if (values.isApproxDateChecked==false) {
         errors.max_date = 'Please enter correct date and time Or select TBD';
    }

  }


  // if (!values.trade_date) {
  //   errors.trade_date = 'Please enter Settlement Date/Time';
  // }


  if(values.trade_date){
     if (values.isSettlementDateChecked !==true) {
        var errorVal=moment(values.trade_date,'MM/DD/YYYY hh:mm:ss').isValid();
        var pattern=/(([0-9]{1}[0-9]{1})|([1-9]{1}))([/]{1})(([0-9]{1}[0-9]{1})|([1-9]{1}))([/]{1})(([0-9]{4})[ ]{1}([0-9]{2}[:]{1}[0-9]{2}[:]{1}[0-9]{2}))|([TBD]{3})/
        if(!pattern.test(values.trade_date))
        {

          errors.trade_date = 'Please enter correct date and time or TBD';
        }
     }
    // else if((values.trade_date!=="TBD")) {
    //   {
    //     if(errorVal==false)
    //     {
    //       errors.trade_date = 'Please enter valid date and time ';
    //     }
    //   }
    // }
  }else {
    if (values.isSettlementDateChecked==false) {
         errors.trade_date = 'Please enter correct date and time Or select TBD';
    }

  }


  // }
  // if (!values.get('final_shares')) {
  //   // errors.final_shares = 'Please enter final shares';
  // }
  if (!values.description) {
    errors.description = 'Please enter description';
  }
  if (!values.industry) {
    errors.industry = 'Please select category';
  }
  if (!values.bd_underwriter_mpid_id) {
    errors.bd_underwriter_mpid_id = 'Please select underwriter MPID';
  }
  if (!values.underwriter_concession_amount) {
    errors.underwriter_concession_amount = 'Please enter underwriter concession amount';
  }



  // if (!values.get('underwriters_list')) {
  //   // errors.underwriters_list = 'Please enter underwriters';
  // }
  if (!values.prospectus_url) {
    errors.prospectus_url = 'Please enter prospectus url';
  }
  return errors;
}
