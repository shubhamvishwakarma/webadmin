import React from 'react/lib/React';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { reduxForm, change, reset } from 'redux-form/immutable';


import renderDropzoneInput from 'components/FileField';
import RaisedButton from 'material-ui/RaisedButton';
import {ToastContainer, ToastStore} from 'react-toasts';
import { mmddyy, parseTime } from 'containers/App/Helpers';
import Select from 'react-select';
import validate from './formValidation';
import { makeSelectOfferingTypes, makeSelectIndustries ,makeSelectUserMpid,makeSelectUserMpidCommision,makeCreateOfferingSuccess} from '../../selectors';

import { submitCreateOfferingForm ,fetchUserMpid,fetchUserMpidCommision,createOfferingSuccess} from '../../actions';

import {Calendar} from 'primereact/components/calendar/Calendar.js';
import  moment from 'moment';


let max = 0;
let min = 0;
let savedData;

class OfferingForm extends React.PureComponent {

  constructor(props) {
    super(props);


    let today = new Date();
    let month = today.getMonth();
    let year = today.getFullYear();
    let prevMonth = (month === 0) ? 11 : month - 1;
    let prevYear = (prevMonth === 11) ? year - 1 : year;


    let minDate = new Date();
    minDate.setMonth(prevMonth+1);
    minDate.setFullYear(prevYear);


    this.state = {
      warning: false,
      message: null,
      lastClosing:false,
      typeId:"",
      name:'',
      ticker_symbol:'',
      cusip_id:'',
      type:'',
      offering_type_id:'',
      max_price:'',
      min_price:'',
      final_shares:'',
      description:'',
      industry:'',
      prospectus_url:'',
      underwriters_list:'',
      category_ids:'',
      trade_date:'',
      max_date:'',
      final_version:false,
      available_to_order:false,
      max_dateError:false,
      trade_dateError:false,
      issubmited:false,
      final_price:0,
      isOrderable:false,
      UnderwriterValue:'',
      UnderwriterId:'',
      selectUserMpId:'',
      bd_underwriter_mpid_id:'',
      selected_bd_underwriter_mpid_id: '',
      underwriter_concession_amount:'',
      minDate: minDate,
      max_date_show:'',
      trade_date_show:'',
      previewLogo:'',
      isApproxDateChecked: false,
      isSettlementDateChecked: false

    };
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.goOffering=this.goOffering.bind(this);
    this._Selected=this._Selected.bind(this);

    this.handleChange=this.handleChange.bind(this);
    this.toggleChange=this.toggleChange.bind(this)
    this.mpIDhandleChange=this.mpIDhandleChange.bind(this);
    this._SelectedofferingType=this._SelectedofferingType.bind(this);
    this.industryHandleChange=this.industryHandleChange.bind(this);
    this.settleMentDateChange=this.settleMentDateChange.bind(this);
    this.ApproxDateChange=this.ApproxDateChange.bind(this);

  }

  componentWillMount() {
    //this.props.getOfferingData();
    this.props.loadUserMpid();
  }


  componentWillReceiveProps(newProps) {
    const { offeringTypes } = newProps;
    if(offeringTypes && this.state.typeId == '') {
      //this is setting the typeId to IPO's tyoeId
      // this.state.typeId=offeringTypes[0].id;
      this.setState({ typeId: offeringTypes[0].id })
    }
  }
  handleFormSubmit(data) {
    this.setState({
      issubmited:true
    })
    validate(this.state);
    const errors = validate(this.state);

    if(Object.keys(errors).length === 0){
        this.props.handleFormSubmit(this.state,this.state.typeId);
        this.props.reset();
    }
    //this.props.handleFormSubmit(data,this.state.typeId);
  }

  mpIDhandleChange(event){
      this.setState({
        selected_bd_underwriter_mpid_id: event,
        bd_underwriter_mpid_id: event.value,
      })
  }

  industryHandleChange = (event) => {
      this.setState({
        industry: event,
        category_ids: event.value
      });
  }


  toggleChange(){
    if(this.state.available_to_order==true){
        this.setState({
          available_to_order:false
        });
    }else{
      this.setState({
          available_to_order:true
      });
    }
  }


  goOffering(){
    this.props.history.goBack();
  }
  _Selected(event,key,payload){
    event=JSON.parse(this.Offering_Type.value);

      if(event[0]=="IPO"){
        this.setState({
          typeId:event[1],
          lastClosing:false
        })
      }else{
        this.setState({
          typeId:event[1],
          lastClosing:true
        })
      }

  }
  _SelectedofferingType(event){
      if(event.name=="IPO")
      {
        this.setState({
          typeId:event.id,
          lastClosing:false
        })
      }
      else{
        this.setState({
          typeId:event.id,
          lastClosing:true
        })
      }

  }


  handleUserMpid(value){
   this.setState({
    UnderwriterValue:'',
    UnderwriterId:''
   })
   this.selectUserMpId=value;
  //  this.props.fetchCommission(this.selectUserMpId);
  }
  handleUpdateCommission(){
    this.setState({
      UnderwriterValue:this.Underwriter.value
    })
  }
  trimvalue(value) {
    if(value.length==1){
      return value.replace(/^\s+|\s+$/g,"");
    }else{
      return value.replace(/^\s+|\s+$/g," ");
    }
  }


  handleChange(event){
    this.setState({
      ext_id:this.fileId.value,
      name:this.trimvalue(this.name.value),
      ticker_symbol:this.trimvalue(this.ticker.value),
      cusip_id:this.trimvalue(this.cusip.value),
      type:this.state.type,
      max_price:this.trimvalue(this.max_price.value),
      min_price:(this.min_price)?this.trimvalue(this.min_price.value):null,
      final_shares:this.trimvalue(this.final_shares.value),
      // max_date:this.trimvalue(this.max_date.value),
       max_date:this.trimvalue(this.state.max_date),
      // trade_date:this.trimvalue(this.trade_date.value),
      trade_date:this.trimvalue(this.state.trade_date),
      description:this.description.value,
      underwriters_list:this.trimvalue(this.underwriters_list.value),
      prospectus_url:this.prospectus_url.value,
      offering_type_id:this.state.offering_type_id,


      //offering_type_id:(this.props.offeringTypes.filter((item)=>{return item.name==this.state.type}))[0].id,
//      category_ids:(this.props.industries.filter((item)=>{return item.name==document.getElementById("industry").value}))[0].id,
      //final_version:this.final_version,
      available_to_order:this.state.available_to_order,
      File:this.File.files,
      // File:this.File.files[0],
      final_price:this.final_price,
      underwriter_concession_amount:this.trimvalue(this.underwriter_concession_amount.value)
    })

    if(this.File.value){
      this.setState({
        previewLogo:URL.createObjectURL(this.File.files[0]),
      })

    }
  }


  settleMentDateChange(){
    if(this.state.isSettlementDateChecked==false){
      this.setState({
        isSettlementDateChecked: true,
        trade_date:'',
        trade_date_show:null

      });
    }else{
      this.setState({
        isSettlementDateChecked: false,
      });
    }
  }
  selectSettlementDate(e){
    if(e.value){
      this.setState({
        trade_date: moment(e.value).format('MM/DD/YYYY hh:mm:ss'),
        trade_date_show:e.value,
        isSettlementDateChecked: false,
      })
    }else{
      this.setState({
        trade_date:'',
        isSettlementDateChecked: false,
      })
    }
  }





  selectApproxDate(e){
    if(e.value){
      var settlementDate = new Date(e.value);
      settlementDate.setDate(e.value.getDate()+2);
      this.setState({
        max_date: moment(e.value).format('MM/DD/YYYY hh:mm:ss'),
        trade_date: moment(settlementDate).format('MM/DD/YYYY hh:mm:ss'),
        max_date_show: e.value,
        trade_date_show:settlementDate,
        isApproxDateChecked: false,
        isSettlementDateChecked: false,
      })
    }else{
      this.setState({
        max_date:'',
         isApproxDateChecked: false,
      })
    }
  }

  ApproxDateChange(){
    if(this.state.isApproxDateChecked==false){
      this.setState({
        isApproxDateChecked: true,
        max_date:'',
        max_date_show:null,
        isSettlementDateChecked: true,
        trade_date:'',
        trade_date_show:null

      });
    }else{
      this.setState({
        isApproxDateChecked: false,
      });
    }
  }



  render() {
   const errors = validate(this.state);
    const { warning, message,typeId ,issubmited} = this.state;


    const { handleSubmit, submitting, updatedData, offeringTypes, industries ,UserMpid ,history,offeringSuccsess} = this.props;


    if(offeringSuccsess!==undefined && offeringSuccsess!==null){
       this.goOffering();
    }
    savedData = updatedData;
    let UserMpidList=[];
    let brokerDealers="";
    let typeAheadUserMpidList=[];
    if(UserMpid && UserMpid.length>0){
      UserMpidList=UserMpid.map((item)=>{
            typeAheadUserMpidList.push({value:item.executing_broker_mpid,label:item.executing_broker_mpid})


              return (<option value={item.executing_broker_mpid} id={item.id} key={item.id}>
                {item.executing_broker_mpid}
              </option>)
          })
    }
    let indusnewOption=[];
    if(industries){
      if(industries.length>0){
          industries.map((item)=>{
            indusnewOption.push({value:item.id,label:item.name})
          })
      }
    }

    let offeringTypesRender=[];

    if(offeringTypes && offeringTypes.length>0){
      offeringTypesRender=offeringTypes.map((item,i)=>{
          return (<div className="col-6 col-sm-12" style={{marginTop:5 }} key={i}>
              { this.state.typeId==item.id &&<RaisedButton backgroundColor="#8DC73F" labelColor="#fff" label={item.name}   style={{width:'90%'}} onClick={()=>{this._SelectedofferingType(item)}} />}
               { this.state.typeId!=item.id &&<RaisedButton  style={{width:'90%'}} labelColor="#000" label={item.name}  onClick={()=>{this._SelectedofferingType(item)}} />}
            </div>)
      })
    }

    return (
      <div>
        <ToastContainer store={ToastStore}/>

          <div className="offering_Detail_header">
              <div onTouchTap={() => history.goBack()} >
                <div className="back_icon"></div>
              </div>
              <div className="Headingname">
                Create new offering
              </div>
          </div>
        <div style={{ padding: '0px 50px 0px 50px'}}>
          <p style={{ paddingLeft: '10px', color: '#B2B2B2'}}>* is required</p>

          <form className="row" onSubmit={handleSubmit(this.handleFormSubmit)}>
            <div style={{ padding: '0px 25px 0px 25px' }}>
                <div className="row">
                    { offeringTypesRender}
                </div>
              <div className="row" style={{marginTop:30}}>
                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px'}}>
                  <label className="update-label">Company name *</label>
                  <input className="update-input" type="text" value={this.state.name} ref={(Elmt)=>{this.name=Elmt}} onChange={this.handleChange}  />
                    {errors&& issubmited && <span className="errorvalidation">{errors.name}</span>}
                </div>
                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <label className="update-label">Ticker Symbol *</label>
                  <input type="text" className="update-input" value={this.state.ticker_symbol} ref={(Elmt)=>{this.ticker=Elmt}} onChange={this.handleChange}/>
                  {errors&& issubmited && <span className="errorvalidation">{errors.ticker_symbol}</span>}
                </div>
              </div>
              <div className="row" style={{marginTop:30}}>
                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <label className="update-label">CUSIP ID</label>
                  <input type="text" className="update-input" value={this.state.cusip_id} ref={(Elmt)=>{this.cusip=Elmt}} onChange={this.handleChange}/>

                  </div>
                  <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                        <label className="update-label">FILE/EXT ID  *</label>
                        <input className="update-input" type="text" className="update-input" value={this.state.ext_id} ref={(Elmt)=>{this.fileId=Elmt}} onChange={this.handleChange} />
                        {errors&& issubmited && <span className="errorvalidation">{errors.ext_id}</span>}
                  </div>
              </div>
              <div className="row" style={{marginTop:30}}>
              {(this.state.lastClosing)?( <div className="col-12 col-sm-12 form-group has-feedback has-Dollar" style={{ padding: '0px 20px 0px 20px' }}>

                  <label className="update-label">Last closing price*</label>
                  <span className="fa fa-dollar form-control-feedback"></span>
                  <input style={{width:'95%'}} className="update-input form-control" type="text" value={this.state.max_price} ref={(Elmt)=>{this.max_price=Elmt}} onChange={this.handleChange}/>
                  {errors&& issubmited && <span className="errorvalidation">{errors.max_price}</span>}

              </div>):(<div><div className="col-6 col-sm-12 form-group has-feedback has-Dollar" style={{ padding: '0px 20px 0px 20px' }}>
                  <label className="update-label">Min Price*</label>
                   <span className="fa fa-dollar form-control-feedback"></span>
                  <input className="update-input form-control" type="text" value={this.state.min_price} ref={(Elmt)=>{this.min_price=Elmt}} onChange={this.handleChange}/>
                  {errors&& issubmited && <span className="errorvalidation">{errors.min_price}</span>}

                  </div>
                  <div className="col-6 col-sm-12 form-group has-feedback has-Dollar" style={{ padding: '0px 20px 0px 20px' }}>

                  <label className="update-label">Max Price*</label>
                  <span className="fa fa-dollar form-control-feedback"></span>
                  <input className="update-input form-control" type="text" value={this.state.max_price} ref={(Elmt)=>{this.max_price=Elmt}} onChange={this.handleChange}/>
                  {errors&& issubmited && <span className="errorvalidation">{errors.max_price}</span>}

                </div></div>)}


              </div>
              <div className="row" style={{marginTop:30}}>
                  <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                      <label className="update-label">Approximate Date/Time *</label>

                       <div className="row">
                        <div className="col-sm-8" >
                         <Calendar minDate={this.state.minDate} value={this.state.max_date_show} onChange={(e) => this.selectApproxDate(e)} showTime={true} showSeconds={true} />

                        </div>
                        <div className="col-sm-4 update-label" style={{fontSize:20 ,paddingTop:10}}>
                          <input type="checkbox"  checked={this.state.isApproxDateChecked} onChange={this.ApproxDateChange}/> TBD
                        </div>

                      </div>
                      {/*<input type="text" className="update-input" value={this.state.max_date} ref={(Elmt)=>{this.max_date=Elmt}} onChange={this.handleChange} placeholder="MM/DD/YYYY HH:MM:SS"/>*/}
                      {errors&& issubmited && <span className="errorvalidation">{errors.max_date}</span>}
                  </div>
                  <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>

                      <label className="update-label">Settlement date / time *</label>

                       <div className="row">
                        <div className="col-sm-8" >
                          <Calendar minDate={this.state.minDate} value={this.state.trade_date_show} onChange={(e) => this.selectSettlementDate(e)} showTime={true} showSeconds={true} />

                        </div>
                        <div className="col-sm-4 update-label" style={{fontSize:20 ,paddingTop:10}}>
                          <input type="checkbox"  checked={this.state.isSettlementDateChecked} onChange={this.settleMentDateChange}/> TBD
                        </div>
                      </div>



                      {/*<input  type="text" className="update-input" value={this.state.trade_date} ref={(Elmt)=>{this.trade_date=Elmt}} onChange={this.handleChange} placeholder="MM/DD/YYYY HH:MM:SS"/>*/}
                      {errors&& issubmited && <span className="errorvalidation">{errors.trade_date}</span>}

                  </div>
              </div>
              <div className="row" style={{marginTop:30}}>
                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                    <label className="update-label">Listed Shares</label>
                    <input type="text" className="update-input" value={this.state.final_shares} ref={(Elmt)=>{this.final_shares=Elmt}} onChange={this.handleChange}/>

                </div>
                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                    <label className="update-label">Industries *</label>
                    <Select
                      name="selected-industry"
                      id="industry"
                      value={this.state.industry}
                      ref={ (select) => { this.industry = select } }
                      onChange={this.industryHandleChange}
                      options={indusnewOption}
                    />
                    {errors&& issubmited && <span className="errorvalidation">{errors.industry}</span>}
                </div>

              </div>

              <div className="row" style={{marginTop:30}}>
                <div className="col-12 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>

                  <label className="update-label">Description *</label>
                  <textarea  type="text" style={{height:"100px",borderStyle: "groove",borderColor:"#ccc",width:'95%'}}  rows={4} value={this.state.description} ref={(Elmt)=>{this.description=Elmt}} onChange={this.handleChange}/>
                  {errors&& issubmited && <span className="errorvalidation">{errors.description}</span>}
                </div>
              </div>
              <div className="row" style={{marginTop:30}}>
                  <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <label className="update-label">Underwriter MPID *</label>
                    <Select
                      name="selected_bd_underwriter"
                      id="bd_underwriter_mpid_id"
                      value={this.state.selected_bd_underwriter_mpid_id}
                      ref={ (select) => { this.bd_underwriter_mpid_id = select } }
                      onChange={this.mpIDhandleChange}
                      options={typeAheadUserMpidList}
                    />
                    {errors&& issubmited && <span className="errorvalidation">{errors.bd_underwriter_mpid_id}</span>}
                  </div>
                  <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>

                      <label className="update-label">Underwriter concession *</label>
                      <input type="text" className="update-input" ref={(Elmt)=>{this.underwriter_concession_amount=Elmt}} value={this.state.underwriter_concession_amount} onChange={this.handleChange} />
                      {errors&& issubmited && <span className="errorvalidation">{errors.underwriter_concession_amount}</span>}

                  </div>
              </div>
              <div className="row" style={{marginTop:30}}>

                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>

                  <label className="update-label">Underwriter(s)</label>
                    <input type="text" className="update-input" value={this.state.underwriters_list} ref={(Elmt)=>{this.underwriters_list=Elmt}} onChange={this.handleChange}/>


                </div>
                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                    <label className="update-label">Prospectus link</label>
                    <input type="text" className="update-input" value={this.state.prospectus_url} ref={(Elmt)=>{this.prospectus_url=Elmt}} onChange={this.handleChange}/>
                    {errors&& issubmited && <span className="errorvalidation">{errors.prospectus_url}</span>}

                </div>
              </div>
              <div className="row">
              <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <div className="row" >
                    <div className="col-12 col-sm-12" style={{ padding: '5px 20px 5px 15px', color: 'rgba(0, 0, 0, 0.3)' }}>
                      <label className="update-label"> Availability</label>

                    </div>
                    <div className="col-12 col-sm-12" style={{ padding: '5px 20px 5px 20px' }}>
                      <div className="row">

                          {/*<Field
                            name="available_to_order"
                            component={Checkbox}
                            autoComplete="off"
                          />*/}
                        <div className="col-10 col-sm-10">
                        <input id="orderrable" type="checkbox" className="update-input" checked={this.state.available_to_order} ref={(Elmt)=>{this.available_to_order=Elmt}} onChange={this.toggleChange} style={{height:"40px",width:"40px",outline:"3px solid blue"}} />

                          <sapn style={{paddingLeft:15}}>Available to order</sapn>
                        </div>

                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-6 col-sm-12" style={{ padding: '10px 20px 10px 20px' }}>
                    { /*<div className="row">
                      <div className="col-12 col-sm-12" style={{ padding: '5px 0px 5px 0px', color: 'rgba(0, 0, 0, 0.3)' }}>
                        Prospectus Version
                      </div>
                      <div className="col-12 col-sm-12" style={{ padding: '5px 0px 5px 0px' }}>
                        <div className="row">
                          <div className="col-1 col-sm-1" >
                            <Field
                              name="prospectus_version"
                              component={Checkbox}
                              autoComplete="off"
                            />
                          </div>
                          <div className="col-10 col-sm-10">
                            Final version
                          </div>

                        </div>
                      </div>
                    </div>*/}
                </div>

              </div>




              <div className="row">
                <div className="col-8 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>

                    <div className="row">
                      <div className="col-12 col-sm-12" style={{ padding: '5px 0px 5px 0px', color: 'rgba(0, 0, 0, 0.3)' }}>
                      <input type="file" accept="image/*" onChange={this.handleChange} ref={(ref)=>{this.File=ref}} />
                      {this.state.previewLogo &&<img style={{width:150,marginTop:15}} src={this.state.previewLogo} />}
                    {/* <Field
                          name="logo"
                          component={renderDropzoneInput}
                        />*/}


                      </div>
                    </div>

                </div>
                <div className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>

                </div>
              </div>
              <div className="row">
                <div className="col-8 col-sm-12" style={{ marginBottom: '30px' }}>
                  <RaisedButton type="submit" backgroundColor="#8DC73F" labelColor="#fff" style={{ marginTop: '30px', float:'right' }} disabled={submitting} label="Save" />
                </div>
                <div className="col-4 col-sm-12" style={{ marginBottom: '30px' }}>

                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

OfferingForm.propTypes = {
  handleSubmit: React.PropTypes.func,
  submitting: React.PropTypes.bool,
  handleFormSubmit: React.PropTypes.func,
  dispatch: React.PropTypes.func,
  offeringTypes: React.PropTypes.any,
  industries: React.PropTypes.any,
  UserMpid: React.PropTypes.any,
  history: React.PropTypes.object,
  offeringSuccsess:React.PropTypes.any
};

const ClickIPOCreateOfferingForm = reduxForm({
  form: 'ClickIPOCreateOfferingForm',
  validate,
  destroyOnUnmount: false,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
  touchOnBlur: false,
})(OfferingForm);

const mapStateToProps = createStructuredSelector({
  offeringTypes: makeSelectOfferingTypes(),
  industries: makeSelectIndustries(),
  UserMpid:makeSelectUserMpid(),
  UnderwriterCommssion:makeSelectUserMpidCommision(),
  offeringSuccsess:makeCreateOfferingSuccess(),
});


function mapDispatchToProps(dispatch, ownProps) {
  const urlId = ownProps.match.params.id;
  return {
    handleFormSubmit: (data,typeId) => {
      dispatch(submitCreateOfferingForm(data,typeId));
    },
    loadUserMpid:()=>dispatch(fetchUserMpid()),
    fetchCommission:(name)=>dispatch(fetchUserMpidCommision(name)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ClickIPOCreateOfferingForm);
