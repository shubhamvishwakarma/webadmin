import {
  FETCH_CURRENT_OFFERINGS,
  FETCH_CURRENT_OFFERINGS_DATA,

  SUBMIT_CREATE_OFFERING_REQUEST,
  SUBMIT_CREATE_OFFERING_SUCCESS,

  CURRENT_OFFERINGS_PAGINATION,

  FETCH_OFFERING_TYPES,
  FETCH_OFFERING_TYPES_DATA,

  FETCH_INDUSTRIES,
  FETCH_INDUSTRIES_DATA,

  SUBMIT_CREATE_OFFERING_FORM,
  CREATE_OFFERING_SUCCESS,
  CREATE_OFFERING_FAILED,

  FETCH_UPDATE_OFFERING,
  SUBMIT_UPDATE_OFFERING_FORM,
  UPDATE_OFFERING_SUCCESS,
  UPDATE_OFFERING_FAILED,
  ARCHIVE_OFFERING,
  ARCHIVE_OFFERING_SUCCESS,
  FETCH_USER_MPID,
  FETCH_USER_MPID_SUCCESS,
  FETCH_USER_MPID_COMMISION,
  FETCH_USER_MPID_COMMISION_SUCCESS,
  FINAL_PROSPECTUS_OFFERING,
  SEND_PROSPECTUS_OFFERING,
  UPDATE_PROSPECTUS_OFFERING,
  FETCH_OFFERING_ORDERS_SUCCESS,
  FETCH_OFFERING_ORDERS,
  UNARCHIVE_OFFERING,
  UNARCHIVE_OFFERING_SUCCESS,
  SIXTYMINUTEEMAIL,
  SIXTYMINUTEEMAIL_SUCCESS,
  UPDATE_OFFERINGS_SUCCESS,
  UPDATE_OFFERINGS_RESPONSE,
} from './constants';






export function updateOfferingRes(data) {
  return {
    type: UPDATE_OFFERINGS_RESPONSE,
    data
  };
}



export function updateOfferingSuccess(data) {
  return {
    type: UPDATE_OFFERINGS_SUCCESS,
    data
  };
}



export function updateOfferingFailed() {
  return {
    type: UPDATE_OFFERING_FAILED,
  };
}

export function submitUpdateOffering(data,state) {
  return {
    type: SUBMIT_UPDATE_OFFERING_FORM,
    data,
    state
  };
}

export function fetchUpdateOffering(id) {
  return {
    type: FETCH_UPDATE_OFFERING,
    id
  };
}
export function fetchIndustries(status = null) {
  return {
    type: FETCH_INDUSTRIES,
    status,
  };
}

export function industriesFetchData(data) {
  return {
    type: FETCH_INDUSTRIES_DATA,
    data,
  };
}

export function fetchCurrentOfferings(status = null) {
  return {
    type: FETCH_CURRENT_OFFERINGS,
    status,
  };
}

export function currentOfferingFetchData(data) {
  return {
    type: FETCH_CURRENT_OFFERINGS_DATA,
    data,
  };
}


export function fetchOfferingTypes(status = null) {
  return {
    type: FETCH_OFFERING_TYPES,
    status,
  };
}

export function fetchOfferingTypesData(data) {
  return {
    type: FETCH_OFFERING_TYPES_DATA,
    data,
  };
}

export function submitCreateOfferingRequest(data,typeId) {
  return {
    type: SUBMIT_CREATE_OFFERING_REQUEST,
    data,
    typeId
  };
}

export function submitCreateOfferingSuccess() {
  return {
    type: SUBMIT_CREATE_OFFERING_SUCCESS,
  };
}

export function currentOfferingNextPage(data) {
  return {
    type: CURRENT_OFFERINGS_PAGINATION,
    data,
  };
}

export function submitCreateOfferingForm(data,typeId) {
  return {
    type: SUBMIT_CREATE_OFFERING_FORM,
    data,
    typeId
  };
}

export function createOfferingSuccess(data) {
  return {
    type: CREATE_OFFERING_SUCCESS,
    data
  };
}

export function createOfferingFailed() {
  return {
    type: CREATE_OFFERING_FAILED,
  };
}

export function archiveOffering(id) {
  return {
    type: ARCHIVE_OFFERING,
    id
  };
}

export function archiveOfferingSuccess(data, ext_id) {
  return {
    type: ARCHIVE_OFFERING_SUCCESS,
    data,
    ext_id
  };
}
export function fetchUserMpid() {
  return {
    type: FETCH_USER_MPID,

  };
}
export function fetchUserMpidSuccess(data) {
  return {
    type: FETCH_USER_MPID_SUCCESS,
    data,
  };
}

export function fetchUserMpidCommision(name) {
  return {
    type: FETCH_USER_MPID_COMMISION,
    name
  };
}
export function fetchUserMpidCommisionSuccess(data) {
  return {
    type: FETCH_USER_MPID_COMMISION_SUCCESS,
    data
  };
}

export function finalProspectusOffering(id, prospectus_url) {
  return {
    type: FINAL_PROSPECTUS_OFFERING,
    id,
    prospectus_url
  };
}

export function sendProspectusOffering(ext_id, prospectus_url) {
  return {
    type: SEND_PROSPECTUS_OFFERING,
    ext_id,
    prospectus_url
  };
}
export function updateProspectusOffering(ext_id,prospectus_url){
  return {
    type: UPDATE_PROSPECTUS_OFFERING,
    ext_id,
    prospectus_url
  };
}

export function getnewOfferingSuccess(data) {
  return {
    type: FETCH_CURRENT_OFFERINGS_DATA,
    data,
  };
}


export function fetchOfferingOrder(id) {
  return {
    type: FETCH_OFFERING_ORDERS,
    id
  };
}
export function offeringOrderSuccess(data) {
  return {
    type: FETCH_OFFERING_ORDERS_SUCCESS,
    data,
  };
}

export function unarchiveOffering(id) {
  return {
    type: UNARCHIVE_OFFERING,
    id
  };
}

export function unarchiveOfferingSuccess(data, ext_id) {
  return {
    type: UNARCHIVE_OFFERING_SUCCESS,
    data,
    ext_id
  };
}


export function sixtyMinuteEmailAllocation(ext_id) {
  return {
    type: SIXTYMINUTEEMAIL,
    ext_id
  };
}
export function makeSixtyMinuteSuccess(data) {
  return {
    type: SIXTYMINUTEEMAIL_SUCCESS,
    data,
  };
}











