import React ,{ Component } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { connect } from 'react-redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';

class ArchiveModal extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      isSubmit:null,
      width:300
    }
  }

  archiveOffering(id){
    this.props.onClose()
    this.props.archiveOffering(id);
  }
  unArchiveOffering(id){
    this.props.onClose()
    this.props.unarchiveOffering(id);
  }

  render() {
    const {archiveConfirmshow,offeringInfo,archiveStatus}=this.props;


    const margin = {top: 20, right: 20, bottom: 30, left: 40};
    if(!this.props.show) {
      return null;
    }


      return (

        <div className="backdrop" style={{height:"100%", position: 'fixed',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: 'rgba(0,0,0,0.3)',
            padding: 10,
            zIndex:1}}>
        <div style={{height:"100%"}}  onClick={this.props.onClose}>
        </div>

          <div className="modal" style={{height:300 ,backgroundColor: '#fff',
            borderRadius: 5,
            maxWidth: 300,
            minHeight: 300,
            position:'absolute',
            margin: 'auto',
            padding: 15,display: 'block',top: '15px'}}>
              <div className="modalheader">
                <p style={{float:'left',color:"rgb(103, 136, 153)",fontSize:18}}> </p>
                <button className="closeBtn float_right" onClick={this.props.onClose}>
                    <span aria-hidden="true" >&times;</span>
                </button>
              </div>
            <div>
               <div className="row performanceModelhead" >
                    <p className="nomagnin text_center">Confirmation </p>
                </div>
                <div  className=" text_center">
                    <p className="nomagnin text_center confirmationboxText">Are you sure you want to {archiveStatus} <span className="confirmBoxModelOfferingName">{offeringInfo.name}</span></p>
                    <p className="nomagnin text_center confirmationboxText">All the active orders will be cancelled and customers will recieve a cancelledation email</p>
                    {archiveStatus=='archive' &&<button className="btn btn-archiveSubmit" onClick={()=>{this.archiveOffering(offeringInfo.ext_id)}} > Archive</button>}
                    {archiveStatus=='unarchive' &&<button className="btn btn-archiveSubmit" onClick={()=>{this.unArchiveOffering(offeringInfo.ext_id)}} > Unarchive</button>}
                </div>

            </div>
          </div>
        </div>
      )
    }
}

ArchiveModal.propTypes = {
  onClose : PropTypes.func.isRequired,
  show: PropTypes.bool,
  offeringInfo:PropTypes.any,
  handleMouseOver: PropTypes.func,
  handleMouseLeave: PropTypes.func,
  archiveStatus:PropTypes.any,
};
export default ArchiveModal;


