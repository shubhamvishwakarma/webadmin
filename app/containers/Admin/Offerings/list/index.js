/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import RaisedButton from 'material-ui/RaisedButton';
import { Link } from 'react-router-dom';
import {Calendar} from 'primereact/components/calendar/Calendar';

import AdminWrapper from 'components/AdminWrapper';
import { makeSelectCurrentOfferingTableView ,makeSelectupdateOffering} from '../selectors';
import TableView from '../tableView';

import {archiveOffering,finalProspectusOffering} from '../actions';
import {ToastContainer, ToastStore} from 'react-toasts';

class OfferingList extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      currentPage: 1,
      effectivePage: 1,
      searching:null,
      searchbyCat:'name',
      fromdate:null,
      todate:null
    };
    this.searchOffering=this.searchOffering.bind(this);
    this.dropDownChange=this.dropDownChange.bind(this);
  }


  searchOffering(event){
      this.setState({
        searching:event.target.value
      })
  }
  dropDownChange(event){
      this.setState({
        searchbyCat:event.target.value,
        searching:null,
        fromdate:null,
        todate:null
      })
  }


  render() {
      const { currentOfferingTableView } = this.props;
      const {searchbyCat} =this.state;

      let currentOfferingTableViewList=[];
      let effectiveOfferingTableViewList=[];
      let archiveOfferingTableViewList=[];
      let  filterData=[];
      if(this.state.searchbyCat=="name" || this.state.searchbyCat=="availableOrder" || this.state.searchbyCat=="date"){
        filterData = this.state.searching? currentOfferingTableView.filter(row => row.name.toLowerCase().indexOf(this.state.searching.toLowerCase()) > -1) : currentOfferingTableView;
      }else if (this.state.searchbyCat=="ext_id"){
        filterData = this.state.searching? currentOfferingTableView.filter(row => row.ext_id.toLowerCase().indexOf(this.state.searching.toLowerCase()) > -1) : currentOfferingTableView;
      }


      if(filterData && filterData.length>0){
        for(let item of filterData){
            if(this.state.searchbyCat=="availableOrder"){
                if(item.available_to_order==true){
                    if(item.status==='active' || item.status==='upcoming'){
                        currentOfferingTableViewList.push(item);
                    } if(item.status==='closed'){
                        effectiveOfferingTableViewList.push(item);
                    } if(item.status==='cancelled'){
                        archiveOfferingTableViewList.push(item);
                    }
                }
            }else{

              if(this.state.fromdate && this.state.todate){
                if(item.status==='active' || item.status==='upcoming'){
                    if(item.anticipated_date){
                      if(new Date(item.anticipated_date) >= new Date(this.state.fromdate) && new Date(item.anticipated_date) <= new Date(this.state.todate)){
                          currentOfferingTableViewList.push(item);
                      }
                    }
                }if(item.status==='cancelled'){
                     if(item.anticipated_date){
                      if(new Date(item.anticipated_date) >= new Date(this.state.fromdate) && new Date(item.anticipated_date) <= new Date(this.state.todate)){
                          archiveOfferingTableViewList.push(item);
                      }
                    }
                  }if(item.status==='closed'){
                      if(item.effective_date){
                        if(new Date(item.effective_date) >= new Date(this.state.fromdate) && new Date(item.effective_date) <= new Date(this.state.todate)){
                              effectiveOfferingTableViewList.push(item);
                        }
                      }
                  }
              }else{
                 if(item.status==='active' || item.status==='upcoming'){
                      currentOfferingTableViewList.push(item);
                  } if(item.status==='closed'){
                      effectiveOfferingTableViewList.push(item);
                  } if(item.status==='cancelled'){
                      archiveOfferingTableViewList.push(item);
                  }
              }
            }
        }
      }
      const Header = (<div className="offer_head">Offerings</div>);
      const Search =(<div className="search inline">
                      <div className="search_icon"></div>
                      <input id="searchInput" type="text" placeholder="Search" onChange={this.searchOffering} />
                      <div className="search_close" onTouchTap={this.getAllOfferings}></div>
                  </div>)
    return (
        <div>
          <ToastContainer store={ToastStore}/>
            <div className="main_outer_wrapper">
                  <div className="row offeringSubheader">
                      <div className="col-sm-6">
                          <div className="offering_head">Offering</div>
                      </div>
                      <div className="col-sm-6">
                          <div className=" offeringGlobalSearch">
                          <div className="filterbyContent">Search By</div>
                            <select className="selectField"  value={this.state.filterBy} onChange={this.dropDownChange} >
                                <option value="name">Name</option>
                                <option value="ext_id">EXT</option>
                                <option value="date">Date</option>
                                <option value="availableOrder">Available to order</option>


                            </select>
                            {searchbyCat != "date" && <div >
                              <input id="searchInput" type="text" placeholder="Search" onChange={this.searchOffering} />
                            </div>}
                            {searchbyCat =="date" &&<div className="calenderArea">
                              <Calendar placeholder="Date From" value={this.state.fromdate} showButtonBar={true} onChange={(e) => this.setState({fromdate: e.value})}></Calendar>
                              <Calendar placeholder="Date To" value={this.state.todate} showButtonBar={true} onChange={(e) => this.setState({todate: e.value})}></Calendar>
                              </div>
                            }
                            <Link to="/admin_offerings/create">
                              <button className="btn btn-newoffering">New Offering</button>
                            </Link>
                        </div>
                      </div>

                  </div>
                  <div >
                      <div className="offeringtabs">
                        <input type="radio" name="tabs" id="tab-broker-dealers" defaultChecked="checked" />
                        <label htmlFor="tab-broker-dealers" className="tab-left-label" style={{marginLeft:12}}>CURRENT</label>
                          <div className="tab">
                            <div className="row">
                              {currentOfferingTableView && <TableView offeringTableView={currentOfferingTableViewList} />}
                          </div>
                        </div>
                        <input type="radio" name="tabs" id="tab-broker-dealers-pending" />
                        <label htmlFor="tab-broker-dealers-pending" className="tab-left-label">EFFECTIVE</label>
                        <div className="tab">
                            <div className="row">
                                {effectiveOfferingTableViewList && <TableView offeringTableView={effectiveOfferingTableViewList} />}
                            </div>
                        </div>
                        <input type="radio" name="tabs" id="tab-archive-offering" />
                        <label htmlFor="tab-archive-offering" className="tab-left-label">Archive</label>

                          <div className="tab">
                            <div className="row">
                                 {archiveOfferingTableViewList && <TableView offeringTableView={archiveOfferingTableViewList} />}
                            </div>
                        </div>
                      </div>
                  </div>
            </div>
        </div>

    );
  }
}


OfferingList.propTypes = {
  currentOfferingTableView: PropTypes.any,
  currentUser: PropTypes.object,
};

function mapDispatchToProps(dispatch) {
  return {

  };
}

const mapStateToProps = createStructuredSelector({
  currentOfferingTableView: makeSelectCurrentOfferingTableView(),
  updateOfferingRes:makeSelectupdateOffering(),
});



export default connect(mapStateToProps, mapDispatchToProps)(OfferingList);
