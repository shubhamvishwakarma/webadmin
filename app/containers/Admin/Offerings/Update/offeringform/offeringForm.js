import React from 'react/lib/React';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { reduxForm, Field, change } from 'redux-form/immutable';
import { TextField, SelectField, Checkbox } from 'redux-form-material-ui';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'redux-form-material-ui/lib/DatePicker';
import renderDropzoneInput from 'components/FileField';
import RaisedButton from 'material-ui/RaisedButton';
import { withRouter } from 'react-router-dom';
import { mmddyy, parseTime } from 'containers/App/Helpers';
import validate from './formValidation';
import moment from 'moment';
import { makeSelectOfferingTypes, makeSelectIndustries, makeSelectupdateOffering ,makeSelectUserMpid ,makeSelectUserMpidCommision,makeupdatedOfferingRes} from '../../selectors';
import { ToastContainer, ToastStore } from 'react-toasts';
import { submitCreateOfferingForm,fetchUpdateOffering,submitUpdateOffering,fetchOfferingTypes,fetchUserMpid,fetchUserMpidCommision} from '../../actions';
import Select from 'react-select';

import {Calendar} from 'primereact/components/calendar/Calendar.js';


let max = 0;
let min = 0;
let savedData;


class OfferingForm extends React.PureComponent {

  constructor(props) {
    super(props);
    let today = new Date();
    let month = today.getMonth();
    let year = today.getFullYear();
    let prevMonth = (month === 0) ? 11 : month - 1;
    let prevYear = (prevMonth === 11) ? year - 1 : year;


    let minDate = new Date();
    minDate.setMonth(prevMonth+1);
    minDate.setFullYear(prevYear);


    this.state = {
      warning: false,
      message: '',
      lastClosing:HTMLMarqueeElement,
      name:'',
      ticker_symbol:'',
      cusip_id:'',
      type:'',
      offering_type_id:'',
      max_price:'TBD',
      min_price:'TBD',
      final_shares:'TBD',
      description:'',
      industry:'',
      prospectus_url:'',
      underwriters_list:'',
      category_ids:'',
      trade_date:'TBD',
      max_date:'TBD',
      final_version:false,
      available_to_order:false,
      max_dateError:false,
      trade_dateError:false,
      issubmited:false,
      final_price:0,
      // isOrderable:false,
      UnderwriterValue:'',
      UnderwriterId:'',
      selectUserMpId:'',
      selected_bd_underwriter_mpid_id: '',
      bd_underwriter_mpid_id:'',
      underwriter_concession_amount:'',
      selected_industry:'',
      uiRender:null,
      minDate: minDate,
      max_date_show:'',
      trade_date_show:'',
      previewLogo:'',
      isApproxDateChecked: false,
      isSettlementDateChecked: false,
      fileID:'',
      isLoad:true


    };
    this.handleFormSubmit = this.handleFormSubmit.bind(this);

    this.handleChange=this.handleChange.bind(this);
    this.mpIDhandleChange = this.mpIDhandleChange.bind(this);
    this.industryhandleChange=this.industryhandleChange.bind(this);
    this.settleMentDateChange=this.settleMentDateChange.bind(this);
    this.ApproxDateChange=this.ApproxDateChange.bind(this);

  }


  componentDidMount() {

    let id=this.props.match.path.split("edit/")[1];
    this.props.getOfferingDetails(id);
    this.props.getOfferingTypes();
    this.props.loadUserMpid();

  }


  industryhandleChange(event){
    this.setState({
      selected_industry:event,
      industry:event.value
    })
  }
  handleUserMpid(value){
   this.setState({
    UnderwriterValue:'',
    UnderwriterId:''
   })
    this.selectUserMpId=value;
  }
  handleUpdateCommission(value){
    this.setState({
      UnderwriterValue:value,
    })
  }



  componentWillReceiveProps(nextProps) {
    if(nextProps.offer){
      if(this.state.isLoad){     
          this.setState({
               offering_type_id: nextProps.offer.offering_type_id,
               type: nextProps.offer.offering_type_name,
               isLoad: false,
          })


          if(nextProps.offer.offering_price_attributes.max_price){
              this.setState({
                max_price:nextProps.offer.offering_price_attributes.max_price
              })
          }
          if(nextProps.offer.offering_price_attributes.min_price){
              this.setState({
                min_price:nextProps.offer.offering_price_attributes.min_price
              });
          }

           if(nextProps.offer.final_shares){
              if(nextProps.offer.final_shares==null || nextProps.offer.final_shares==0){
                  this.setState({
                      final_shares:'TBD'
                  })
              }else{
                  this.setState({
                      final_shares:nextProps.offer.final_shares
                  })
              }
            }

            if(nextProps.offer.final_price){
              if(nextProps.offer.final_price==null || nextProps.offer.final_price==0){
                  this.setState({
                      final_price:'0'
                  })
              }else{
                  this.setState({
                      final_price:nextProps.offer.final_price
                  })
              }
            }

            if(nextProps.offer.underwriter_concession_amount){
                if(nextProps.offer.underwriter_concession_amount==null){
                    this.setState({
                        underwriter_concession_amount:''
                    })
                }else{
                    this.setState({
                        underwriter_concession_amount:nextProps.offer.underwriter_concession_amount
                    })
                }
            }

            if(nextProps.offer.bd_underwriter_mpid_id ) {
                const selectedMPIDObject = {
                  value: nextProps.offer.bd_underwriter_mpid_id,
                  label: nextProps.offer.bd_underwriter_mpid_id
                }
                this.setState({
                  selected_bd_underwriter_mpid_id: selectedMPIDObject,
                  bd_underwriter_mpid_id:nextProps.offer.bd_underwriter_mpid_id
                });
            }

            if(nextProps.offer.underwriters_list){
               this.setState({underwriters_list:nextProps.offer.underwriters_list})
            }



            if(nextProps.offer.offering_date_attributes){
              if(nextProps.offer.offering_date_attributes.max_date){
                this.setState({
                  max_date:nextProps.offer.offering_date_attributes.max_date,
                  max_date_show: new Date(nextProps.offer.offering_date_attributes.max_date),
                  isApproxDateChecked:false,
                })
              }else{
                  this.setState({
                    isApproxDateChecked:true,
                    max_date:'',
                    max_date_show:''
                    // isSettlementDateChecked: true
                  })
              }

              if(nextProps.offer.offering_date_attributes.trade_date){
                  this.setState({
                    trade_date:nextProps.offer.offering_date_attributes.trade_date,
                    trade_date_show: new Date(nextProps.offer.offering_date_attributes.trade_date),
                    isSettlementDateChecked:false,
                  })

              }else{
                  this.setState({
                    // isApproxDateChecked:true,
                    isSettlementDateChecked: true,
                    trade_date:'',
                    trade_date_show:''
                  })
              }
            }

            if(Array.isArray(nextProps.offer.industries)){
              if(nextProps.offer.industries.length>0){
                if(this.props.industries){
                  for(let items of this.props.industries){
                    if(nextProps.offer.industries[0].name == items.name){
                      const selectedIndustryObject = {
                        value: items.id,
                        label: nextProps.offer.industries[0].name
                      }
                      this.setState({
                        selected_industry: selectedIndustryObject,
                        industry:items.id
                      });
                    }
                  }
                }

              }
            }
         }
      }
  }

  handleChange(event){

    this.setState({
      issubmited: true,
      type:this.state.type,
      max_date:this.state.max_date,
      trade_date:this.state.trade_date,
      industry:this.state.industry,
      offering_type_id:this.state.offering_type_id,
      category_ids:(this.props.industries.filter((item)=>{return item.name==this.state.selected_industry.label}))[0].id,
      File:this.File.files,
      bd_underwriter_mpid_id:this.state.bd_underwriter_mpid_id,
      selected_bd_underwriter_mpid_id: this.state.selected_bd_underwriter_mpid_id,
    })
    if(this.File.value){
      this.setState({
        previewLogo:URL.createObjectURL(this.File.files[0]),
      })

    }


  }

  mpIDhandleChange(event){
      this.setState({
        selected_bd_underwriter_mpid_id: event,
        bd_underwriter_mpid_id: event.value
      })
  }

  handleFormSubmit(formData) {
    const formattedData = formData.toJS()
    //we are going to prepare the object we are sending to the backend for submitting, no need to send unncesary info out of this component
    //need to change the two dropdowns to redux-form way, currently we are setting the value via state, we have to change to use redux-form
      var offering_date_attributes_ = {};
      if (this.state.isApproxDateChecked==true) {
          offering_date_attributes_.max_date ="TBD"
      }else{
          offering_date_attributes_.max_date =this.state.max_date;
      }

      if (this.state.isSettlementDateChecked==true) {
          offering_date_attributes_.trade_date ="TBD"
      }else{
          offering_date_attributes_.trade_date =this.state.trade_date;
      }


      const dataToAPI = {
        offering_type_id: this.state.offering_type_id,
        ext_id: formattedData.ext_id,
        name: formattedData.name,
        ticker_symbol: formattedData.ticker_symbol,
        cusip_id: formattedData.cusip_id,
        offering_price_attributes: formattedData.offering_price_attributes,
        offering_date_attributes: offering_date_attributes_,
        final_shares: formattedData.final_shares,
        category_ids: this.state.industry,
        description: formattedData.description,
        bd_underwriter_mpid_id: this.state.bd_underwriter_mpid_id,
        underwriter_concession_amount: formattedData.underwriter_concession_amount,
        underwriters_list: formattedData.underwriters_list,
        available_to_order: formattedData.available_to_order,
        offering_type_name: this.state.type,
        logo: this.File.files[0]
      }
      // console.log('this is the data to API: ', dataToAPI);

       this.props.handleFormSubmit(dataToAPI,this.state);
  }



  _SelectedofferingType(event){
     this.setState({
       type:event.name,
       offering_type_id:event.id,
       issubmited:true
     })

  }

  settleMentDateChange(){
    if(this.state.isSettlementDateChecked==false){
      this.setState({
        isSettlementDateChecked: true,
        trade_date:'',
        trade_date_show:null,
        issubmited:true

      });
    }else{
      this.setState({
        isSettlementDateChecked: false,
        issubmited:true
      });
    }
  }
  selectSettlementDate(e){
    if(e.value){
      this.setState({
        trade_date: moment(e.value).format('MM/DD/YYYY hh:mm:ss'),
        trade_date_show:e.value,
        isSettlementDateChecked: false,
        issubmited:true
      })
    }else{
      this.setState({
        trade_date:'',
        isSettlementDateChecked: false,
        issubmited:true
      })
    }
  }





  selectApproxDate(e){
    if(e.value){
      var settlementDate = new Date(e.value);
      settlementDate.setDate(e.value.getDate()+2);
      this.setState({
        max_date: moment(e.value).format('MM/DD/YYYY hh:mm:ss'),
        trade_date: moment(settlementDate).format('MM/DD/YYYY hh:mm:ss'),
        max_date_show: e.value,
        trade_date_show:settlementDate,
        isApproxDateChecked: false,
        isSettlementDateChecked: false,
        issubmited: true
      })
    }else{
      this.setState({
        max_date:'',
        isApproxDateChecked: false,
        issubmited: true
      })
    }
    // this.handleChange();
  }

  ApproxDateChange(){
    if(this.state.isApproxDateChecked==false){
      this.setState({
        isApproxDateChecked: true,
        max_date:'',
        max_date_show:null,
        isSettlementDateChecked: true,
        trade_date:'',
        trade_date_show:null,
        issubmited: true

      });
    }else{
      this.setState({
        isApproxDateChecked: false,
        issubmited: true
      });
    }
    // this.handleChange();
  }
  goBackOffering(){
    this.props.history.goBack()
  }

  render() {

    const { warning, message } = this.state;
      const { handleSubmit, submitting, updatedData, offeringTypes, industries,offer ,UserMpid,history,updatedOfferingRes} = this.props;
      if(updatedOfferingRes){
        this.goBackOffering();
      }


    let CurrentOffer={};
    let options=<option></option>;
    let indoptions=<option></option>;
    /*MinMax*/
      let MinMax=(this.state.type!='IPO')? (
        <div  className="col-12 col-sm-12 form-group has-feedback has-Dollar" style={{ padding: '0px 70px  0px 20px' }}>
            <label className="update-label" htmlFor="offering_price_attributes.max_price">Last closing price</label>
            <span className="fa fa-dollar form-control-feedback"></span>
            <Field style={{width: '100%'}} name='offering_price_attributes.max_price'  className="update-input" type="text"  component='input' />

        </div>) :( <div >
            <div className="col-6 col-sm-12 form-group has-feedback has-Dollar" style={{ padding: '0px 20px 0px 20px' }}>
              <label className="update-label"  htmlFor="offering_price_attributes.min_price">Min Price</label>
              <span className="fa fa-dollar form-control-feedback"></span>
              <Field name='offering_price_attributes.min_price'  className="update-input" type="text"  component='input' />

            </div>

            <div className="col-6 col-sm-12 form-group has-feedback has-Dollar" style={{ padding: '0px 20px 0px 20px' }}>
            <label className="update-label"  htmlFor="offering_price_attributes.max_price">Max Price</label>
            <span className="fa fa-dollar form-control-feedback"></span>
            <Field  name='offering_price_attributes.max_price'  className="update-input" type="text"  component='input' />

          </div>

        </div>
    )




      let UserMpidList=[];
      let typeAheadUserMpidList=[];
      if(UserMpid && UserMpid.length>0) {
        UserMpidList=UserMpid.map((item)=>{
          typeAheadUserMpidList.push({value:item.executing_broker_mpid,label:item.executing_broker_mpid})

           if(offer.bd_underwriter_mpid_id!==undefined){
              if(item.executing_broker_mpid==offer.bd_underwriter_mpid_id){
                  return <option key={item.executing_broker_mpid} selected value={item.executing_broker_mpid}>{item.executing_broker_mpid}</option>
              }
            }
            return <option key={item.executing_broker_mpid} value={item.executing_broker_mpid}>{item.executing_broker_mpid}</option>

          })
      }

     if(offeringTypes && offer)
     {
       if(offeringTypes.length>0)
        {

        options=offeringTypes.map((item)=>{
          if(item.name==offer.offering_type_name)
            {
              return <option key={item.id} selected value={item.name}>{item.name}</option>
            }

              return <option key={item.id} value={item.name}>{item.name}</option>


        })
      }
     }

     let offeringTypesRender=[];
     if(offeringTypes && offer){


        if(offeringTypes && offeringTypes.length>0){

          offeringTypesRender=offeringTypes.map((item,i)=>{
              return (<div className="col-6 col-sm-12" style={{marginTop:5}} key={i}>
                  { item.id===this.state.offering_type_id &&<RaisedButton backgroundColor="#8DC73F" labelColor="#fff" label={item.name} style={{width:'90%'}}  onClick={()=>{this._SelectedofferingType(item)}} />}
                  { item.id!==this.state.offering_type_id &&<RaisedButton  labelColor="#000" label={item.name} style={{width:'90%'}}   onClick={()=>{this._SelectedofferingType(item)}} />}
                </div>)
          })
        }
     }
     let indusnewOption=[];
     if(industries && offer)
     {
       if(offer.industries)
       {
        if(industries.length>0){
        indoptions=industries.map((item)=>{
          indusnewOption.push({value:item.id,label:item.name})
          if(offer.industries[0]!==undefined)
          {
                if(item.name==(offer.industries[0].name))
                  {
                    return <option key={item.id} selected value={item.name}>{item.name}</option>
                  }
          }

              return <option key={item.id} value={item.name}>{item.name}</option>


            })
          }
      }
    }
    if (offer) {
      CurrentOffer=offer;
    }

    let Cusip =[];
    if (offer) {
      Cusip =
        <div>
          { offer.cusip_id && <input type="text" className="update-input" defaultValue={this.state.cusip_id} ref={(Elmt)=>{this.cusip=Elmt}} onChange={this.handleChange}  readOnly/>}
          { !offer.cusip_id &&<input type="text" className="update-input" value={this.state.cusip_id} ref={(Elmt)=>{this.cusip=Elmt}} onChange={this.handleChange}/>}
        </div>
    }

    savedData = updatedData;
    return (
      <div >
        <ToastContainer store={ToastStore}/>

          <div className="offering_Detail_header">
              <div onTouchTap={() =>history.goBack()} >
                <div className="back_icon"></div>
              </div>
              <div className="Headingname">
                Update offering
              </div>
          </div>
        <div style={{padding:'0px 50px 0px 50px'}}>
          <form className="row" onSubmit={handleSubmit(this.handleFormSubmit)}>
            <div style={{ padding: '0px 25px 0px 25px' }}>
              <div className="row">
                    { offeringTypesRender}
                </div>
              <div className="row" style={{marginTop:30}}>
                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <label htmlFor='name' className="update-label">Company Name</label>
                  <Field name='name' component='input' type='text' className="update-input"/>
                </div>
                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <label className="update-label" htmlFor='ticker_symbol'>Ticker Symbol</label>
                  <Field className="update-input" name='ticker_symbol' component='input' type='text' />
                </div>

              </div>
              <div className="row" style={{marginTop:30}}>
                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <label htmlFor='cusip_id' className="update-label">CUSIP ID</label>
                  <Field name='cusip_id' component='input' type='text' className="update-input"/>
                </div>
                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <label className="update-label" htmlFor='ext_id' >EXT ID/FILE ID</label>
                  <Field className="update-input" name='ext_id' component='input' type='text' />
                </div>
              </div>

              <div className="row" style={{marginTop:30}}>
                {MinMax}
              </div>
              <div className="row"  style={{marginTop:30}}>
                  <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                    <label className="update-label">Approximate Date/Time</label>

                    <div className="row">
                        <div className="col-sm-8" >
                         <Calendar minDate={this.state.minDate} value={this.state.max_date_show} onChange={(e) => this.selectApproxDate(e)} showTime={true} showSeconds={true} />

                        </div>
                        <div className="col-sm-4 update-label" style={{fontSize:20 ,paddingTop:10}}>
                          <input type="checkbox"  checked={this.state.isApproxDateChecked} onChange={this.ApproxDateChange}/> TBD
                        </div>

                      </div>


                    {/*<input type="text" className="update-input" value={this.state.max_date} ref={(Elmt)=>{this.max_date=Elmt}} onChange={this.handleChange} placeholder="MM/DD/YYYY HH:MM:SS"/>*/}
                  </div>
                  <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                    <label className="update-label">Settlement date / time</label>
                    <div className="row">
                      <div className="col-sm-8" >
                        <Calendar minDate={this.state.minDate} value={this.state.trade_date_show} onChange={(e) => this.selectSettlementDate(e)} showTime={true} showSeconds={true} />

                      </div>
                      <div className="col-sm-4 update-label" style={{fontSize:20 ,paddingTop:10}}>
                        <input type="checkbox"  checked={this.state.isSettlementDateChecked} onChange={this.settleMentDateChange}/> TBD
                      </div>
                    </div>
                    {/*<input  type="text" className="update-input" value={this.state.trade_date} ref={(Elmt)=>{this.trade_date=Elmt}} onChange={this.handleChange} placeholder="MM/DD/YYYY HH:MM:SS"/>*/}
                  </div>
              </div>

              <div className="row"  style={{marginTop:30}}>

                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <label htmlFor='final_shares' className="update-label">Listed Shares</label>
                  <Field className="update-input" name='final_shares' component='input' type='text' />
                </div>
                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                    <label className="update-label">Industries</label>
                    <Select
                        name="selected_industry"
                        value={this.state.selected_industry}
                        ref={(select)=>{this.industry=select}}
                        onChange={(event)=>this.industryhandleChange(event)}
                        options={indusnewOption}
                      />

                </div>

              </div>
              <div className="row"  style={{marginTop:30}}>
                <div className="col-12 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <label className="update-label">Description</label>
                  <Field className="update-input" name='description' component='textarea'  style={{ height: "100px", borderStyle: "groove", borderColor: "#ccc", width: '95%' }}/>
                </div>
              </div>
              <div className="row"  style={{marginTop:30}}>
                  <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>

                  <label className="update-label">Underwriter MPID</label>
                  <Select
                    name="selected_bd_underwriter"
                    id="bd_underwriter_mpid_id"
                    value={this.state.selected_bd_underwriter_mpid_id}
                    ref={(select) => { this.bd_underwriter_mpid_id = select }}
                    onChange={(event)=>this.mpIDhandleChange(event)}
                    options={typeAheadUserMpidList}
                  />
                </div>
                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <label htmlFor="underwriter_concession_amount" className="update-label">Underwriter concession</label>
                  <Field name="underwriter_concession_amount" component='input' type='text' className="update-input"/>
                </div>
              </div>

              <div className="row" style={{marginTop:30}}>
                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <label className="update-label">Underwriter(s)</label>
                  <Field className="update-input" name="underwriters_list" component='input' type='text'/>
                </div>
                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <Field name="available_to_order" component='input' type="checkbox" style={{ height: "40px", width: "40px", outline: "3px solid blue" }} className="update-input"/>
                  <label className="update-label" style={{paddingLeft: '10px'}}>Available Order</label>
                </div>
              </div>

              <div className="row"  style={{marginTop:30}}>
                <div className="col-6 col-sm-12" style={{ padding: '10px 20px 20px 20px' }}>
                  <div className="row">
                    <div className="col-6 col-sm-12" className="update-label">
                      <label className="update-label">Logo</label>
                    </div>
                    <div className="col-12 col-sm-12" style={{ padding: '5px 0px 5px 20px',color:"red" }}>
                      <input type="file" accept="image/*" onChange={this.handleChange} ref={(ref)=>{this.File=ref}} />
                      <div>

                      {this.state.previewLogo &&<img src={this.state.previewLogo} height="100" width="100" style={{float:"right", marginRight:"40%"}} />}
                      {!this.state.previewLogo &&<img src={ (offer) ? 'https:' + offer.logo_large : '' } height="100" width="100" style={{float:"right", marginRight:"40%"}}/>}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <RaisedButton type="submit" backgroundColor="#8DC73F" labelColor="#fff" style={{ marginTop: '30px' }}  label="Save" />
                </div>
              </div>

            </div>
          </form>
        </div>
      </div>
    );
  }
}

OfferingForm.propTypes = {
  handleSubmit: React.PropTypes.func,
  submitting: React.PropTypes.bool,
  handleFormSubmit: React.PropTypes.func,
  dispatch: React.PropTypes.func,
  offeringTypes: React.PropTypes.any,
  industries: React.PropTypes.any,
  offer:React.PropTypes.any,
  updatedOfferingRes:  React.PropTypes.any,
};

const ClickIPOupdateOfferingForm = reduxForm({
  form: 'ClickIPOupdateOfferingForm',
  validate,
  destroyOnUnmount: true,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
  touchOnBlur: false,
})(OfferingForm);

const mapStateToProps = createStructuredSelector({
  offer:makeSelectupdateOffering(),
  initialValues: makeSelectupdateOffering(),
  offeringTypes: makeSelectOfferingTypes(),
  industries: makeSelectIndustries(),
  UserMpid:makeSelectUserMpid(),
  UnderwriterCommssion:makeSelectUserMpidCommision(),
  updatedOfferingRes:makeupdatedOfferingRes(),
});


function mapDispatchToProps(dispatch, ownProps) {
  const urlId = ownProps.match.params.id;
  return {
    getOfferingDetails: (id) => { dispatch(fetchUpdateOffering(id)); },
    handleFormSubmit: (data,state) => {dispatch(submitUpdateOffering(data,state)); },
    getOfferingTypes:()=>{
      dispatch(fetchOfferingTypes())
    },
    loadUserMpid:()=>dispatch(fetchUserMpid()),
    fetchCommission:(name)=>dispatch(fetchUserMpidCommision(name)),

  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ClickIPOupdateOfferingForm));
