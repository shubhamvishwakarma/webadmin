/**
 * API calls for Ola
 */

import { call, put, all, takeLatest, select } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { BASE_URL } from 'containers/App/constants';
import { showGlobalLoader, closeGlobalLoader } from 'containers/App/actions';
import request from 'utils/request';
import * as actions from './actions';
import * as actionTypes from './constants';
import {store} from '../../../app'
import axios from 'axios';
import {ToastStore} from 'react-toasts';
import { LogStaging } from '../../App/Helpers';

function uploadLogo(url,data, config ){
  LogStaging(33)
    return axios.post(url,data, config );
}
const getToken = (state) => state.getIn(['global', 'currentUser', 'token']);

function* handleFetchUpdateOfferings(action) {
  LogStaging(34)
  yield put(showGlobalLoader());

  const TOKEN = yield select(getToken);

  const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });

  let URL;

  let q;
  let page;
  let offering;

  URL = `${BASE_URL}/offerings/${action.id}`;


  try {
    offering = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });

    yield put(actions.updateOfferingSuccess(offering));
  } catch (err) {

    if(err.response.status==401){
        localStorage.clear();
        window.location.href = '/login';
    }
    yield put(actions.updateOfferingFailed(err.error));
    console.log(err);
  } finally {
    yield put(closeGlobalLoader());
  }
}

function* WatcherFetchUpdateOffering() {
  yield takeLatest(actionTypes.FETCH_UPDATE_OFFERING, handleFetchUpdateOfferings);
}


function* handleFetchCurrentOfferings(action) {
  LogStaging(35)
  yield put(showGlobalLoader());


  const TOKEN = yield select(getToken);

  const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });

  let URL;

   URL = `${BASE_URL}/offerings`;

  let offerings;
  try {
    offerings = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
     yield put(actions.currentOfferingFetchData(offerings));
  } catch (err) {
    console.log(err);
    if(err.response.status==401){
      localStorage.clear();
      window.location.href = '/login';
    }

    yield put(actions.currentOfferingFetchData(err.error));
    console.log(err);
  } finally {
    yield put(closeGlobalLoader());
  }
}

function* WatcherFetchCurrentOfferings() {
  yield takeLatest(actionTypes.FETCH_CURRENT_OFFERINGS, handleFetchCurrentOfferings);
}



function* handleFetchOfferingTypes() {
  LogStaging(36)
   //yield put(showGlobalLoader());

  const TOKEN = yield select(getToken);

  const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });

  let URL;
    URL = `${BASE_URL}/offerings/types`;
  let offeringTypes;
  try {
    offeringTypes = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    if (offeringTypes) {
      yield put(actions.fetchOfferingTypesData(offeringTypes));
    }
  } catch (err) {
    if(err.response.status==401){
        localStorage.clear();
        window.location.href = '/login';
    }
    yield put(actions.fetchOfferingTypesData(err.error));
    console.log(err);
  } finally {
    // yield put(closeGlobalLoader());
  }
}

function* WatcherFetchOfferingTypes() {
  yield takeLatest(actionTypes.FETCH_OFFERING_TYPES, handleFetchOfferingTypes);
}

function* handleFetchIndustries() {
  LogStaging(37)

  // yield put(showGlobalLoader());

  const TOKEN = yield select(getToken);

  const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });

  let URL;
    URL = `${BASE_URL}/offerings/industries`;
  let offeringTypes;
  try {
    offeringTypes = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    if (offeringTypes) {
      yield put(actions.industriesFetchData(offeringTypes));
    }
  } catch (err) {
    if(err.response.status==401){
      localStorage.clear();
      window.location.href = '/login';
    }
    yield put(actions.industriesFetchData(err.error));
    console.log(err);
  } finally {
    //yield put(closeGlobalLoader());
  }
}

function* WatcherFetchIndustries() {
  yield takeLatest(actionTypes.FETCH_INDUSTRIES, handleFetchIndustries);
}

function* handleCreateOffering(action) {
  LogStaging(38)
  yield put(showGlobalLoader());

  const TOKEN = yield select(getToken);

  const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });

      //var logo = action.data.get('logo');
      var logo = action.data.File;
      console.log('logo: ', logo)
      if (logo && logo[0]){
        logo = logo[0];
      } else {
        logo = null;
      }
      LogStaging(logo);

      //var formData = action.data.toJS();
      var formData = action.data;

      // if (formData.logo) {
      //   delete(formData.logo);
      // }

      var offering_date_attributes_ = {};
      if (formData.isApproxDateChecked==true) {
           offering_date_attributes_.max_date ="TBD"
      }else{
        offering_date_attributes_.max_date =formData.max_date;
      }
      if (formData.isSettlementDateChecked==true) {
           offering_date_attributes_.trade_date ="TBD"
      }else{
        offering_date_attributes_.trade_date =formData.trade_date;
      }

      console.log(offering_date_attributes_)

      // offering_date_attributes_.max_date = (formData.max_date=="TBD")?null:formData.max_date;
      // offering_date_attributes_.trade_date=(formData.trade_date=="TBD")?null:formData.trade_date;
      formData.offering_date_attributes = offering_date_attributes_;
      delete(formData.max_date);

      var offering_price_attributes_ = {};
      offering_price_attributes_.max_price = (formData.max_price=="TBD")?null:formData.max_price;

      formData.offering_price_attributes = offering_price_attributes_;
      offering_price_attributes_.min_price = (formData.min_price=="TBD")?null:formData.min_price;;
      delete(formData.max_price);
      delete(formData.min_price);
      formData.offering_type_id=action.typeId;

      var category_ids_ = [];
      if(formData.category_ids){
        category_ids_.push(formData.category_ids);
      }
      formData.category_ids = category_ids_;

      // var offering_share_attributes_ = {};
      // offering_share_attributes_.anticipated_shares = (formData.final_shares=="TBD")?null:formData.final_shares;
      // formData.offering_share_attributes = offering_share_attributes_;
      // delete formData.final_shares;
      formData.final_shares=(formData.final_shares=="TBD")?null:formData.final_shares;
      if(!formData.available_to_order){
        formData.available_to_order = false;
      }

      if(!formData.prospectus_version){
        formData.prospectus_version = false;
      }

  const BODY = JSON.stringify({
    ...formData,
  });

  const URL = `${BASE_URL}/offerings`;
  try {

    const offeringResponse = yield call(request, URL, {
      method: 'POST',
      headers: ajaxRequestHeaders,
      body: BODY,
    });
    if(logo&&offeringResponse.ext_id){
        let data = new FormData();
        data.append('logo',action.data.File[0])
        var url=`${BASE_URL}/offerings/${offeringResponse.ext_id}/logo`;
        var config = {
          headers: {
            'accept': 'application/json',
            'accept-language': 'en_US',
            'Authorization':TOKEN,
            'content-type': `multipart/form-data; boundary=${data._boundary} `
          }
        };
        const logoRes=yield call(uploadLogo,url,data, config)
          if(logoRes)
          {
              yield put(actions.createOfferingSuccess(logoRes.data));
              yield put(actions.createOfferingSuccess(null));
              yield put(actions.currentOfferingNextPage(logoRes.data));
          }
          else{

              ToastStore.error('Error while uploading Logo');
          }
    }

    else{
    yield put(actions.createOfferingSuccess(offeringResponse));
    yield put(actions.createOfferingSuccess(null));
    yield put(actions.currentOfferingNextPage(offeringResponse));
    }
   // window.location=window.location.origin+'/admin_offerings';
  } catch (err) {
    if(err.response.status==401){
        localStorage.clear();
        window.location.href = '/login';
    }
    ToastStore.error('Error while creating offer!!');
    yield put(actions.createOfferingFailed());
  } finally {

    yield put(closeGlobalLoader());
  }
}
function* watcherCreateOfferingForm() {
  yield takeLatest(actionTypes.SUBMIT_CREATE_OFFERING_FORM, handleCreateOffering);
}


function* handleUpdateOffering(action) {
  console.log('data that we have recieved from the form (action) : ', action)
  LogStaging(39)
  // console.log(action);
  // console.log(action.state.max_date.includes("T"));

   yield put(showGlobalLoader());

  const TOKEN = yield select(getToken);

  const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });

      var logo = action.data.logo
      if (logo){
        logo = logo[0];
      } else {
        logo = null;
         delete(action.data.logo);

      }
      console.log(logo)
     console.log(action.data);

/////Comment start///////

      // var Data = action.data.toJS();
      // console.log('toJS: ', Data)

      // if (Data.logo) {
      //   delete(Data.logo);
      // }

      // var offering_date_attributes_ = {};
      // offering_date_attributes_.max_date =(action.state.max_date.includes("T")?null:action.state.max_date);
      // offering_date_attributes_.trade_date =(action.state.trade_date.includes("T")?null:action.state.trade_date);


      // var offering_date_attributes_ = {};
      // if (action.state.isApproxDateChecked==true) {
      //      offering_date_attributes_.max_date ="TBD"
      // }else{
      //   offering_date_attributes_.max_date =action.state.max_date;
      // }

      // if (action.state.isSettlementDateChecked==true) {
      //      offering_date_attributes_.trade_date ="TBD"
      // }else{
      //     offering_date_attributes_.trade_date =action.state.trade_date;
      // }

      // var offering_price_attributes_ ={};
      // offering_price_attributes_.max_price=(action.state.max_price=="TBD")?null:action.state.max_price;
      // offering_price_attributes_.min_price=(action.state.min_price=="TBD")?null:action.state.min_price;
      // var offering_share_attributes_={}
      // // offering_share_attributes_.anticipated_shares=action.state.final_shares;

      // var formData={
      //     'name':action.state.name,
      //     'cusip_id':action.state.cusip_id,
      //     'ext_id':action.state.ext_id,
      //     'description':action.state.description,
      //     'ticker_symbol':action.state.ticker_symbol,
      //     'prospectus_url':action.state.prospectus_url,
      //     'underwriters_list':action.state.underwriters_list,
      //     'offering_type_id':action.state.offering_type_id,
      //     'category_ids':action.state.category_ids,
      //     'available_to_order':action.state.available_to_order,
      //     'final_version':action.state.final_version,
      //     'offering_date_attributes':offering_date_attributes_,
      //     'offering_price_attributes':offering_price_attributes_,
      //     'offering_share_attributes':offering_share_attributes_,
      //     'final_shares':(action.state.final_shares=="TBD")?null:action.state.final_shares,
      //     'underwriter_concession_amount':action.state.underwriter_concession_amount,
      //     'bd_underwriter_mpid_id':action.state.bd_underwriter_mpid_id
      // }
      // console.log(formData);
      // if(formData.ext_id)
      // {
      //   formData.ext_id==window.location.pathname.split('edit/')[1]
      // }
/////Comment end///////

      const URL = `${BASE_URL}/offerings/update_offering`;


      console.log('action.data: ', action.data)

      const BODY = JSON.stringify({
        ...action.data,
      });



  try {

    const offeringResponse = yield call(request, URL, {
      method: 'POST',
      headers: ajaxRequestHeaders,
      body: BODY,
    });
    // console.log('offeringResponse after update: ', offeringResponse);
    if(action.data.logo && offeringResponse.ext_id) {
      let data = new FormData();
      data.append('logo', action.data.logo)
      var url=`${BASE_URL}/offerings/${offeringResponse.ext_id}/logo`;
      var config = {
        headers: {
          'accept': 'application/json',
          'accept-language': 'en_US',
          'Authorization':TOKEN,
          'content-type': `multipart/form-data; boundary=${data._boundary} `
        }
      };
      const logoRes = yield call(uploadLogo, url, data, config)

      if(logoRes) {
        yield put(actions.updateOfferingSuccess(logoRes))
        yield put(actions.updateOfferingRes(logoRes))
        yield put(actions.updateOfferingRes(null))
        // yield put(actions.createOfferingSuccess(offeringResponse));
        // yield put(actions.fetchCurrentOfferings());
        // yield put(actions.fetchEffectiveOfferings());
        }
      }
      else{
        //should call actions.updateOfferingSuccess
        //pass the updated object -- offeringResponse is the object that we pass
        //then update it in the reducer file

         yield put(actions.updateOfferingSuccess(offeringResponse))
         yield put(actions.updateOfferingRes(offeringResponse))
         yield put(actions.updateOfferingRes(null))
        // yield put(push('/admin_offerings'));
        // yield put(actions.createOfferingSuccess(offeringResponse));
        // yield put(actions.fetchCurrentOfferings());
      }

      // yield put(push('/admin_offerings'));
    // window.location=window.location.origin+'/admin_offerings';
  } catch (err) {
    if(err.response.status==401){
        localStorage.clear();
        window.location.href = '/login';
    }
    ToastStore.error('Enter Valid Data');
    yield put(actions.createOfferingFailed());
  } finally {

    yield put(closeGlobalLoader());
  }
}
function* watcherupdateOfferingForm() {
  yield takeLatest(actionTypes.SUBMIT_UPDATE_OFFERING_FORM, handleUpdateOffering);
}




function* handleArchiveOffering(action) {
  LogStaging(40)
  LogStaging(action)
  yield put(showGlobalLoader());

  const TOKEN = yield select(getToken);

  const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });
   const BODY = JSON.stringify({
        ext_id: action.id
    });
  let URL;
     URL = `${BASE_URL}/web_admin/archive_offering`;
    let offerings;
    try {
      offerings = yield call(request, URL, {
        method: 'PUT',
        headers: ajaxRequestHeaders,
        body:BODY
      });
      //passing the ext id so we can update the offering status in the reducers without making a call to the backend
       yield put(actions.archiveOfferingSuccess(offerings, action.id));
      //  window.location=window.location.origin+'/admin_offerings';
    } catch (err) {
      if(err.response.status==401){
        localStorage.clear();
        window.location.href = '/login';
      }
      console.log(err);
    } finally {
       yield put(closeGlobalLoader());
    }
}

function* watcherArchiveOffering() {
  yield takeLatest(actionTypes.ARCHIVE_OFFERING, handleArchiveOffering);
}


function* handleUnArchiveOffering(action) {
  LogStaging(48)
  LogStaging(action)
  console.log(action);
  yield put(showGlobalLoader());

  const TOKEN = yield select(getToken);

  const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });
   const BODY = JSON.stringify({
        ext_id: action.id
    });
  let URL;
     URL = `${BASE_URL}/web_admin/unarchive_offering`;
    let offerings;
    try {
      offerings = yield call(request, URL, {
        method: 'PUT',
        headers: ajaxRequestHeaders,
        body:BODY
      });
      //passing the ext id so we can update the offering status in the reducers without making a call to the backend
       yield put(actions.unarchiveOfferingSuccess(offerings, action.id));
      //  window.location=window.location.origin+'/admin_offerings';
    } catch (err) {
      if(err.response.status==401){
        localStorage.clear();
        window.location.href = '/login';
      }
      console.log(err);
    } finally {
       yield put(closeGlobalLoader());
    }
}

function* watcherUnArchiveOffering() {
  yield takeLatest(actionTypes.UNARCHIVE_OFFERING, handleUnArchiveOffering);
}

function* handleFetchUserMpId(action) {
  LogStaging(41)
  yield put(showGlobalLoader());
  const TOKEN = yield select(getToken);
   const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });


  const URL = `${BASE_URL}/offerings/bd_underwriter_mpids`;
  let response;
  try {
    response = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    if (response) {
      yield put(actions.fetchUserMpidSuccess(response));
    }
  } catch (err) {
      ToastStore.error('Unable to Fetch UserMPID');
      if(err.response.status==401){
          localStorage.clear();
          window.location.href = '/login';
      }
      console.log(err);
  } finally {
      yield put(closeGlobalLoader());
  }
}
function* watcherHandleFetchUserMpid() {
  yield takeLatest(actionTypes.FETCH_USER_MPID, handleFetchUserMpId);
}

function* handleFetchMPIDComission(action){
  LogStaging(42)
      let ajaxRequestHeaders = new Headers({
        'Content-Type': 'application/json',
        Accept: 'application/json',
      });
      const TOKEN = yield select(getToken);
      yield put(showGlobalLoader());
      ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
      const URL = `${BASE_URL}/underwriters/underwriter_commission_percentage`;
      const BODY = JSON.stringify({
        executing_broker_mpid:action.name
      });
      let response;
      try {
        response = yield call(request, URL, {
          method: 'POST',
          headers: ajaxRequestHeaders,
          body:BODY
        });
        if (response) {
          yield put(actions.fetchUserMpidCommisionSuccess(response));
        }
      } catch (err) {
        if(err.response.status==401){
            localStorage.clear();
            window.location.href = '/login';
        }
          yield put(actions.fetchUserMpidCommisionSuccess({commission_percentage:''}));
      } finally {
          yield put(closeGlobalLoader());
      }
}


function* watcherhandleFetchMPIDComission() {
  yield takeLatest(actionTypes.FETCH_USER_MPID_COMMISION, handleFetchMPIDComission);
}




function* handleFinalProspectus(action) {
  LogStaging(43)
  yield put(showGlobalLoader());

  const TOKEN = yield select(getToken);

  const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });

  //the final prospectus is hard coded we have to change it once we fix the issue with the front end
  const BODY = JSON.stringify({
      ext_id: action.id,
      final_prospectus: action.prospectus_url
    });

    let URL;
      URL = `${BASE_URL}/web_admin/final_prospectus`;
      let offerings;
      try {
        offerings = yield call(request, URL, {
          method: 'PUT',
          headers: ajaxRequestHeaders,
          body:BODY
        });
        // yield put(actions.archiveOfferingSuccess(offerings));
        // window.location=window.location.origin+'/admin_offerings';
      } catch (err) {
        if(err.response.status==401){
          localStorage.clear();
          window.location.href = '/login';
        }
        console.log(err);
      } finally {
        yield put(closeGlobalLoader());
      }
}

function* watcherhandleFinalProspectusg() {
  yield takeLatest(actionTypes.FINAL_PROSPECTUS_OFFERING, handleFinalProspectus);
}



function* handleSendProspectus(action) {
  LogStaging(44)
  yield put(showGlobalLoader());
  const TOKEN = yield select(getToken);

  const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });

  //the final prospectus is hard coded we have to change it once we fix the issue with the front end
  const BODY = JSON.stringify({
      ext_id: action.ext_id,
      final_prospectus: action.prospectus_url
    });

    let URL;
      URL = `${BASE_URL}/web_admin/send_prospectus`;
      let offerings;
      try {
        offerings = yield call(request, URL, {
          method: 'PUT',
          headers: ajaxRequestHeaders,
          body:BODY
        });
        // yield put(actions.archiveOfferingSuccess(offerings));
        // window.location=window.location.origin+'/admin_offerings';
      } catch (err) {
        if(err.response.status==401){
          localStorage.clear();
          window.location.href = '/login';
        }
        console.log(err);
      } finally {
        yield put(closeGlobalLoader());
      }
}

function* watcherhandleSendProspectus() {
  yield takeLatest(actionTypes.SEND_PROSPECTUS_OFFERING, handleSendProspectus);
}


function* handleUpdateProspectus(action) {
  LogStaging(45)
  yield put(showGlobalLoader());
  const TOKEN = yield select(getToken);

  const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });

  //the final prospectus is hard coded we have to change it once we fix the issue with the front end
  const BODY = JSON.stringify({
      ext_id: action.ext_id,
      i_or_f :'i',
      prospectus: action.prospectus_url
    });

    let URL;
      URL = `${BASE_URL}/web_admin/update_prospectus`;
      let offerings;
      try {
        offerings = yield call(request, URL, {
          method: 'PUT',
          headers: ajaxRequestHeaders,
          body:BODY
        });
        // yield put(actions.archiveOfferingSuccess(offerings));
        // window.location=window.location.origin+'/admin_offerings';
      } catch (err) {
        if(err.response.status==401){
          localStorage.clear();
          window.location.href = '/login';
        }
        console.log(err);
      } finally {
        yield put(closeGlobalLoader());
      }
}

function* watcherhandleUpdateProspectus() {
  yield takeLatest(actionTypes.UPDATE_PROSPECTUS_OFFERING, handleUpdateProspectus);
}



function* handleFetchOfferingsOrder(action) {
  LogStaging(39)
  yield put(showGlobalLoader());

  const TOKEN = yield select(getToken);

  const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });

  let URL;
  let offering;

  URL = `${BASE_URL}/offerings/${action.id}/users?email`;


  try {
    offering = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });

    yield put(actions.offeringOrderSuccess(offering));
  } catch (err) {

    if(err.response.status==401){
        localStorage.clear();
        window.location.href = '/login';
    }
    // yield put(actions.updateOfferingFailed(err.error));
    // console.log(err);
  } finally {
    yield put(closeGlobalLoader());
  }
}

function* WatcherFetchOfferingsOrder() {
  yield takeLatest(actionTypes.FETCH_OFFERING_ORDERS, handleFetchOfferingsOrder);
}




function* handlesixtyMinuteEmailAllocation(action) {
  LogStaging(24)
  yield put(showGlobalLoader());
  const TOKEN = yield select(getToken);

  const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });
    const BODY = JSON.stringify({
      ext_id: action.ext_id
    });
    let URL;
    URL = `${BASE_URL}/web_admin/sixty_min_email`;
    let allocation;
    try {
      allocation = yield call(request, URL, {
        method: 'PUT',
        headers: ajaxRequestHeaders,
        body:BODY
      });
       ToastStore.success('60 Min Email Send Successfully');
       yield put(actions.makeSixtyMinuteSuccess(allocation));
       yield put(actions.makeSixtyMinuteSuccess(null));
    } catch (err) {
      if(err.response.status==401){
        localStorage.clear();
        window.location.href = '/login';
      }
      console.log(err);
    } finally {
       yield put(closeGlobalLoader());
    }
}

function* watchersixtyMinuteEmailAllocation() {
  yield takeLatest(actionTypes.SIXTYMINUTEEMAIL, handlesixtyMinuteEmailAllocation);
}




// Bootstrap sagas
export default function* offeringSagas() {
  yield all([
    WatcherFetchCurrentOfferings(),
    WatcherFetchOfferingTypes(),
    WatcherFetchIndustries(),
    watcherCreateOfferingForm(),
    WatcherFetchUpdateOffering(),
    watcherupdateOfferingForm(),
    watcherArchiveOffering(),
    watcherHandleFetchUserMpid(),
    watcherhandleFetchMPIDComission(),
    watcherhandleFinalProspectusg(),
    watcherhandleSendProspectus(),
    watcherhandleUpdateProspectus(),
    WatcherFetchOfferingsOrder(),
    watcherUnArchiveOffering(),
    watchersixtyMinuteEmailAllocation(),
  ]);
}
