
import $ from 'jquery';

function viewport() {
  let e = window;
  let a = 'inner';
  if (!('innerWidth' in window)) {
    a = 'client';
    e = document.documentElement || document.body;
  }
  return { width: e[`${a}Width`], height: e[`${a}Height`] };
}

function hideSearch() {
  const view = viewport();
  const windowWidth = view.width;

  $('#searchInput').val('');
  $('.top_bar').removeClass('searching');
  if (windowWidth < 821) {
    $('.top_bar').removeClass('open');
  }
}


export function initialScript() {
  const view = viewport();
  const windowWidth = view.width;

  $('.check').on('change', function () {
    if ($(this).is(':checked')) {
      $(this).closest('.filter').addClass('active');
    } else {
      $(this).closest('.filter').removeClass('active');
    }
  });

  $('.menu_icon').on('click', () => {
    $('.side_bar').addClass('open');
    $('.overlay').css({ display: 'block' });
  });

  $('.overlay').on('click', () => {
    $('.side_bar').removeClass('open');
    $('.overlay').css({ display: 'none' });
  });

  $('#searchInput').on('click', () => {
    $('.top_bar').addClass('searching');
  });

  if (windowWidth < 769) {
    $('.search_icon').on('click', () => {
      $('.top_bar').addClass('open');
    });

    $('.open_account').on('click', () => {
      $('.filter_section').addClass('open');
      $('.overlay').css({ display: 'block' });
      $('.overlay').on('click', () => {
        $('.filter_section').removeClass('open');
        $('.overlay').css({ display: 'none' });
      });
    });
  }

  $('.search_close').on('click', hideSearch);
  $('.back').on('click', hideSearch);
}
