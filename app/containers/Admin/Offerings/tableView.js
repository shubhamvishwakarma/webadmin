import React from 'react';
import PropTypes from 'prop-types';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { Link } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';


import { getOfferingPriceRange, getPriceText, getDate, nFormatter } from 'containers/App/Helpers';
import {archiveOffering,unarchiveOffering,sendProspectusOffering,updateProspectusOffering,sixtyMinuteEmailAllocation} from './actions';
import { makeSixtyMinuteSuccess} from './selectors';
import  ArchiveModal from './list/archiveConfirm';
import ProspectusConfirmModal from './list/prospectusConfirm';
import Modal from 'react-responsive-modal';
import {ToastContainer, ToastStore} from 'react-toasts';
  class TableView extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state={
      archiveConfirmshow:false,
      emailConfirmshow:false,
      prospectConfirmshow:false,
      offeringInfo:{},
      isLoad: false,
      isArchive: '',
      sendSixtyMinuteConfirm:false,
      sixtyMinuteofferingInfo:''
    }
    this.collapseOffering=this.collapseOffering.bind(this);
    this.toggleModal=this.toggleModal.bind(this);
    this.handleProspectusChange=this.handleProspectusChange.bind(this);
    this.openSixtyMinuteModal=this.openSixtyMinuteModal.bind(this);
    this.closeSixtyMinuteModal=this.closeSixtyMinuteModal.bind(this);
  }



  toggleModal = () => {
      this.setState({
        archiveConfirmshow: false,
        emailConfirmshow: false,
        prospectConfirmshow: false
      });
  }


  openSixtyMinuteModal(offeringInfo){
      this.setState({
        sixtyMinuteofferingInfo: offeringInfo,
        sendSixtyMinuteConfirm:true
      })
  }
  closeSixtyMinuteModal(){
      this.setState({
        sendSixtyMinuteConfirm:false
      })
  }

  openArchiveModal(offering,archiveStatus){
      this.setState({
        archiveConfirmshow:true,
        offeringInfo:offering,
        isArchive: archiveStatus
      });
  }
  openSendEmailModal(offering){
      this.setState({
        emailConfirmshow:true,
        offeringInfo:offering
      });
  }
  openProspectusModal(offering){
      this.setState({
        prospectConfirmshow:true,
        offeringInfo:offering
      });
  }


finalProspectusOffering(id, prospectus_url){
    this.props.finalProspectusOffering(id, prospectus_url)
}


allocationEmailOffering(id){
    this.props.allocationEmailOfferings(id);
}
collapseOffering(item,types){
    this.setState({isLoad: true})
    if(item.isOpen==true){
      item.isOpen=false;
    }else{
      item.isOpen=true;
    }
    setTimeout(()=>{
        this.setState({isLoad: false})
    },500)
}

updateProspectus(offeringInfo){
    offeringInfo.isOpen=false;
    this.props.updateProspectusOffering(offeringInfo.ext_id,offeringInfo.prospectus_url);
}

handleProspectusChange(e,item){
    this.setState({isLoad: true})
    item.prospectus_url=e.target.value;
    setTimeout(()=>{
        this.setState({isLoad: false})
    },100)
}

  doSendSixtyMinuteEmail(offeringInfo){
      this.props.sendSixtyMinuteEmail(offeringInfo.ext_id);
  }

  render(){
    const { offeringTableView, getNewPage ,offeringEmailSuccess} = this.props;

    const {isOpen}=this.state;
    if(isOpen){
      this.state.isOpen=false;
    }

    if(offeringEmailSuccess != undefined){
      this.state.sendSixtyMinuteConfirm=false;
    }

    if (offeringTableView && offeringTableView.length > 0) {
      return (


        <div className="table">
          <ul id="listContent">
          {/* add css transition here */}
          {/* add css animation for archiving offering */}
          <ReactCSSTransitionGroup
            transitionName="fade"
            transitionEnterTimeout={300}
            transitionLeaveTimeout={300}>
            {offeringTableView.map((item,i) => (
                <li className="offeringList_li" key={i} >
                  <div className="row">
                    <div className="col-sm-1"></div>
                    <div className={"col-sm-10 offeringlistrow "+ (item.offering_type_name === 'IPO' ? 'green_border' : 'blue_border')}>
                      <div className="row ">
                        <div className="col-sm-5 offeringlistLeft">
                          <div className="row">
                            <div className="col-sm-2">
                                {item.logo_large !==null && <img style={{paddingTop:12}} src={item.logo_large}/>}
                                {item.logo_large ===null && <div className="icon_column">
                                  <div className={item.offering_type_name === 'IPO' ? 'icon' : 'icon_2'}>{item.name.charAt(0)}</div>
                                </div>}
                            </div>
                            <div className="col-sm-10 offeringcontent">
                                  <div className={item.is_viewed ? 'name_read_ offeringName' : 'offeringName'}>{item.name}
                                    <span style={{color:(item.offering_type_name === 'IPO') ? "#8DC73F":"rgba(8,57,82,1)" }}>  ({(item.offering_type_name === 'IPO') ? 'IPO':'Secondary'})</span>
                                  </div>
                                  {((item.status === 'active')||(item.status === 'upcoming')) &&<p className="no_margin titles">Anticipated Date : <span className="sub_titles">{getDate(item)}</span></p>}
                                  {((item.status === 'closed')||(item.status === 'cancelled')) &&<p className="no_margin titles">Effective Date : <span className="sub_titles">{getDate(item)}</span></p>}

                                  {(item.status==='active' || item.status==='upcoming' || item.status==='cancelled') &&<p  className="no_margin titles">
                                    {(item.offering_type_name === 'IPO')?'Price range':'Last Closing Price'} <span className="sub_titles">{getOfferingPriceRange(item)} </span></p>}

                                  {item.status==='closed' &&<p  className="no_margin titles">Final Price :    <span className="sub_titles">$ {item.final_price}</span></p>}
                                  <p className="no_margin titles">Followers: <span className="sub_titles">{ item.followers ? item.followers : '0' } </span></p>
                                  <p  className="no_margin titles">Orders : <span className="sub_titles">{item.total_orders_count ? item.total_orders_count : 0} </span></p>
                                  <p  className="no_margin titles">Amount Ordered : <span className="sub_titles">$ { item.totalAmountOrdered ? item.totalAmountOrdered : 0 }</span></p>

                            </div>
                          </div>
                        </div>
                        <div className="col-sm-3 offeringlistbtns ">

                          <div className="gridButtons text_center">

                              {item.status === 'cancelled' &&<div>
                                 <button className="btn btn-primary" onClick={()=>{this.openArchiveModal(item,'unarchive')}} >Unarchive </button>
                              </div>}
                              {((item.status === 'active')||(item.status === 'upcoming')) &&<div>
                                 {  item.total_orders_count >0 && <Link to={`/admin_offerings/orders/${item.ext_id}`}><button className="btn btn-primary" > Orders</button></Link>}
                                 {  item.total_orders_count === 0 && <button className="btn btn-primary" disabled={true}> Orders</button>}
                              </div>}

                              {(item.status === 'closed') &&<div>
                                 <Link to={`/admin_offerings/orders/${item.ext_id}`}><button className="btn btn-primary" > Orders</button></Link>
                              </div>}



                               {((item.status === 'active')||(item.status === 'upcoming')) && <div>
                               {/*
                               <button className="btn btn-primary" onClick={()=>{this.openProspectusModal(item)}} >Send Prospectus</button>*/}

                               <button className="btn btn-primary"  onClick={()=>{this.collapseOffering(item)}}  >Initial Prospectus</button>

                               </div> }
                                {((item.status === 'active')||(item.status === 'upcoming') || (item.status === 'closed')) &&<div>
                                  <button className="btn btn-primary" disabled={item.total_orders_count === 0} onClick={()=>this.openSixtyMinuteModal(item)}>60 min email</button>
                               </div>}
                          </div>

                      </div>
                       <div className="col-sm-3 offeringlistbtns text_center">
                           {((item.status === 'active')||(item.status === 'upcoming')) &&  <div>
                              <Link to={`/admin_offerings/edit/${item.ext_id}`}><button className="btn btn-primary" >Edit Offering</button></Link>
                                <button className="btn btn-archive" onClick={()=>{this.openArchiveModal(item,'archive')}}  >Archive</button>
                           </div>}
                       </div>
                      </div>
                      {item.isOpen && <div className="row offeringcollapse" >
                          <p className="header">Edit Prospectus</p>
                          <input type="text" value={item.prospectus_url}  onChange={(event)=>{this.handleProspectusChange(event,item)}} />
                          <button className="btn btn-submitprospectus" onClick={()=>{this.updateProspectus(item)}} >Update</button>
                      </div>}
                    </div>
                    <div className="col-sm-1"></div>
                  </div>
                </li>

              ))
              }

            </ReactCSSTransitionGroup>
            {/* end css transition here */}
          </ul>
           <ArchiveModal unarchiveOffering={this.props.unarchiveOffering} archiveOffering={this.props.archiveOffering} offeringInfo={this.state.offeringInfo} show={this.state.archiveConfirmshow} archiveStatus={this.state.isArchive}  onClose={this.toggleModal} ></ArchiveModal>

           <ProspectusConfirmModal  sendProspectusOffering={this.props.sendProspectusOffering} offeringInfo={this.state.offeringInfo} show={this.state.prospectConfirmshow}  onClose={this.toggleModal} ></ProspectusConfirmModal>
            {this.state.sendSixtyMinuteConfirm && <Modal open={this.state.sendSixtyMinuteConfirm} onClose={this.closeSixtyMinuteModal}>
                  <div>
                    <div className="ModalHeader">
                        <button className="closeBtn float_right" onClick={this.closeSixtyMinuteModal}>
                            <span aria-hidden="true" >&times;</span>
                        </button>
                    </div>
                    <div>
                    <div className="row">
                        <div className="confirmAllocationPopup">
                          <p>Do you want to send 60 minute email ?  </p>
                          <div className="row">
                            <button style={{margin:10}} className="buttons btn-success tableButtons"  onClick={()=>this.doSendSixtyMinuteEmail(this.state.sixtyMinuteofferingInfo)}>Send </button>
                            <button style={{margin:10}} className="buttons btn-warning tableButtons " onClick={this.closeSixtyMinuteModal}> Cancel </button>
                          </div>
                        </div>
                      </div>
                    </div>
                    </div>
                </Modal>}
        </div>
      );
    } else if (offeringTableView && offeringTableView.length === 0) {
      return (
        <div className="name" style={{ paddingTop: '30px', paddingLeft: '20px' }}>
            No result found
          </div>
      );
    }
    return (null);
  }
}

TableView.propTypes = {
  offeringTableView: PropTypes.any,
  getNewPage: PropTypes.func,
  offeringEmailSuccess: PropTypes.any
};

function mapDispatchToProps(dispatch) {
  return {
    unarchiveOffering:(id)=>{dispatch(unarchiveOffering(id))},
    archiveOffering:(id)=>{dispatch(archiveOffering(id))},
    sendProspectusOffering:(ext_id, prospectus_url)=>{dispatch(sendProspectusOffering(ext_id, prospectus_url))},
    updateProspectusOffering:(ext_id, prospectus_url)=>{dispatch(updateProspectusOffering(ext_id, prospectus_url))},
    sendSixtyMinuteEmail:(ext_id)=>{dispatch(sixtyMinuteEmailAllocation(ext_id))}
  };
}

const mapStateToProps = createStructuredSelector({
  offeringEmailSuccess: makeSixtyMinuteSuccess(),
});

export default connect(mapStateToProps, mapDispatchToProps)(TableView);
