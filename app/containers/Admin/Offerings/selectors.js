import { createSelector } from 'reselect';

const selectOffering = (state) => state.get('adminOfferings');


const makeSelectCurrentOfferingTableView = () => createSelector(
  selectOffering,
  (adminState) => adminState.get('currentOfferingTableView')
);

const makeSelectOfferingTypes = () => createSelector(
  selectOffering,
  (adminState) => adminState.get('offeringTypes')
);

const makeSelectIndustries = () => createSelector(
  selectOffering,
  (adminState) => adminState.get('industries')
);
const makeSelectupdateOffering = () => createSelector(
  selectOffering,
  (adminState) => adminState.get('UpdateOffering')
)
const makeSelectUserMpid = () => createSelector(
  selectOffering,
  (adminState) => adminState.get('UserMpid')
);

const makeSelectUserMpidCommision = () => createSelector(
  selectOffering,
  (adminState) => adminState.get('underWriterCommission')
)
const makeCreateOfferingSuccess = () => createSelector(
  selectOffering,
  (adminState) => adminState.get('offeringSuccsess')
)
const makeOfferingOrderSuccess = () => createSelector(
  selectOffering,
  (adminState) => adminState.get('offeringOrdersSuccsess')
)

const makeSixtyMinuteSuccess= () => createSelector(
  selectOffering,
  (adminState) => adminState.get('sixtyMinuteSuccess')
)

const makeupdatedOfferingRes=() => createSelector(
  selectOffering,
  (adminState) => adminState.get('updateSpecificoffering')
)


export {
  selectOffering,
  makeSelectCurrentOfferingTableView,
  makeSelectOfferingTypes,
  makeSelectIndustries,
  makeSelectupdateOffering,
  makeSelectUserMpid,
  makeSelectUserMpidCommision,
  makeCreateOfferingSuccess,
  makeOfferingOrderSuccess,
  makeSixtyMinuteSuccess,
  makeupdatedOfferingRes,

};
