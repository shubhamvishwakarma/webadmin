/*
 * Investor Reducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import {
  FETCH_CURRENT_OFFERINGS_DATA,
  CURRENT_OFFERINGS_PAGINATION,
  FETCH_OFFERING_TYPES_DATA,
  FETCH_INDUSTRIES_DATA,
  UPDATE_OFFERING_SUCCESS,
  ARCHIVE_OFFERING,
  ARCHIVE_OFFERING_SUCCESS,
  FETCH_USER_MPID_SUCCESS,
  FETCH_USER_MPID,
  FETCH_USER_MPID_COMMISION,
  FETCH_USER_MPID_COMMISION_SUCCESS,
  CREATE_OFFERING_SUCCESS,
  FETCH_OFFERING_ORDERS_SUCCESS,
  UNARCHIVE_OFFERING_SUCCESS,
  SIXTYMINUTEEMAIL_SUCCESS,
  UPDATE_OFFERINGS_SUCCESS,
  UPDATE_OFFERINGS_RESPONSE
} from './constants';

const initialState = fromJS({
  offerings: null,
});

//create a reducer that changes the status of the offering to cancelled when the archive offering is called



function adminOfferingReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_CURRENT_OFFERINGS_DATA:
    // console.log(1)
      return state.set('offerings', action.data).set('currentOfferingTableView', action.data);
    case CURRENT_OFFERINGS_PAGINATION:
      // console.log(2)
      return state.set('offerings', action.data).set('currentOfferingTableView', state.toJS().currentOfferingTableView.concat(action.data));
       case FETCH_OFFERING_TYPES_DATA:
      // console.log(5)
      return state.set('offerings', action.data).set('offeringTypes', action.data);
    case FETCH_INDUSTRIES_DATA:
      // console.log(6)
      return state.set('offerings', action.data).set('industries', action.data);
    case UPDATE_OFFERINGS_SUCCESS:
        let offeringArrayList1 = state.get('currentOfferingTableView')
        if(offeringArrayList1){
          const index = offeringArrayList1.findIndex(i => i.ext_id === action.data.ext_id)
           offeringArrayList1.splice(index, 1,action.data)
          // return state.set('currentOfferingTableView', offeringArrayList1)
        }
        // return state.set('currentOfferingTableView', offeringArrayList1)
        // console.log(offeringArrayList1);
        // const index = offeringArrayList1.findIndex(i => i.ext_id === action.data.ext_id)
        //  offeringArrayList1.splice(index, 1,action.data)
        //  console.log(index);
        //  console.log(offeringArrayList1);

        // if(index!=-1){
        //   console.log("Update")
        //   console.log(offeringArrayList1);
        //     return state.set('offerings', offeringArrayList1)
        // }else{
        //   console.log("Existing")
        //   return state.set('offerings', action.data).set('UpdateOffering', action.data);
        // }


        //  console.log(offeringArrayList1);
        // //  return state.set('offerings', offeringArrayList1)
        //   const testState1 = state.set('offerings', offeringArrayList1).set('offerings', offeringArrayList1);
        //   return testState1
      // console.log('state after: ', state.get('offerings'))


      // return testState

      //  return state.set('offerings', action.data).set('UpdateOffering', action.data);


    case UPDATE_OFFERING_SUCCESS:
      //have to take action.data and update the offeirngs table
      // console.log(7)
      return state.set('offerings', action.data).set('UpdateOffering', action.data);
    case ARCHIVE_OFFERING_SUCCESS:
      // console.log(8)
    // console.log('inside archive_offering success: ', action.data)
    // console.log('inside of archi_offering succes: ', action)
      //this is where I have to update the offering status from active to cancelled
      // console.log('state Map object: ', state)
      let offeringArray = state.get('offerings')
      // console.log('offeringArray', offeringArray)

      let updatedOffering = offeringArray.find(offering => offering.ext_id === action.ext_id)
      // console.log('updated offering: ', updatedOffering)
      // console.log('status of updatedOffering before: ', updatedOffering.status)
      updatedOffering.status = 'cancelled'
      // console.log('status of updatedOffering after: ', updatedOffering.status)
      // console.log('offeringArray: ', state.get('offerings'))
      const testState = state.set('currentOfferingTableView', offeringArray).set('ArchiveOffering', updatedOffering);
      // console.log('state after: ', state.get('offerings'))


      return testState
    case UNARCHIVE_OFFERING_SUCCESS:
      let offeringArrayList = state.get('offerings')

      let updatedOfferingList = offeringArrayList.find(offering => offering.ext_id === action.ext_id)
      updatedOfferingList.status = 'active'
      const UpdateState = state.set('currentOfferingTableView', offeringArrayList).set('ArchiveOffering', updatedOfferingList);
      return UpdateState

    case  FETCH_USER_MPID_SUCCESS:
      // console.log(9)
      return state.set('offerings',action.data).set('UserMpid', action.data);
    case  FETCH_USER_MPID_COMMISION_SUCCESS:
      // console.log(10)
      return state.set('offerings',action.data).set('underWriterCommission', action.data);
    // case ACTIVE_OFFFERING
    case CREATE_OFFERING_SUCCESS :
       return state.set('offeringSuccsess',action.data);
    case FETCH_OFFERING_ORDERS_SUCCESS:
     return state.set('offeringOrdersSuccsess',action.data);
    case SIXTYMINUTEEMAIL_SUCCESS:
        return state.set('sixtyMinuteSuccess',action.data);
    case UPDATE_OFFERINGS_RESPONSE:
      return state.set('updateSpecificoffering', action.data);
    default:
      return state;
  }
}


export default adminOfferingReducer;
