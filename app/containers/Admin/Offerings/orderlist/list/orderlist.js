import React from 'react/lib/React';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { reduxForm, Field, change, reset } from 'redux-form/immutable';
import RaisedButton from 'material-ui/RaisedButton';
import {ToastContainer, ToastStore} from 'react-toasts';
import { mmddyy, parseTime ,currency} from 'containers/App/Helpers';
import Select from 'react-select';
import { makeOfferingOrderSuccess} from '../../selectors';
import { fetchOfferingOrder} from '../../actions';

import {DataTable} from 'primereact/components/datatable/DataTable';
import {Column} from 'primereact/components/column/Column';
import {Button} from 'primereact/components/button/Button';
import {InputText} from 'primereact/components/inputtext/InputText';
import {ColumnGroup} from 'primereact/components/columngroup/ColumnGroup';
import {Row} from 'primereact/components/row/Row';




class OrderListingComponent extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      searchbyCat:null,
      searching: ''
    };

    this.allocatesAmountTemplate=this.allocatesAmountTemplate.bind(this);
    this.requestedAmountTemplate=this.requestedAmountTemplate.bind(this);
    this.buyingpowerTemplate=this.buyingpowerTemplate.bind(this);
    this.statusTemplate=this.statusTemplate.bind(this);
    this.dropDownChange=this.dropDownChange.bind(this);
    this.searchOffering=this.searchOffering.bind(this);
  }

  componentDidMount() {
     let id=this.props.match.path.split("orders/")[1];
     this.props.loadofferingOrders(id);
  }

  allocatesAmountTemplate(rowData){
      return <div className="ui-cell-data "> $ {currency(rowData.order_info.amount_allocated)}</div>
  }
  requestedAmountTemplate(rowData){
      return <div className="ui-cell-data "> $ {currency(rowData.order_info.amount_requested)}</div>
  }
  buyingpowerTemplate(rowData){
      return <div className="ui-cell-data " > $ {currency(rowData.order_info.available_buying_power)}</div>
  }

  statusTemplate(rowData){
      if(rowData.order_info.status=="active"){
          return <div className="ui-cell-data" >  {rowData.order_info.status}</div>
      }else{
        return <div className="ui-cell-data update-error" >  {rowData.order_info.status}</div>
      }
  }


  dropDownChange(event){
      this.setState({
        searchbyCat:event.target.value,
        searching:'',
      })
  }
  searchOffering(event){
      this.setState({
        searching:event.target.value
      })
  }

  render() {
      const {offeringOrders ,history} =this.props;


      let  filterData=[];
      if(this.state.searchbyCat=="user_name" ){
        filterData = this.state.searching? offeringOrders.filter(row => row.user_info.name.toLowerCase().indexOf(this.state.searching.toLowerCase()) > -1) : offeringOrders;
      }else if (this.state.searchbyCat=="account_name"){
        filterData = this.state.searching? offeringOrders.filter(row => row.user_info.account_name.toLowerCase().indexOf(this.state.searching.toLowerCase()) > -1) : offeringOrders;
      }else if (this.state.searchbyCat=="email"){
        filterData = this.state.searching? offeringOrders.filter(row => row.user_info.email.toLowerCase().indexOf(this.state.searching.toLowerCase()) > -1) : offeringOrders;
      }else{
        filterData = this.state.searching? offeringOrders.filter(row => row.user_info.name.toLowerCase().indexOf(this.state.searching.toLowerCase()) > -1) : offeringOrders;
      }
            return(
                <div>
                  <div className="row offeringSubheader">
                      <div className="col-sm-6">

                      <div className="offering_Detail_header">
                          <div onTouchTap={() => history.goBack()} >
                            <div className="back_icon"></div>
                          </div>
                          <div className="Headingname">
                            Offering Orders
                          </div>
                      </div>
                      </div>
                      <div className="col-sm-6">
                          <div className=" offeringGlobalSearch">
                           <div className="filterbyContent">Search By</div>
                              <select className="selectField"  value={this.state.filterBy} onChange={this.dropDownChange} >
                                  <option value="user_name">User Name</option>
                                  <option value="account_name">Account Name</option>
                                  <option value="email">User Email</option>
                              </select>
                            <div className="search_icon"></div>
                            <input id="searchInput" type="text" placeholder="Search" onChange={this.searchOffering} />
                            <div className="search_close" onTouchTap={this.getAllOfferings}></div>


                        </div>
                      </div>

                  </div>
                  <div className="row">
                    <div className="col-12"  style={{paddingLeft:30,paddingRight:20,paddingTop:15}}>
                        <DataTable value={filterData} editable={false}  responsive={true}   scrollable={true} scrollHeight="90rem" resizableColumns={true}  columnResizeMode="expand">
                              <Column className="dataTableNameColumn" style={{width:150}} field="user_info.name" header="User Name"  sortable={true} />
                              <Column className="tableCols" style={{width:150}}  field="user_info.account_name" header="Account Name"  sortable={true} />
                              <Column className="tableCols" style={{width:250}}  field="user_info.email" header="User Email"  sortable={true} />

                              <Column className="tableCols"  style={{width:150}}  field="offering_info.i_prospectus_datatime" header="Initial  Prospectus"  sortable={true} />

                              <Column className="tableCols" style={{width:150}}  field="offering_info.f_prospectus_datatime" header="Final  Prospectus"  sortable={true} />
                              <Column className="tableCols" style={{width:150}}  field="order_info.amount_allocated"  body={this.allocatesAmountTemplate}  header="Allocated  Amount"  sortable={true} />

                              <Column className="tableCols" style={{width:150}}  field="order_info.amount_requested"  body={this.requestedAmountTemplate} header="Requested  Amount"  sortable={true} />
                              <Column className="tableCols" style={{width:150}}  field="order_info.shares_allocated" header="Allocated Shares"  sortable={true} />

                              <Column className="tableCols" style={{width:150}}  field="order_info.available_buying_power" body={this.buyingpowerTemplate} header="IPO Buying Power"  sortable={true} />

                              <Column className="tableCols" style={{width:150}} body={this.statusTemplate}   field="order_info.status" header="Status"  sortable={true} />



                        </DataTable>
                    </div>
                  </div>
          </div>

        );
  }
}

OrderListingComponent.propTypes = {
  handleFormSubmit: React.PropTypes.func,
  dispatch: React.PropTypes.func,
  history: React.PropTypes.object,
  offeringOrders:React.PropTypes.any
};

const ClickIPOCreateOrderListingComponent = reduxForm({
  form: 'ClickIPOCreateOrderListingComponent',
  destroyOnUnmount: false,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
  touchOnBlur: false,
})(OrderListingComponent);

const mapStateToProps = createStructuredSelector({
   offeringOrders: makeOfferingOrderSuccess(),
});


function mapDispatchToProps(dispatch, ownProps) {
  const urlId = ownProps.match.params.id;
  return {
     loadofferingOrders:(id)=>dispatch(fetchOfferingOrder(id)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ClickIPOCreateOrderListingComponent);
