

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Switch, Route ,withRouter} from 'react-router-dom';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { makeSelectCurrentUser } from 'containers/App/selectors';
import { fetchAllocationList, fetchOfferingList } from './actions';
import saga from './sagas';
import reducer from './reducer';
import AllocationList from './list/index';
import CreateNewAllocation from './createallocation/index';
import AllocationDetail from './updateallocation/index';

class Allocations extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  componentWillMount() {
    this.props.fetchInitialData();
  }

  render() {
    const { match } = this.props;
    return (
      <Switch>
        <Route exact path={match.url} component={AllocationList} />
        <Route path={`${match.url}/create_allocations`} component={CreateNewAllocation} />
        <Route path={`${match.url}/details/:id`} component={AllocationDetail} />

      </Switch>
    );
  }
}

Allocations.propTypes = {
  currentUser: React.PropTypes.object,
  match: PropTypes.object,
  fetchInitialData: React.PropTypes.func,
};

const mapStateToProps = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    fetchInitialData: () => {
      dispatch(fetchAllocationList());
      dispatch(fetchOfferingList());
    },
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withReducer = injectReducer({ key: 'adminAllocations', reducer });
const withSaga = injectSaga({ key: 'adminAllocations', saga });

export default withRouter(compose(
  withReducer,
  withSaga,
  withConnect,
)(Allocations));
