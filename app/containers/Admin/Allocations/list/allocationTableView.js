import React from 'react';
import PropTypes from 'prop-types';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { Link } from 'react-router-dom';
import { createStructuredSelector } from 'reselect';
import Waypoint from 'react-waypoint';
import { connect } from 'react-redux';
import { Field,Checkbox } from 'redux-form-material-ui';
import { mmddyy, convertToLocalString, currency } from 'containers/App/Helpers';

import { fetchShowAllocation,fetchRunAllocation ,fetchSaveAllocation,fetchCompleteAllocation,sendProspectusAllocation,sixtyMinuteEmailAllocation,AllocationEmail,callIntactUpload,fetchShowAllocationSuccess} from './../actions';
import { makeSelectAllocations,makegetShowAllocation,makegetCompleteAllocation ,makeRunAllocationError,makeFinalProspectSuccess,makeSixtyMinuteSuccess,makeAllocationEmailSuccess} from '../selectors';

import ReactDataGrid from 'react-data-grid';
import {DataTable} from 'primereact/components/datatable/DataTable';
import {Column} from 'primereact/components/column/Column';
import {Button} from 'primereact/components/button/Button';
import {InputText} from 'primereact/components/inputtext/InputText';
import {ColumnGroup} from 'primereact/components/columngroup/ColumnGroup';
import {Row} from 'primereact/components/row/Row';

import Modal from 'react-responsive-modal';
import ShowAllocationModal from './showallocationModal'

  class AllocationTableView extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state={
      offeringInfo:{},
      isLoad: false,
      final_price:'',
      allocationInfo:'',
      open: null,
      isSave:false,
      isEdit:false,
      isOpen: false,
      initialTotalAllocatedShare:0,
      confirmModal:false,
      isCompleteConfirm: false,
      isComplete: false,
      sendProspectConfirm: false,
      sendProspectusAllocationInfo:'',
      sendSixtyMinuteConfirm:false,
      sixtyMinuteAllocationInfo:'',
      allocationEmailConfirm: false,
      allocationEmailAllocationInfo:'',
      isAllocatedChange:true,
      Total_initial_shares:0,
      isUIrender:false,
      allocatedShareDifference:0,
      isGrater: false,
      isReOpen:false,
      Total_allocated_amount:0,
      isUpdateState:null,
      openshowAllocation: null
    }

    this.showCompleteAllocation=this.showCompleteAllocation.bind(this);
    this.onOpenModal=this.onOpenModal.bind(this);
    this.onCloseModal=this.onCloseModal.bind(this);
    this.getValue=this.getValue.bind(this);
    this.openRunErrorModal=this.openRunErrorModal.bind(this);
    this.closeRunErrorModal=this.closeRunErrorModal.bind(this);
    this.handleProspectusChange=this.handleProspectusChange.bind(this);
    this.openFinalProspectusModal=this.openFinalProspectusModal.bind(this);
    this.closeFinalProspectusModal=this.closeFinalProspectusModal.bind(this);

    this.openSixtyMinuteModal=this.openSixtyMinuteModal.bind(this);
    this.closeSixtyMinuteModal=this.closeSixtyMinuteModal.bind(this);
    this.openAllocationEmailModal=this.openAllocationEmailModal.bind(this)
    this.closeAllocationEmailModal=this.closeAllocationEmailModal.bind(this)
    this.handleSubmit=this.handleSubmit.bind(this)
  }


    handleSubmit(e){
        e.preventDefault();
        this.props.callIntactUpload()
    }

    showCompleteAllocation(allocation){
        this.setState({
          final_price:allocation.final_price,
          allocationInfo:allocation
        })
        this.props.showallocation(allocation.ext_id);
        this.onOpenModal();
    }
    openConfirmModal= () =>{
        this.setState({ confirmModal: true });
    }
    closeConfirmModal= () =>{
        this.setState({ confirmModal: false });
    }

    onOpenModal = () => {
        this.setState({ open: true,openshowAllocation:true ,isAllocatedChange:true, initialTotalAllocatedShare:0,allocatedShareDifference:0,isSave:false,isReOpen:true});
    }
    onCloseModal = () => {
        this.props.showAllocationsReset();
        this.setState({ open: false ,isReOpen:false,openshowAllocation:false});
    }


    allocatesSharesTemplate(rowData){
        if(Number(rowData.allocated_amount)>Number(rowData.buying_power)){
          rowData.greater=true;
        }else{
          rowData.greater=false;
        }
      return <div className={"ui-cell-data "+ (rowData.greater==true ? 'update-error' :'')}>{rowData.allocated_shares}</div>
    }


    getValue(allocation){
        this.setState({
          isEdit:true,
          final_price:allocation.final_price,
          allocationInfo:allocation
        })

        this.props.showallocation(allocation.ext_id);
        this.onOpenModal();
    }


    runAllocations(allocation){
      this.setState({
        isErrorModal:true
      })
      this.props.runAllocation(allocation.ext_id)
    }

    collapseOffering(item,types){
      this.setState({isLoad: true})
      if(item.isOpen==true){
        item.isOpen=false;
      }else{
        item.isOpen=true;
      }
      setTimeout(()=>{
          this.setState({isLoad: false})
      },500)
    }
    handleProspectusChange(e,item){
      this.setState({isLoad: true})
      item.prospectus_url=e.target.value;
      setTimeout(()=>{
          this.setState({isLoad: false})
      },100)
    }

    openRunErrorModal(){
        this.setState({
          runErrorModal:true
        })
    }

    closeRunErrorModal(){
        this.setState({
          runErrorModal:false,
          isErrorModal:false
        })
    }
    sendFianlProspectus(allocationInfo){

      this.props.sendProspectusAllocation(allocationInfo.ext_id,allocationInfo.prospectus_url);
    }
    openFinalProspectusModal(AllocationInfo){
        this.setState({
          sendProspectusAllocationInfo: AllocationInfo,
          sendProspectConfirm:true
        })
    }

    closeFinalProspectusModal(){
        this.setState({
          sendProspectConfirm:false
        })
    }


  openSixtyMinuteModal(AllocationInfo){
      this.setState({
        sixtyMinuteAllocationInfo: AllocationInfo,
        sendSixtyMinuteConfirm:true
      })
  }
  closeSixtyMinuteModal(){
      this.setState({
        sendSixtyMinuteConfirm:false
      })
  }

  doSendSixtyMinuteEmail(allocationInfo){
    this.props.sixtyMinuteEmailAllocation(allocationInfo.ext_id);
  }

  openAllocationEmailModal(AllocationInfo){
      this.setState({
        allocationEmailAllocationInfo: AllocationInfo,
        allocationEmailConfirm:true
      })
  }
  closeAllocationEmailModal(){
      this.setState({
        allocationEmailConfirm:false
      })
  }

  doSendAllocationEmail(allocationInfo){
    this.props.AllocationEmail(allocationInfo.ext_id);
  }
  selectedOffering(e,item,index){
    this.setState({isLoad: true})
        if(item.checked==true ){
          item.checked=false
          this.props.AllocationView[index].checked=false
        }else{
          item.checked=true
          this.props.AllocationView[index].checked=true
        }
    setTimeout(()=>{
      this.setState({isLoad: false})
    },500)

  }


  render(){
    const { AllocationView, getNewPage ,showAllocations,RunAllocationError,finalProspectusSuccess,sixtyMinuteSuccess,AllocationEmailSuccess} = this.props;

    const {isOpen,isSave}=this.state;

     if(RunAllocationError!=undefined && this.state.isErrorModal){
      this.openRunErrorModal();
    }
    if(finalProspectusSuccess != undefined){
      this.state.sendProspectConfirm=false;
    }
    if(sixtyMinuteSuccess != undefined){
      this.state.sendSixtyMinuteConfirm=false;
    }
    if(AllocationEmailSuccess != undefined){
      this.state.allocationEmailConfirm=false;
    }

    let Total_allocated_shares=0;
    let Total_BuyingPower=0;
    let Total_allocated_amount=0;

    if(showAllocations){
          for(let items of showAllocations){
              Total_allocated_shares=Number(Total_allocated_shares) + Number(items.allocated_shares);
              Total_BuyingPower=Number(Total_BuyingPower) + Number(items.buying_power);
              Total_allocated_amount=Number(Total_allocated_amount) + Number(items.allocated_amount);
              this.state.Total_allocated_amount=Total_allocated_amount;
          }


            let Total_initial_allocated_shares=0;

            if(this.state.isAllocatedChange==true && Total_initial_allocated_shares==0){
              for(let item of this.props.showAllocations){
                Total_initial_allocated_shares=Number(Total_initial_allocated_shares) + Number(item.allocated_shares);
              }

               this.state.Total_initial_shares=Total_initial_allocated_shares;
            }
            if(this.state.isAllocatedChange==false && this.state.isUIrender==true){
                if(this.state.Total_initial_shares < Total_allocated_shares){
                   this.state.isSave=true;
                   this.state.isUIrender=false;
                   this.state.allocatedShareDifference= Number(Total_allocated_shares) - Number(this.state.Total_initial_shares);
                }else{
                  //  this.state.allocatedShareDifference=0;
                  this.state.allocatedShareDifference= Number(Total_allocated_shares) - Number(this.state.Total_initial_shares);
                   console.log(this.state.allocatedShareDifference);
                   this.state.isSave=true;

                }
                if(this.state.allocatedShareDifference==0){
                  this.state.isSave=false;
                }

            }


    }

        let footerGroupShowAllocation = <ColumnGroup>
        <Row>
            <Column className="dataTableTotal" footer="Total" colSpan={1} />
            <Column footer="" />
            <Column footer="" />
            {this.state.Total_allocated_amount &&<Column className="dataTableTotal"  footer={"$" + this.state.Total_allocated_amount}/>}
            {Total_allocated_shares &&<Column className="dataTableTotal"  footer={Total_allocated_shares}/>}
            <Column className="dataTableTotal" footer="" />
        </Row>

      </ColumnGroup>;

    if (AllocationView && AllocationView.length > 0) {
      return (


        <div className="table">
          <ul id="listContent">
          {/* add css transition here */}
          {/* add css animation for archiving offering */}
          <ReactCSSTransitionGroup
            transitionName="fade"
            transitionEnterTimeout={300}
            transitionLeaveTimeout={300}>
            {AllocationView.map((item,i) => (
                <li className="allocationList_li" key={i} >
                  <div className="row">
                  <div className="col-sm-1">
                  </div>
                  <div className="col-sm-10 allocationlistrow">
                    <div className="row ">
                        <div className="col-sm-5 allocationlistLeft">
                          {/* <div className="row" style={{display:'inline-flex',width:'100%'}}>
                             <div className="icon_column">
                                <div className="icon_2">{item.name.charAt(0)}</div>
                              </div>
                              <div className="allocationName">{item.name}</div>
                          </div>*/}
                          <div className="row ">

                          <div className="col-sm-2">
                                {item.logo_thumb !==null && <img style={{paddingTop:12}} src={item.logo_thumb}/>}
                                {item.logo_thumb ===null && <div className="icon_column">
                                  <div className="icon_2">{item.name.charAt(0)}</div>
                                </div>}
                            </div>
                            {/*<div className="col-sm-2">
                               <div className="icon_column">
                                <div className="icon_2">{item.name.charAt(0)}</div>
                              </div>
                            </div>*/}
                            <div className="col-sm-10 allocationcontent">
                            <Link to={`/allocations/details/${item.ext_id}`} >
                              <div className="allocationName" >{item.name}</div>
                            </Link>

                              <p className="no_margin titles" style={{marginTop:10}}>Allocated Date : <span className="sub_titles">{mmddyy(item.allocated_date)}</span></p>
                              <p className="no_margin titles">Allocated Amount : <span className="sub_titles">$ {item.total_allocated_amount.toLocaleString()}</span></p>
                              <p className="no_margin titles">Allocated Shares : <span className="sub_titles">{item.total_allocated_shares}</span></p>
                              <p  className="no_margin titles">
                                  Final Price <span className="sub_titles"> $ {currency(item.final_price)}</span>
                              </p>
                              {item.allocated === 'completed'  && <p  className="no_margin titles"> <input type="checkbox" value={item.checked} onChange={(event) =>this.selectedOffering(event,item,i)} /> Intact File </p>}

                            </div>
                          </div>

                        </div>

                        {item.allocated === 'in_progress' && <div className="col-sm-7 allocationlistbtns" >
                          <div className="buttonView">
                           {/*} <button className="btn btn-primary" onClick={()=>this.openSixtyMinuteModal(item)}>60 min email</button>*/}
                            <button className="btn btn-primary"  onClick={()=>this.runAllocations(item)}>Run</button>
                            <button className="btn btn-primary" disabled={item.total_allocated_shares <= 0 } onClick={()=>{this.getValue(item)}}> Show</button>
                          </div>
                        </div>}
                         {item.allocated === 'completed' && <div className="col-sm-3 allocationlistbtns">

                            <div className="gridButtons">
                                <button className="btn btn-primary" onClick={()=>{this.showCompleteAllocation(item)}} disabled={item.total_allocated_shares
                                    <=0 }> Show</button>

                                <button className="btn btn-primary" onClick={()=>{this.openAllocationEmailModal(item)}}>Send Allocation</button>
                            </div>
                        </div>}
                         {item.allocated === 'completed' &&<div className="col-sm-4 allocationlistbtns">

                            <button className="btn btn-primary"> Send to TS</button>
                            <button className="btn btn-primary" onClick={()=>{this.collapseOffering(item)}} >Final Prospectus</button>

                        </div>}
                        </div>
                        <div className="col-sm-1">
                        </div>
                      {item.isOpen &&<div className="row offeringcollapse" >
                          <p className="header">Final Prospectus</p>
                          <input type="text" value={item.prospectus_url}  onChange={(event)=>{this.handleProspectusChange(event,item)}} />
                          <button className="btn btn-submitprospectus" onClick={()=>{this.openFinalProspectusModal(item)}} >Send Final Prospectus</button>
                      </div>}
                    </div>
                  </div>


                </li>



              ))
              }

            </ReactCSSTransitionGroup>
            {/* end css transition here */}
          </ul>
           <ShowAllocationModal showAllocations={showAllocations} openshowAllocation={this.state.openshowAllocation}  onClose={this.onCloseModal} editAllocation={this.state.isEdit} finalPrice={this.state.final_price}  allocationInfo={this.state.allocationInfo} ></ShowAllocationModal>
           {this.state.sendSixtyMinuteConfirm && <Modal open={this.state.sendSixtyMinuteConfirm} onClose={this.closeSixtyMinuteModal}>
                <div>
                  <div className="ModalHeader">
                      <button className="closeBtn float_right" onClick={this.closeSixtyMinuteModal}>
                          <span aria-hidden="true" >&times;</span>
                      </button>
                  </div>
                  <div>
                  <div className="row">
                      <div className="confirmAllocationPopup">
                        <p>Do you want to send 60 minute email ?  </p>
                        <div className="row">
                          <button style={{margin:10}} className="buttons btn-success tableButtons"  onClick={()=>this.doSendSixtyMinuteEmail(this.state.sixtyMinuteAllocationInfo)}>Save </button>
                          <button style={{margin:10}} className="buttons btn-warning tableButtons " onClick={this.closeSixtyMinuteModal}> Cancel </button>
                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
              </Modal>}

              {this.state.allocationEmailConfirm && <Modal open={this.state.allocationEmailConfirm} onClose={this.closeAllocationEmailModal}>
                    <div>
                      <div className="ModalHeader">
                          <button className="closeBtn float_right" onClick={this.closeAllocationEmailModal}>
                              <span aria-hidden="true" >&times;</span>
                          </button>
                      </div>
                      <div>
                      <div className="row">

                          <div className="confirmAllocationPopup">
                            <p> Do you want to send allocation email  ?  </p>
                            <div className="row">
                              <button style={{margin:10}} className="buttons btn-success tableButtons"  onClick={()=>this.doSendAllocationEmail(this.state.allocationEmailAllocationInfo)}>Send </button>
                              <button style={{margin:10}} className="buttons btn-warning tableButtons " onClick={this.closeAllocationEmailModal}> Cancel </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      </div>
                  </Modal>}

                  {showAllocations && showAllocations.length==0 &&  this.state.open &&
                      <Modal open={this.state.open} onClose={this.onCloseModal}>
                      <div>
                        <div className="ModalHeader">
                            <button className="closeBtn float_right" onClick={this.onCloseModal}>
                                <span aria-hidden="true" >&times;</span>
                            </button>
                        </div>
                        <div className="row text_center">
                            <p style={{display: 'inline-flex'}}>
                              <span className="dataTableTotal" style={{paddingLeft:20}}>Final Offering Price : ${this.state.final_price}  </span>
                            </p>
                        </div>
                        <div>
                         <div className="row">
                            <div className="runAllocationPopup" >
                              <p>Firstly you need to run allocation</p>
                            </div>
                          </div>
                        </div>
                        </div>
                      </Modal>}

                        {this.state.runErrorModal && <Modal open={this.state.runErrorModal} onClose={this.closeRunErrorModal}>
                          <div>
                            <div className="ModalHeader">
                                <button className="closeBtn float_right" onClick={this.closeRunErrorModal}>
                                    <span aria-hidden="true" >&times;</span>
                                </button>
                            </div>
                            <div>
                            <div className="row">
                                <div className="confirmAllocationPopup">
                                  <p> Invalid Offering status </p>
                                  <div className="row">
                                        <button style={{margin:10}} className="buttons btn-warning tableButtons " onClick={this.closeRunErrorModal}> Close </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            </div>
                        </Modal>}

                        {this.state.sendProspectConfirm && <Modal open={this.state.sendProspectConfirm} onClose={this.closeFinalProspectusModal}>
                          <div>
                            <div className="ModalHeader">
                                <button className="closeBtn float_right" onClick={this.closeFinalProspectusModal}>
                                    <span aria-hidden="true" >&times;</span>
                                </button>
                            </div>
                            <div>
                            <div className="row">
                                <div className="confirmAllocationPopup">
                                  <p> Please confirm this is the final  prospectus of this allocation. You want to send final prospectus ?  </p>
                                  <div className="row">
                                    <button style={{margin:10}} className="buttons btn-success tableButtons"  onClick={()=>this.sendFianlProspectus(this.state.sendProspectusAllocationInfo)}> Save </button>
                                    <button style={{margin:10}} className="buttons btn-warning tableButtons " onClick={this.closeFinalProspectusModal}> Cancel </button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            </div>
                        </Modal>}
        </div>
      );
    } else if (AllocationView && AllocationView.length === 0) {
      return (
        <div className="name" style={{ paddingTop: '30px', paddingLeft: '20px' }}>
            No result found
          </div>
      );
    }
    return (null);
  }
}

AllocationTableView.propTypes = {
  AllocationView: PropTypes.any,
  getNewPage: PropTypes.func,
  showAllocations: PropTypes.any,
  completeAllocationRes: PropTypes.any,
  RunAllocationError: PropTypes.any,
  finalProspectusSuccess: PropTypes.any,
  sixtyMinuteSuccess: PropTypes.any,
  AllocationEmailSuccess: PropTypes.any
};

function mapDispatchToProps(dispatch) {
  return {
    showallocation:(id)=>{dispatch(fetchShowAllocation(id))},
    // completeAllocation:(id)=>{dispatch(fetchCompleteAllocation(id))},
    runAllocation:(id)=>{dispatch(fetchRunAllocation(id))},
    sendProspectusAllocation:(ext_id, prospectus_url)=>{dispatch(sendProspectusAllocation(ext_id, prospectus_url))},
    sixtyMinuteEmailAllocation:(ext_id)=>{dispatch(sixtyMinuteEmailAllocation(ext_id))},
    AllocationEmail:(ext_id)=>{dispatch(AllocationEmail(ext_id))},
    callIntactUpload: () => { dispatch(callIntactUpload())},
    showAllocationsReset: () => { dispatch(fetchShowAllocationSuccess(null))}



  };
}

const mapStateToProps = createStructuredSelector({
  allocations: makeSelectAllocations(),
  completeAllocationRes:makegetCompleteAllocation(),
  showAllocations :makegetShowAllocation(),
  RunAllocationError:makeRunAllocationError(),
  finalProspectusSuccess:makeFinalProspectSuccess(),
  sixtyMinuteSuccess:makeSixtyMinuteSuccess(),
  AllocationEmailSuccess:makeAllocationEmailSuccess(),
});

export default connect(mapStateToProps, mapDispatchToProps)(AllocationTableView);
