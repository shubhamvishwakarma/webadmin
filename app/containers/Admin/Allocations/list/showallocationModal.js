import React ,{ Component } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { connect } from 'react-redux';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import { mmddyy, convertToLocalString, currency } from 'containers/App/Helpers';


import Modal from 'react-responsive-modal';
import {DataTable} from 'primereact/components/datatable/DataTable';
import {Column} from 'primereact/components/column/Column';
import {Button} from 'primereact/components/button/Button';
import {InputText} from 'primereact/components/inputtext/InputText';
import {ColumnGroup} from 'primereact/components/columngroup/ColumnGroup';
import {Row} from 'primereact/components/row/Row';
import { fetchShowAllocation,fetchRunAllocation ,fetchSaveAllocation,fetchCompleteAllocation,sendProspectusAllocation,sixtyMinuteEmailAllocation,AllocationEmail,callIntactUpload} from './../actions';
import { makeSelectAllocations,makegetShowAllocation,makegetCompleteAllocation ,makeRunAllocationError,makeFinalProspectSuccess,makeSixtyMinuteSuccess,makeAllocationEmailSuccess,makeSaveAllocationSuccess} from '../selectors';
class ShowAllocationModal extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      isSubmit:null,
      width:1200,
      allocatedShareDifference:0,
      isAllocatedChange: null,
      isUIrender:null,
      Total_initial_shares:0,
      Total_allocated_amount:0,
      final_price:0,
      isSave:null,
      updateStates: false,
      isGrater:false,
      isSaveConfirm:false,
      isComplete: false,
      isCompleteConfirm: false,

    }
    this.allocatesAmountTemplate=this.allocatesAmountTemplate.bind(this);
    this.allocatesAccountTemplate=this.allocatesAccountTemplate.bind(this);
    this.allocatesEmailTemplate=this.allocatesEmailTemplate.bind(this);
    this.buyingpowerTemplate=this.buyingpowerTemplate.bind(this);
    this.SharesEditor=this.SharesEditor.bind(this);
    this.inputTextEditor=this.inputTextEditor.bind(this);
    this.closeSaveConfirmModal=this.closeSaveConfirmModal.bind(this)
  }


    buyingpowerTemplate(rowData){
        if(Number(rowData.allocated_amount)>Number(rowData.buying_power)){
          rowData.greater=true;
        }else{
          rowData.greater=false;
        }
       return <div className={"ui-cell-data "+ (rowData.greater==true ? 'update-error' :'')} > $ {currency(rowData.buying_power)}</div>
    }


    allocatesEmailTemplate(rowData){
          rowData.allocated_amount=Number(rowData.allocated_shares) * Number(this.state.final_price);
          if(Number(rowData.allocated_amount)>Number(rowData.buying_power)){
            rowData.greater=true;
          }else{
            rowData.greater=false;
          }
        return <div className={"ui-cell-data "+ (rowData.greater==true ? 'update-error' :'')}> {rowData.email}</div>
    }


   allocatesAccountTemplate(rowData){
          rowData.allocated_amount=Number(rowData.allocated_shares) * Number(this.state.final_price);
          if(Number(rowData.allocated_amount)>Number(rowData.buying_power)){
            rowData.greater=true;
          }else{
            rowData.greater=false;
          }
        return <div className={"ui-cell-data "+ (rowData.greater==true ? 'update-error' :'')}> {rowData.account_name}</div>
    }
    allocatesAmountTemplate(rowData){
        rowData.allocated_amount=Number(rowData.allocated_shares) * Number(this.state.final_price);
        if(Number(rowData.allocated_amount)>Number(rowData.buying_power)){
          rowData.greater=true;
        }else{
          rowData.greater=false;
        }
      return <div className={"ui-cell-data "+ (rowData.greater==true ? 'update-error' :'')}> $ {currency(rowData.allocated_amount)}</div>
    }

    SharesEditor(props) {

        props.rowData.allocated_amount = Number(props.rowData.allocated_shares) * Number(this.state.final_price);

        if(Number(props.rowData.allocated_amount)>Number(props.rowData.buying_power)){
          props.rowData.greater=true;
           this.state.isSave=true;
        }else{
          props.rowData.greater=false;
          if(this.state.isUIrender){
            this.state.isSave=false;
          }

        }
        return this.inputTextEditor(props, 'allocated_shares');
    }

    inputTextEditor(props, field) {
        props.rowData.allocated_amount = Number(props.rowData.allocated_shares) * Number(this.state.final_price);

        if(Number(props.rowData.allocated_amount)>Number(props.rowData.buying_power)){
          props.rowData.greater=true;
        }else{
          props.rowData.greater=false;
        }
        return <InputText  type="text" value={props.rowData.allocated_shares} onChange={(e) => this.onEditorValueChange(props, e.target.value)} />;
    }
    onEditorValueChange(props, value) {
          props.rowData.isEdit=true;
          let updatedfield = [...props.value];
          updatedfield[props.rowIndex][props.field] = value;
          this.setState({pendingAllocation: updatedfield,isAllocatedChange:false,isUIrender:true,isGrater:false})
          setTimeout(()=>{
             this.setState({
                isGrater: false,
                updateStates:true
              })
          },500)
    }

    saveAllocations(){
       let tempNewAllocations=[];
        for(let items of this.props.showAllocations){
          if(items.isEdit){
            tempNewAllocations.push({email: items.email, allocated_amount: items.allocated_amount, allocated_shares: items.allocated_shares, buying_power: items.buying_power,name:items.name,account_name:items.account_name});
          }
        }

        let d_str = JSON.stringify(tempNewAllocations);
        // let sendAllocationRequest={
        //   ext_id:this.props.allocationInfo.ext_id,
        //   data_str: d_str
        // }
        let sendAllocationRequest={
          ext_id:this.props.allocationInfo.ext_id,
          data_str: tempNewAllocations
        }
      this.props.saveAllocationData(sendAllocationRequest)
    }

    openSaveConfirmModal=()=>{
      this.setState({
        isSaveConfirm:true
      })
    }
    closeSaveConfirmModal=()=>{
      this.setState({
        isSaveConfirm:false,
        isSave:false,
        updateStates:false
      })
    }

    completeAllocations(){
      this.props.completeAllocation(this.props.allocationInfo.ext_id);
      this.openCompleteModal();
    }
    openCompleteModal=()=>{
      this.setState({ isComplete: true });
    }
    closeCompleteModal= () =>{
      this.setState({ isComplete: false ,updateStates:false});
    }

    openCompleteConfirmModal=()=>{
      this.setState({
        isCompleteConfirm:true
      })
    }
    closeCompleteConfirmModal=()=>{
      this.setState({
        isCompleteConfirm:false
      })
    }



  render() {
    const {showAllocations,editAllocation,finalPrice,allocationInfo,completeAllocationRes,saveAllocationRes}=this.props;
    this.state.final_price=finalPrice;
    if(completeAllocationRes != undefined){
      this.closeCompleteConfirmModal();
    }

    if(saveAllocationRes === true){
        this.state.isSaveConfirm=false;
        this.state.isSave=false;
        this.state.updateStates=false;
      // this.closeSaveConfirmModal();
    }


    let Total_allocated_shares=0;
    let Total_BuyingPower=0;
    let Total_allocated_amount=0;
    if(showAllocations){
        this.state.isSave=false;
        this.state.isGrater=false;

      for(let items of showAllocations){
            Total_allocated_shares=Number(Total_allocated_shares) + Number(items.allocated_shares);
            Total_BuyingPower=Number(Total_BuyingPower) + Number(items.buying_power);
            Total_allocated_amount=Number(Total_allocated_amount) + Number(items.allocated_amount);

            this.state.Total_allocated_amount=Total_allocated_amount;
            if(Number(items.allocated_amount)>Number(items.buying_power)){
              this.state.isSave=true;
              this.state.isGrater=true;
            }
            if(items.greater){
              this.state.isGrater=true;
            }
        }
        let Total_initial_allocated_shares=0;

          if(this.state.isAllocatedChange==null && Total_initial_allocated_shares==0){
            for(let item of this.props.showAllocations){
              Total_initial_allocated_shares=Number(Total_initial_allocated_shares) + Number(item.allocated_shares);
            }
              this.state.Total_initial_shares=Total_initial_allocated_shares;
          }
      }


    if(this.state.isAllocatedChange==false && this.state.isUIrender==true){
        if(this.state.Total_initial_shares < Total_allocated_shares){
            this.state.isSave=true;
            this.state.isUIrender=false;
            this.state.allocatedShareDifference= Number(Total_allocated_shares) - Number(this.state.Total_initial_shares);

        }else{
          this.state.allocatedShareDifference= Number(Total_allocated_shares) - Number(this.state.Total_initial_shares);

          this.state.isSave=true;
        }
        if(this.state.allocatedShareDifference==0){
          this.state.isSave=false;
        }

    }


    const margin = {top: 20, right: 20, bottom: 30, left: 40};
    if(!this.props.openshowAllocation) {
      return null;
    }

     let footerGroupShowAllocation = <ColumnGroup>
        <Row>
            <Column className="dataTableTotal" footer="Total" colSpan={1} />
            <Column footer="" />
            <Column footer="" />
            {this.state.Total_allocated_amount &&<Column className="dataTableTotal"  footer={"$" + this.state.Total_allocated_amount}/>}
            {Total_allocated_shares &&<Column className="dataTableTotal"  footer={Total_allocated_shares}/>}
            <Column className="dataTableTotal" footer="" />
        </Row>

      </ColumnGroup>;


      return (

        <div className="backdrop" style={{height:"100%", position: 'fixed',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            backgroundColor: 'rgba(0,0,0,0.3)',
            padding: 20,
            zIndex:1}}>
        <div style={{height:"100%"}}  onClick={this.props.onClose}>
        </div>

          <div className="modal" style={{height:550 ,backgroundColor: '#fff',
            borderRadius: 5,
            maxWidth: 1200,
            minHeight: 550,
            position:'absolute',
            margin: 'auto',
            padding: 15,display: 'block',top: '25px'}}>
                  <div>
                          <div className="ModalHeader" >
                              <button className="closeBtn float_right" onClick={this.props.onClose}>
                                  <span aria-hidden="true" >&times;</span>
                              </button>
                          </div>
                          <div className="row text_center">
                              <p style={{display: 'inline-flex'}}>
                                <span className="dataTableTotal" style={{paddingLeft:20}}>Final Offering Price : $ {this.state.final_price}  </span>
                              </p>
                          </div>
                          <div>
                            <DataTable value={showAllocations} editable={true}  responsive={true}  footerColumnGroup={footerGroupShowAllocation} scrollable={true} scrollHeight="300px" resizableColumns={true}  columnResizeMode="expand">
                              <Column className="dataTableNameColumn" field="name" header="User Name"  sortable={true} />
                              <Column className="tableCols" body={this.allocatesAccountTemplate}   field="account_name" header="User Account"  sortable={true} />
                              <Column  className="tableCols"  body={this.allocatesEmailTemplate} field="email" header="User Email"  sortable={true} />
                              <Column className="tableCols" body={this.allocatesAmountTemplate} field="allocated_amount" header="Allocated Amount"  sortable={true} />

                              <Column className="tableCols" body={this.allocatesSharesTemplate} field="allocated_shares" header="Allocated Shares" editor={editAllocation? this.SharesEditor:false}  sortable={true} />
                              <Column  className="tableCols" field="buying_power" body={this.buyingpowerTemplate} header="Buying Power"  sortable={true} />
                            </DataTable>
                            {editAllocation && <div className="row" style={{paddingRight:30,paddingTop:10}}>
                            <div className="col col-sm-9">
                              {this.state.isGrater && <p className="ErrorMessage" style={{textAlign:'left'}}>Allocated amount is greater than buying power, please change the allocated shares</p>}

                              {this.state.allocatedShareDifference>0 && <p className="ErrorMessage" style={{textAlign:'left'}}> Allocated Shares are  greater than the initial allocated shares by {this.state.allocatedShareDifference} share(s). Please allocate the appropriate amount to all the customers.</p>}
                              {this.state.allocatedShareDifference<0 && <p className="ErrorMessage" style={{textAlign:'left'}}>Allocated Shares are  less than the initial allocated shares by {Math.abs(this.state.allocatedShareDifference)} share(s). Please allocate the appropriate amount to all the customers.</p>}

                            </div>
                            <div className="col col-sm-3">

                              <Button disabled={this.state.isSave} className="ui-button-warning tableButtons " onClick={()=>this.openSaveConfirmModal()} >Save</Button>

                              <Button   disabled={this.state.isSave || this.state.updateStates}  className="ui-button-success tableButtons " onClick={()=>this.openCompleteConfirmModal()}>Finalize</Button>
                            </div>
                          </div>}
                        </div>
                      </div>
          </div>
                <Modal open={this.state.isSaveConfirm} onClose={this.closeSaveConfirmModal}>
                  <div>
                    <div className="ModalHeader">
                        <button className="closeBtn float_right" onClick={this.closeSaveConfirmModal}>
                            <span aria-hidden="true" >&times;</span>
                        </button>
                    </div>
                    <div>
                    <div className="row">
                        <div className="confirmAllocationPopup">
                        <p> Are you sure you want to save this version of the allocation list?</p>
                          {/*<p> Please confirm this is the final version of this allocation. You have allocated  {Total_allocated_shares} shares at a price of $ {this.state.final_price} for a total of $ {currency(Total_allocated_amount)} </p>*/}
                          <div className="row">
                                <button style={{margin:10}} className="buttons btn-success tableButtons"  onClick={()=>this.saveAllocations()}> Save </button>
                                <button style={{margin:10}} className="buttons btn-warning tableButtons " onClick={this.closeCompleteConfirmModal}> Cancel </button>
                          </div>
                        </div>
                      </div>
                    </div>
                    </div>
                  </Modal>
                  <Modal open={this.state.isCompleteConfirm} onClose={this.closeCompleteConfirmModal}>
                    <div>
                      <div className="ModalHeader">
                          <button className="closeBtn float_right" onClick={this.closeCompleteConfirmModal}>
                              <span aria-hidden="true" >&times;</span>
                          </button>
                      </div>
                      <div>
                      <div className="row">
                          <div className="confirmAllocationPopup">
                            <p> Please confirm this is the final version of this allocation. You have allocated  {Total_allocated_shares} shares at a price of $ {this.state.final_price} for a total of $ {currency(Total_allocated_amount)} </p>
                            <div className="row">
                                  <button style={{margin:10}} className="buttons btn-success tableButtons"  onClick={()=>this.completeAllocations()}> Finalize </button>
                                  <button style={{margin:10}} className="buttons btn-warning tableButtons " onClick={this.closeCompleteConfirmModal}> Cancel </button>
                            </div>
                          </div>
                        </div>
                      </div>
                      </div>
                  </Modal>
                   {completeAllocationRes !=undefined &&
                    <Modal open={this.state.isComplete} onClose={this.closeCompleteModal}>
                    <div>
                      <div className="ModalHeader">
                          <button className="closeBtn float_right" onClick={this.closeCompleteModal}>
                              <span aria-hidden="true" >&times;</span>
                          </button>
                      </div>
                      <div>
                        <div className="row">
                          <div className="runAllocationPopup" >
                            <p>Allocation Complete Successfully</p>
                          </div>
                        </div>
                      </div>
                      </div>
                    </Modal>}
        </div>
      )
    }
}

ShowAllocationModal.propTypes = {
  onClose : PropTypes.func.isRequired,
  openshowAllocation: PropTypes.bool,
  editAllocation: PropTypes.bool,
  showAllocations:PropTypes.any,
  handleMouseOver: PropTypes.func,
  handleMouseLeave: PropTypes.func,
  allocationInfo:PropTypes.any,
  finalPrice:PropTypes.any,
  saveAllocations:PropTypes.func,
  completeAllocationRes: PropTypes.any,
  saveAllocationRes:  PropTypes.any,
};

function mapDispatchToProps(dispatch) {
  return {
    saveAllocationData:(data)=>{dispatch(fetchSaveAllocation(data))},
    completeAllocation:(id)=>{dispatch(fetchCompleteAllocation(id))},

  };
}

const mapStateToProps = createStructuredSelector({
  completeAllocationRes:makegetCompleteAllocation(),
  saveAllocationRes:makeSaveAllocationSuccess(),

});
export default connect(mapStateToProps, mapDispatchToProps)(ShowAllocationModal);


