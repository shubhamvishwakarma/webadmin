/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import RaisedButton from 'material-ui/RaisedButton';
import { Link ,Redirect} from 'react-router-dom';
import AdminWrapper from 'components/AdminWrapper';
import { makeSelectAllocations,makegetShowAllocation,makegetCompleteAllocation ,makeRunAllocationError} from '../selectors';
import { fetchShowAllocation,fetchRunAllocation ,fetchSaveAllocation,fetchCompleteAllocation,callIntactUpload,fetchUpdatedAllocationDataSuccess,fetchOfferingSuccess} from './../actions';

import { Grid } from 'react-redux-grid';
import { mmddyy, convertToLocalString, currency } from 'containers/App/Helpers';


import ReactDataGrid from 'react-data-grid';
import {DataTable} from 'primereact/components/datatable/DataTable';
import {Column} from 'primereact/components/column/Column';
import {Button} from 'primereact/components/button/Button';
import {InputText} from 'primereact/components/inputtext/InputText';
import {ColumnGroup} from 'primereact/components/columngroup/ColumnGroup';
import {Row} from 'primereact/components/row/Row';

import AllocationTableView from './allocationTableView';

import Modal from 'react-responsive-modal';
import {ToastContainer, ToastStore} from 'react-toasts';


class AllocationList extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

    constructor(props) {
      super(props);
      this.state = {
        pendingAllocation:[],
        activeAllocation:[],
        searching:''
      };

      this.searchOffering=this.searchOffering.bind(this);
      this.sendIntactUpload=this.sendIntactUpload.bind(this);
    }
    componentDidMount() {

      this.props.resetUpdateAllocation();
      this.props.resetFetchAllocation();
      if (this.props.allocations) {
        this.setState({ activeAllocation: this.props.allocations});
      }
    }
    searchOffering(event){
      this.setState({
        searching:event.target.value
      })
    }
    sendIntactUpload(){
      if(this.state.activeAllocation.length>0){
        let extIds=[];
        for(let items of this.state.activeAllocation){
          if(items.checked==true){
             extIds.push(items.ext_id)
          }
        }
        this.props.callIntactUpload(extIds.join());
      }
    }

  render() {
    const { allocations,showAllocations ,completeAllocationRes,RunAllocationError} = this.props;
    const Header = (<div className="offer_head">Allocations</div>);
    let completeAllocation=[];
    let pendingAllocation=[];
      if(allocations){
          const filterData = this.state.searching? allocations.filter(row => row.name.toLowerCase().indexOf(this.state.searching.toLowerCase()) > -1) : allocations;

            for(let items of filterData){
                if(items.allocated=="in_progress"){
                    pendingAllocation.push(items);
                }
                if(items.allocated=="completed"){
                    items.checked=false;
                    completeAllocation.push(items);
                }
            }
            this.state.activeAllocation=completeAllocation;
            this.state.pendingAllocation=pendingAllocation;
      }
    return (

         <div>
            <ToastContainer store={ToastStore}/>
            <div className="main_outer_wrapper">
                  <div className="row allocationSubheader">
                      <div className="col-sm-5">
                          <div className="allocation_head">Allocations</div>
                      </div>
                      <div className="col-sm-7">
                          <div className=" allocationGlobalSearch">
                            <div className="search_icon"></div>
                            <input id="searchInput" type="text" placeholder="Search" onChange={this.searchOffering} />
                            <div className="search_close" onTouchTap={this.getAllOfferings}></div>

                              <button className="btn btn-newallocation" onClick={this.sendIntactUpload}>Intact File</button>
                            <Link to="/allocations/create_allocations">
                              <button className="btn btn-newallocation">New Allocation</button>
                            </Link>
                        </div>
                      </div>

                  </div>
                  <div className="allocationtabs">
                    <input type="radio" name="tabs" id="tab-broker-dealers-pending"  defaultChecked="checked"/>
                    <label htmlFor="tab-broker-dealers-pending" className="tab-left-label">PENDING</label>
                    <div className="tab">
                        <div className="row">
                           <AllocationTableView  AllocationView={this.state.pendingAllocation} />
                        </div>
                    </div>

                    <input type="radio" name="tabs" id="tab-broker-dealers" />
                    <label htmlFor="tab-broker-dealers" className="tab-left-label" style={{marginLeft:12}}>COMPLETE</label>
                      <div className="tab">
                        <div className="row">
                            <AllocationTableView  AllocationView={this.state.activeAllocation} />
                        </div>
                    </div>


                    <input type="radio" name="tabs" id="tab-help" />
                    <label htmlFor="tab-help" className="tab-left-label" style={{marginLeft:12}}>HELP</label>
                      <div className="tab">
                        <div className="row" style={{paddingLeft:20,paddingRight:20}}>
                           <p> 1.Create an IPO offering</p>
                           <p> 2.Update the IPO offering</p>
                           <p> 3. Click the 60 min email button</p>
                           <p> 4. Then under the pending tab in the allocation click Run</p>
                           <p> 5. Then click show allocation button </p>
                           <p> 6. Then modify the list</p>
                           <p> 7. Then click save button </p>
                           <p> 8. Then click finalize button </p>
                        </div>
                    </div>

                  </div>
          </div>

        </div>

    );
  }
}


AllocationList.propTypes = {
  allocations: PropTypes.any,
  showAllocations: PropTypes.any,
  completeAllocationRes: PropTypes.any,
  RunAllocationError: PropTypes.any,
};

function mapDispatchToProps(dispatch) {
  return {
    showallocation:(id)=>{dispatch(fetchShowAllocation(id))},
    runAllocation:(id)=>{dispatch(fetchRunAllocation(id))},
    saveAllocation:(data)=>{dispatch(fetchSaveAllocation(data))},
    completeAllocation:(id)=>{dispatch(fetchCompleteAllocation(id))},
    callIntactUpload: (ext_id) => { dispatch(callIntactUpload(ext_id))},
    resetUpdateAllocation: () => { dispatch(fetchUpdatedAllocationDataSuccess(null))},
    resetFetchAllocation: () => { dispatch(fetchOfferingSuccess(null))}



  };
}

const mapStateToProps = createStructuredSelector({
  allocations: makeSelectAllocations(),
  completeAllocationRes:makegetCompleteAllocation(),
  showAllocations :makegetShowAllocation(),
  RunAllocationError:makeRunAllocationError(),

});


export default connect(mapStateToProps, mapDispatchToProps)(AllocationList);
