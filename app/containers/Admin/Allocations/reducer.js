/*
 * Investor Reducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import {
  FETCH_ALLOCATION_LIST_SUCCESS,
  FETCH_OFFERING_LIST_SUCCESS,
  SUBMIT_ALLOCATION_DATE_TIME,
  FETCH_UPDATED_ALLOCATION_DATA_SUCCESS,
  FETCH_OFFERING_SUCCESS,
  FETCH_USER_MPID_SUCCESS,
  FETCH_USER_MPID_COMMISION_SUCCESS,
  FETCH_SHOW_ALLOCATION_DATA_SUCCESS,
  SAVE_ALLOCATION_DATA_SUCCESS,
  COMPLETE_ALLOCATION_DATA_SUCCESS,
  RUN_ALLOCATION_ERROR,
  FINAL_PROSPECTUS_ALLOCATION,
  FINAL_PROSPECTUS_SUCCESS,
  SIXTYMINUTEEMAIL_SUCCESS,
  ALLOCATION_EMAIL,
  ALLOCATION_EMAIL_SUCCESS,
  UPDATE_ALLOCATION,
  CALL_INTACT_FILE_UPLOAD,
} from './constants';

const initialState = fromJS({
  allocationList: null,
  pendingAllocationList: null,
  offeringList: null,
  updatedOfferingID: null,
  updatedAllocationData: [],
});


function adminAllocationReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_OFFERING_SUCCESS:
      return state.set('AllocationOffering',action.data)
    case  FETCH_USER_MPID_SUCCESS:
      return state.set('UserMpid',action.data)
    case FETCH_ALLOCATION_LIST_SUCCESS:
      return state.set('allocationList', action.data);
    case FETCH_OFFERING_LIST_SUCCESS:
      return state.set('offeringList', action.data);
    case SUBMIT_ALLOCATION_DATE_TIME:
      return state.set('updatedOfferingID', action.data.toJS().id);
    case FETCH_UPDATED_ALLOCATION_DATA_SUCCESS:
      return state.set('updatedAllocationData', action.data);
    case FETCH_USER_MPID_COMMISION_SUCCESS:
      return state.set('underWriterCommission',action.data);
    case FETCH_SHOW_ALLOCATION_DATA_SUCCESS:
       return state.set('showAllocation',action.data);
    case SAVE_ALLOCATION_DATA_SUCCESS :
       return state.set('saveAllocation',action.data);
    case COMPLETE_ALLOCATION_DATA_SUCCESS:
        return state.set('completeAllocationRes',action.data);
    case RUN_ALLOCATION_ERROR:
        return state.set('RunAllocationError',action.data);
    case FINAL_PROSPECTUS_SUCCESS:
        return state.set('finalProspectusSuccess',action.data);
    case SIXTYMINUTEEMAIL_SUCCESS:
        return state.set('sixtyMinuteSuccess',action.data);
    case ALLOCATION_EMAIL_SUCCESS:
        return state.set('AllocationEmailSuccess',action.data);
    case CALL_INTACT_FILE_UPLOAD:
        return state.set('allocationList',action.data);
    default:
      return state;
  }
}


export default adminAllocationReducer;
