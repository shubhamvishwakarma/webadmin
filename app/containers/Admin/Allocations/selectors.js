import { createSelector } from 'reselect';

const selecAllocations = (state) => state.get('adminAllocations');

const makeSelectAllocations = () => createSelector(
  selecAllocations,
  (adminState) => adminState.get('allocationList')
);


const makeSelectOfferings = () => createSelector(
  selecAllocations,
  (adminState) => adminState.get('offeringList')
);

const makeSelectUpdatedAllocationData = () => createSelector(
  selecAllocations,
  (adminState) => adminState.get('updatedAllocationData')
);

const makeSelectOffering = () => createSelector(
  selecAllocations,
  (adminState) => adminState.get('AllocationOffering')
);

const makeSaveAllocationSuccess = () => createSelector(
  selecAllocations,
  (adminState) => adminState.get('saveAllocation')
);


const makeShowUpdatedAllocationData = () => createSelector(
  selecAllocations,
  (adminState) => {
    const showAllocationData = adminState.get('updatedAllocationData');
    const initialValues = {};
    if (showAllocationData) {
      initialValues.final_price = showAllocationData.final_price;
      initialValues.click_shares = showAllocationData.click_shares;
      initialValues.asked_shares = showAllocationData.asked_shares;
      initialValues.underwriter_concession=showAllocationData.underwriter_concession;
    }
    return initialValues;
  }
);

const makeSelectAllocationFormInitialValues = () => createSelector(
  selecAllocations,
  (adminState) => {
    const allocationData = adminState.get('AllocationOffering');
    const initialValues = {};
    if (allocationData) {
      initialValues.final_shares = allocationData.final_shares;
      initialValues.final_price = allocationData.final_price;
      initialValues.asked_shares = "";
      initialValues.underwriter_concession_amount=allocationData.underwriter_concession_amount;
    }
    return initialValues;
  }
);

const makeSelectUserMpid = () => createSelector(
  selecAllocations,
  (adminState) => adminState.get('UserMpid')
);

const makeSelectUserMpidCommision = () => createSelector(
  selecAllocations,
  (adminState) => adminState.get('underWriterCommission')
)
const makegetShowAllocation = () => createSelector(
  selecAllocations,
  (adminState) => adminState.get('showAllocation')
)


const makegetCompleteAllocation = () => createSelector(
  selecAllocations,
  (adminState) => adminState.get('completeAllocationRes')
)

const makeRunAllocationError = () => createSelector(
  selecAllocations,
  (adminState) => adminState.get('RunAllocationError')
)
const makeFinalProspectSuccess= () => createSelector(
  selecAllocations,
  (adminState) => adminState.get('finalProspectusSuccess')
)

const makeSixtyMinuteSuccess= () => createSelector(
  selecAllocations,
  (adminState) => adminState.get('sixtyMinuteSuccess')
)

const makeAllocationEmailSuccess= () => createSelector(
  selecAllocations,
  (adminState) => adminState.get('AllocationEmailSuccess')
)





export {
  selecAllocations,
  makeSelectAllocations,
  makeSelectOfferings,
  makeSelectOffering,
  makeSelectUpdatedAllocationData,
  makeSelectAllocationFormInitialValues,
  makeSelectUserMpid,
  makeSelectUserMpidCommision,
  makegetShowAllocation,
  makegetCompleteAllocation,
  makeRunAllocationError,
  makeFinalProspectSuccess,
  makeSixtyMinuteSuccess,
  makeAllocationEmailSuccess,
  makeSaveAllocationSuccess,
  makeShowUpdatedAllocationData
};
