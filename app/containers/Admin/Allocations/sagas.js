/**
 * API calls for Ola
 */

import { call, put, all, takeLatest, select } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { BASE_URL } from 'containers/App/constants';
import { showGlobalLoader, closeGlobalLoader } from 'containers/App/actions';
import request from 'utils/request';
import * as actions from './actions';
import * as actionTypes from './constants';
import {ToastContainer, ToastStore} from 'react-toasts';
import Cookies from 'universal-cookie';
import { LogStaging } from '../../App/Helpers';
import axios from 'axios';

const cookies = new Cookies();
const getToken = (state) => state.getIn(['global', 'currentUser', 'token']);


function saveAllocation(url,data, config ){
  LogStaging(33)
    return axios.put(url,data, config );
}

//Final Submit API ^^^^^^^

function* handleSubmitAllocation(action){
  LogStaging(9)
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  const TOKEN = yield select(getToken);
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/offerings/create_allocation_process`;
  const BODY = JSON.stringify({
    ... action.data
  });
  let response;
  try {
    response = yield call(request, URL, {
      method: 'POST',
      headers: ajaxRequestHeaders,
      body:BODY
    });
    if (response) {
      ToastStore.success("Successfully Added");
      //TODO: this is a temp fix we should not navigate using this method, we should use the redux push mehtod to navigate user
      //with push the page does not reload, it will unmount the form component and mount the list component
     window.location=window.location.origin+"/allocations";
    }
  } catch (err) {

    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }

  } finally {
      yield put(closeGlobalLoader());
  }
}


function* watcherhandleSubmitAllocation() {
  yield takeLatest(actionTypes.SUBMIT_ALLOCATION, handleSubmitAllocation);
}


function* handleUpdateAllocation(action){
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });

  const BODY = JSON.stringify({
        ext_id:action.data.ext_id,
        final_price:action.data.final_price,
        click_shares:action.data.click_shares,
        effective_date:action.data.effective_date,
        underwriter_concession_amount:action.data.underwriter_concession_amount,
        underwriter_mpid_id:action.data.underwriter_mpid_id,
        offering_bd_brokerages_attributes:action.data.offering_bd_brokerages_attributes,
        asked_shares:action.data.asked_shares
  });
  const TOKEN = yield select(getToken);
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/offerings/${action.data.ext_id}/update_allocation_process`;
  let response;
  try {
    response = yield call(request, URL, {
      method: 'PUT',
      headers: ajaxRequestHeaders,
      body:BODY
    });
    if (response) {
      ToastStore.success("Successfully Update");
      //TODO: this is a temp fix we should not navigate using this method, we should use the redux push mehtod to navigate user
      //with push the page does not reload, it will unmount the form component and mount the list component
       window.location=window.location.origin+"/allocations";
    }
  } catch (err) {

    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }

  } finally {
      yield put(closeGlobalLoader());
  }
}


function* watcherhandleUpdateAllocation() {
  yield takeLatest(actionTypes.UPDATE_ALLOCATION, handleUpdateAllocation);
}




//Final Submit API ^^^^^^^

function* handleFetchMPIDComission(action){
  LogStaging(10)
      let ajaxRequestHeaders = new Headers({
        'Content-Type': 'application/json',
        Accept: 'application/json',
      });
      const TOKEN = yield select(getToken);
      yield put(showGlobalLoader());
      ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
      const URL = `${BASE_URL}/underwriters/underwriter_commission_percentage`;
      const BODY = JSON.stringify({
        executing_broker_mpid:action.name
      });
      let response;
      try {
        response = yield call(request, URL, {
          method: 'POST',
          headers: ajaxRequestHeaders,
          body:BODY
        });
        if (response) {
          yield put(actions.fetchUserMpidCommisionSuccess(response));
        }
      } catch (err) {
        if(err.response.status==401){
            cookies.remove('currentUser');
            cookies.remove('email');
            cookies.remove('role');
            window.location.href = '/login';
        }
          yield put(actions.fetchUserMpidCommisionSuccess({commission_percentage:''}));
      } finally {
          yield put(closeGlobalLoader());
      }
}


function* watcherhandleFetchMPIDComission() {
  yield takeLatest(actionTypes.FETCH_USER_MPID_COMMISION, handleFetchMPIDComission);
}


function* handleFetchOffering(action) {
  LogStaging(11)
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  const TOKEN = yield select(getToken);
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/offerings/${action.id}`;
  let response;
  try {
    response = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    if (response) {
      yield put(actions.fetchOfferingSuccess(response));
    }
  } catch (err) {
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
      console.log(err);
  } finally {
      yield put(closeGlobalLoader());
  }
}

function* watcherHandleFetchOffering() {
  yield takeLatest(actionTypes.FETCH_OFFERING, handleFetchOffering);
}

function* handleFetchUserMpId(action) {
  LogStaging(12);
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  const TOKEN = yield select(getToken);
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/offerings/bd_underwriter_mpids`;
  let response;
  try {
    response = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    if (response) {
      yield put(actions.fetchUserMpidSuccess(response));
    }
  } catch (err) {
      ToastStore.error('Unable to Fetch UserMPID');
      if(err.response.status==401){
          cookies.remove('currentUser');
          cookies.remove('email');
          cookies.remove('role');
          window.location.href = '/login';
      }
      console.log(err);
  } finally {
      yield put(closeGlobalLoader());
  }
}
function* watcherHandleFetchUserMpid() {
  yield takeLatest(actionTypes.FETCH_USER_MPID, handleFetchUserMpId);
}


function* handleFetchAllocationList() {
  LogStaging(13)
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  const TOKEN = yield select(getToken);
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/offerings/get_allocations`;
  let response;
  try {
    response = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    if (response) {
      yield put(actions.fetchAllocationListSuccess(response));
    }
  } catch (err) {
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
    console.log(err);
  } finally {
    yield put(closeGlobalLoader());
  }
}
function* watcherFetchAllocationList() {
  yield takeLatest(actionTypes.FETCH_ALLOCATION_LIST, handleFetchAllocationList);
}



// detail section sagas
function* handleSubmitDateTime(action) {
  LogStaging(14)
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  const TOKEN = yield select(getToken);
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const actionData = action.data.toJS();
  const BODY = JSON.stringify({
    ...actionData,
  });
  const URL = `${BASE_URL}/offerings/${actionData.id}/create_allocation`;
  let response;
  try {
    response = yield call(request, URL, {
      method: 'PUT',
      headers: ajaxRequestHeaders,
      body: BODY,
    });
    yield put(actions.submitDateTimeSuccess(response));
    yield put(push(`/allocations/create_allocations/${actionData.id}`));
  } catch (err) {
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
    yield put(actions.submitDateTimeFailed());
  } finally {
    yield put(closeGlobalLoader());
  }
}
function* watcherSubmitDateTime() {
  yield takeLatest(actionTypes.SUBMIT_ALLOCATION_DATE_TIME, handleSubmitDateTime);
}

function* handleFetchOfferingList() {
  LogStaging(15)
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  const TOKEN = yield select(getToken);
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/offerings/get_name`;
  let response;
  try {
    response = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    yield put(actions.fetchOfferingListSuccess(response));
  } catch (err) {
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
    console.log(err);
  } finally {
    yield put(closeGlobalLoader());
  }
}
function* watcherFetchOfferingList() {
  yield takeLatest(actionTypes.FETCH_OFFERING_LIST, handleFetchOfferingList);
}

function* handleCreateAllocation(action) {
  LogStaging(16)
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  const TOKEN = yield select(getToken);
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const actionData = action.data;
  const brokerages = actionData.broker_dealers.map((item) => {
    if (item.id) {
      return {
        percentage: item.commission_percentage,
        broker_dealer_id: item.broker_dealer_id,
        id: item.id,
      };
    } return {
      percentage: item.commission_percentage,
      broker_dealer_id: item.broker_dealer_id,
    };
  });

  const BODY = JSON.stringify({
    ext_id: actionData.id,
    final_price: actionData.final_price,
    final_shares: actionData.final_shares,
    offering_bd_brokerages_attributes: brokerages,
  });

  const URL = `${BASE_URL}/offerings/${actionData.id}/allocation_update_brokerages`;

  try {
    yield call(request, URL, {
      method: 'PUT',
      headers: ajaxRequestHeaders,
      body: BODY,
    });
    yield put(actions.submitCreateAllocationSuccess());
    yield put(push('/allocations'));
  } catch (err) {
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
    console.log(err);
  } finally {
    yield put(closeGlobalLoader());
  }
}
function* watcherCreateAllocation() {
  yield takeLatest(actionTypes.SUBMIT_CREATE_ALLOCATION_REQUEST, handleCreateAllocation);
}

function* handleFetchUpdatedAllocationData(action) {
  LogStaging(17)
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  const TOKEN = yield select(getToken);
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/offerings/${action.id}/offering_allocation_details`;
  let response;
  try {
    response = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    yield put(actions.fetchUpdatedAllocationDataSuccess(response));
  } catch (err) {
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
    console.log(err);
  } finally {
    yield put(closeGlobalLoader());
  }
}
function* watcherFetchUpdatedAllocationData() {
  yield takeLatest(actionTypes.FETCH_UPDATED_ALLOCATION_DATA, handleFetchUpdatedAllocationData);
}

function* handleSendNotification(action) {
  LogStaging(18)
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  const TOKEN = yield select(getToken);
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const BODY = JSON.stringify({
    ext_id: action.id,
    final_price: action.final_price,
  });
  const URL = `${BASE_URL}/offerings/${action.id}/allocation_only_finalprice`;
  try {
    yield call(request, URL, {
      method: 'PUT',
      headers: ajaxRequestHeaders,
      body: BODY,
    });
    yield put(actions.sendNotificationEmailSuccess());
   //window.location=window.location.origin+'/allocations';
    yield put(push('/allocations'));
  } catch (err) {
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
    ToastStore.error("There is some issue with data, please retry")
    console.log(err);
  } finally {
    yield put(closeGlobalLoader());
  }
}
function* watcherSendNotification() {
  yield takeLatest(actionTypes.SEND_NOTIFICATION_EMAIL, handleSendNotification);
}



function* handleShowAllocation(action){
  LogStaging(19)
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  const TOKEN = yield select(getToken);
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/offerings/show_allocation_list`;
  const BODY = JSON.stringify({
    ext_id: action.id
  });
  let response;
  try {
    response = yield call(request, URL, {
      method: 'POST',
      headers: ajaxRequestHeaders,
      body:BODY
    });
    if (response) {
      yield put(actions.fetchShowAllocationSuccess(response));
    }
  } catch (err) {

    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }

  } finally {
      yield put(closeGlobalLoader());
  }
}


function* watcherhandleShowAllocation() {
  yield takeLatest(actionTypes.FETCH_SHOW_ALLOCATION_DATA, handleShowAllocation);
}



function* handleRunAllocation(action){
  LogStaging(20)
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  const TOKEN = yield select(getToken);
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/offerings/execute_allocation_process`;
  const BODY = JSON.stringify({
    ext_id: action.id
  });
  let response;
  try {
    response = yield call(request, URL, {
      method: 'POST',
      headers: ajaxRequestHeaders,
      body:BODY
    });
    window.location=window.location.origin+'/allocations';
    // console.log(response);
    // if (response) {
    //   yield put(actions.fetchShowAllocationSuccess(response));
    // }
  } catch (err) {
    console.log(JSON.stringify(err));

     console.log(err.response);

    if(err.response.status==404){
      yield put(actions.RunAllocationError(err.response.status));
       yield put(actions.RunAllocationError(undefined));
     }


    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }

  } finally {
      yield put(closeGlobalLoader());
  }
}


function* watcherhandleRunAllocation() {
  yield takeLatest(actionTypes.FETCH_RUN_ALLOCATION_DATA, handleRunAllocation);
}



function* handleSaveAllocation(action) {
  LogStaging(21)
  yield put(showGlobalLoader());
   const TOKEN = yield select(getToken);
        let data = new FormData();
        data.append('data_array', JSON.stringify(action.data.data_str))
        var url=`${BASE_URL}/offerings/${action.data.ext_id}/save_allocation_list`;
        var config = {
          headers: {
            'accept': 'application/json',
            'accept-language': 'en_US',
            'Authorization':TOKEN,
            'content-type': `multipart/form-data; boundary=${data._boundary} `
          }
        };
        let response;
        try {
          response = yield call(saveAllocation,url,data, config)
          console.log(response);
          if(response.status==204){
            yield put(actions.fetchSaveAllocationSuccess(true));
            yield put(actions.fetchSaveAllocationSuccess(null));
          }

        } catch (err) {
          console.log(err.response.status);


          if(err.response.status==401){
              cookies.remove('currentUser');
              cookies.remove('email');
              cookies.remove('role');
              window.location.href = '/login';
          }
        } finally {

          yield put(closeGlobalLoader());
        }

  // var formData = new FormData();
  // formData.append('data_str', JSON.stringify(action.data.data_str));
  // const BODY = JSON.stringify({
  //   ...actionData,
  // });
  // const URL = `${BASE_URL}/offerings/${action.data.ext_id}/save_allocation_list`;
  // let response;
  // try {
  //   response = yield call(request, URL, {
  //     method: 'PUT',
  //     headers: ajaxRequestHeaders,
  //     body: formData,
  //   });
  //   console.log(response);
  //   // window.location=window.location.origin+"/allocations";
  //   //yield put(actions.fetchSaveAllocationSuccess(response));
  // } catch (err) {
  //   console.log(err.response.status);
  //   if(err.response.status==401){
  //       cookies.remove('currentUser');
  //       cookies.remove('email');
  //       cookies.remove('role');
  //       window.location.href = '/login';
  //   }
  // } finally {
  //   yield put(closeGlobalLoader());
  // }
}
function* watcherSaveAllocation() {
  yield takeLatest(actionTypes.SAVE_ALLOCATION, handleSaveAllocation);
}



function* handleCompleteAllocation(action) {
  LogStaging(22)
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  const TOKEN = yield select(getToken);
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const actionData = {
      'ext_id':action.id
  };
  const BODY = JSON.stringify({
    ...actionData,
  });



  const URL = `${BASE_URL}/offerings/${action.id}/complete_allocation`;
  let response;
  try {
    response = yield call(request, URL, {
      method: 'PUT',
      headers: ajaxRequestHeaders,
      body: BODY,
    });
    console.log(response);
    yield put(actions.fetchCompleteAllocationSuccess(response));
  } catch (err) {
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
  } finally {
    window.location=window.location.origin+'/allocations';
    yield put(closeGlobalLoader());
  }
}
function* watcherCompleteAllocation() {
  yield takeLatest(actionTypes.COMPLETE_ALLOCATION, handleCompleteAllocation);
}


function* handleFinalProspectus(action) {
  LogStaging(23)
  yield put(showGlobalLoader());

  const TOKEN = yield select(getToken);

  const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });

    const BODY = JSON.stringify({
      ext_id: action.ext_id,
      i_or_f :'f',
      prospectus: action.prospectus_url
    });

    let URL;
      URL = `${BASE_URL}/web_admin/update_prospectus`;
      let allocation;
      try {
        allocation = yield call(request, URL, {
          method: 'PUT',
          headers: ajaxRequestHeaders,
          body:BODY
        });
        yield put(actions.makeFinalProspectSuccess(allocation));
        yield put(actions.makeFinalProspectSuccess(null));
      } catch (err) {
        if(err.response.status==401){
          localStorage.clear();
          window.location.href = '/login';
        }
        console.log(err);
      } finally {
        yield put(closeGlobalLoader());
      }
}

function* watcherhandleFinalProspectusg() {
  yield takeLatest(actionTypes.FINAL_PROSPECTUS_ALLOCATION, handleFinalProspectus);
}




function* handlesixtyMinuteEmailAllocation(action) {
  LogStaging(24)
  yield put(showGlobalLoader());
  const TOKEN = yield select(getToken);

  const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });
    const BODY = JSON.stringify({
      ext_id: action.ext_id
    });
    let URL;
    URL = `${BASE_URL}/web_admin/sixty_min_email`;
    let allocation;
    try {
      allocation = yield call(request, URL, {
        method: 'PUT',
        headers: ajaxRequestHeaders,
        body:BODY
      });
       yield put(actions.makeSixtyMinuteSuccess(allocation));
       yield put(actions.makeSixtyMinuteSuccess(null));
    } catch (err) {
      if(err.response.status==401){
        localStorage.clear();
        window.location.href = '/login';
      }
      console.log(err);
    } finally {
       yield put(closeGlobalLoader());
    }
}

function* watchersixtyMinuteEmailAllocation() {
  yield takeLatest(actionTypes.SIXTYMINUTEEMAIL, handlesixtyMinuteEmailAllocation);
}



function* handleallocationEmail(action) {
  LogStaging(25)
  yield put(showGlobalLoader());

  const TOKEN = yield select(getToken);

  const ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Authorization': TOKEN,
  });
    const BODY = JSON.stringify({
      ext_id: action.ext_id
    });
    let URL;
    URL = `${BASE_URL}/web_admin/allocation_email`;
    let Allocation;
    try {
      Allocation = yield call(request, URL, {
        method: 'PUT',
        headers: ajaxRequestHeaders,
        body:BODY
      });
      ToastStore.success("Send Allocation Email Successfully");
       yield put(actions.makeAllocationEmailSuccess(Allocation));
       yield put(actions.makeAllocationEmailSuccess(null));
      //  window.location=window.location.origin+'/admin_offerings';
    } catch (err) {
      if(err.response.status==401){
        localStorage.clear();
        window.location.href = '/login';
      }
      console.log(err);
    } finally {
       yield put(closeGlobalLoader());
    }
}

function* watcherallocationEmail() {
  yield takeLatest(actionTypes.ALLOCATION_EMAIL, handleallocationEmail);
}




function* handleUploadIntactFile(action) {
  console.log(1)
  console.log(action);
  yield put(showGlobalLoader());
  const TOKEN = yield select(getToken);

    try{

          let ajaxRequestHeaders_FILE = new Headers({
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'Authorization': TOKEN,
          });
          let URL_FILE=`${BASE_URL}/offerings/intact_file_upload`;
          let Body_File=JSON.stringify({
                ext_id :action.ext_id,
                ipo_or_secondary: 2
          })
          let FileUpload=yield call(request,URL_FILE,{
              method:"POST",
              headers:ajaxRequestHeaders_FILE,
              body:Body_File
          })
          if(FileUpload)
          {
            alert("Success Fully Uploaded Intact File")
          }


 }
 catch(err){
        if(err.response.status==401){
            cookies.remove('currentUser');
            cookies.remove('email');
            cookies.remove('role');
            window.location.href = '/login';
        }
        console.log("err")
 }
 finally{
     yield put(closeGlobalLoader());
 }



}


function* watcherUploadOffering() {
  yield takeLatest(actionTypes.CALL_INTACT_FILE_UPLOAD, handleUploadIntactFile);
}




// Bootstrap sagas
export default function* allocationSagas() {
  yield all([
    watcherFetchAllocationList(),
    watcherSubmitDateTime(),
    watcherFetchOfferingList(),
    watcherCreateAllocation(),
    watcherFetchUpdatedAllocationData(),
    watcherSendNotification(),
    watcherHandleFetchOffering(),
    watcherHandleFetchUserMpid(),
    watcherhandleFetchMPIDComission(),
    watcherhandleSubmitAllocation(),
    watcherhandleShowAllocation(),
    watcherhandleRunAllocation(),
    watcherSaveAllocation(),
    watcherCompleteAllocation(),
    watcherhandleFinalProspectusg(),
    watchersixtyMinuteEmailAllocation(),
    watcherallocationEmail(),
    watcherhandleUpdateAllocation(),
    watcherUploadOffering(),
  ]);
}
