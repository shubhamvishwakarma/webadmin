import React from 'react/lib/React';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { reduxForm, Field } from 'redux-form/immutable';
import { TimePicker, SelectField } from 'redux-form-material-ui';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'redux-form-material-ui/lib/DatePicker';
import moment from 'moment';
import { withRouter } from 'react-router-dom';
import { mmddyy } from 'containers/App/Helpers';
import validate from './formValidation';
import * as actions from '../../actions';
import { makeSelectOfferings,makeSelectOffering,makeSelectUserMpid,makeSelectUpdatedAllocationData, makeSelectUserMpidCommision,makeShowUpdatedAllocationData } from '../../selectors';
import {ToastContainer, ToastStore} from 'react-toasts';
import { getOfferingPriceRangeNum,validatePrice,validateDate ,getAllocationPriceRangeNum,getAllocationUpdatePriceRangeNum} from 'containers/App/Helpers';
import { confirmAlert } from 'react-confirm-alert';
import './alert.css';
import Select from 'react-select';
import {Calendar} from 'primereact/components/calendar/Calendar';
// const fields = ['offering', 'final_shares', 'final_price','asked_shares','underwriter_concession'];

 // eslint-disable-line react/prefer-stateless-function
class UpdateAllocationForm extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(){
    super()

    let today = new Date();
    let month = today.getMonth();
    let year = today.getFullYear();
    let prevMonth = (month === 0) ? 11 : month - 1;
    let prevYear = (prevMonth === 11) ? year - 1 : year;

    let minDate = new Date();
    minDate.setMonth(prevMonth+1);
    minDate.setFullYear(prevYear);



    this.state={
      allocationId:'',
      offeringId:'',
      final_price:'',
      continueCount:0,
      sendEmail:false,
      max_date:'',
      max_time:'',
      max_price:'',
      UnderwriterValue:'',
      UnderwriterId:'',
      isPrice:false,
      selected_bd_underwriter_mpid_id: '',
      bd_underwriter_mpid_id:'',
      amountOfShares:'',
      underwriter_concession:'',
      selected_offeringId:'',
      effectiveDate:'',
      isUIRender:false,
      DisableDropdown: true,
      isChangeOffering:false,
      isLoad:true,
      date1:'',
      minDate:minDate,
      isEffectiveChecked: false,
      asked_shares:''
    }
    this.handleContinue=this.handleContinue.bind(this);
    this.handleSendNotification=this.handleSendNotification.bind(this);
    this.handlefetchOffering=this.handlefetchOffering.bind(this);
    this.mpIDhandleChange=this.mpIDhandleChange.bind(this);
    this.changeBrokerValue=this.changeBrokerValue.bind(this);
    this.selectTBD=this.selectTBD.bind(this);


  }

  handlefetchOffering(event){
    this.setState({
      sendEmail:false,
      effectiveDate:this.state.effectiveDate,
      isPrice:true,
      underwriter_concession:'',
      selected_offeringId:event,
      isChangeOffering:false,
      isLoad:false,
      isEffectiveDateChange:false
    })


    if(event.value){
       this.props.fetchOffering(event.value);
    }
  }
  componentWillMount() {
    this.handlefetchUserMpid();
    let id=this.props.match.path.split("details/")[1];


    this.setState({
      allocationId:id
    })
    this.props.fetchAllocation(id);
    this.props.fetchOffering(id);


  }

  handlefetchUserMpid(){
    setTimeout(()=>{
      this.props.fetchUserMpid();
    },500)

  }




   componentWillReceiveProps(nextProps) {

    if(this.state.isLoad==true){
          if(nextProps.AllocationData && nextProps.offerings){
                  let id=this.props.match.path.split("details/")[1];
                  const selectedOfferingObject = {
                    value: id,
                    label: nextProps.AllocationData.name
                  }
                  this.setState({
                    selected_offeringId: selectedOfferingObject
                  });

                  if(nextProps.AllocationData && nextProps.AllocationData.final_price!==null){
                      this.setState({
                        offeringId:id,
                        final_price:nextProps.AllocationData.final_price,
                        isEmailShow:false
                      })
                  }
                  if(nextProps.AllocationData.click_shares>=0 && nextProps.AllocationData.click_shares!==null){
                    this.setState({
                      amountOfShares: nextProps.AllocationData.click_shares
                    });
                  }
                  if(nextProps.AllocationData.underwriter_concession){
                    this.setState({
                      underwriter_concession: nextProps.AllocationData.underwriter_concession
                    });
                  }
          }

          if(nextProps.AllocationData){
            if(nextProps.AllocationData.effective_date!== null && nextProps.AllocationData.effective_date!== undefined){
              this.setState({
                effectiveDate:new Date(moment(nextProps.AllocationData.effective_date).format('MM/DD/YYYY')),
                isEffectiveChecked: false
              })
            }else{
               this.setState({
                effectiveDate:'',
                isEffectiveChecked: true
              })
            }

          }
          if(nextProps.AllocationData && nextProps.UserMpid){
              const selectedMPIDObject = {
                value: nextProps.AllocationData.underwriter_mpid,
                label: nextProps.AllocationData.underwriter_mpid
              }
              if(nextProps.AllocationData.underwriter_mpid && nextProps.AllocationData.underwriter_mpid){
                  this.setState({
                    selected_bd_underwriter_mpid_id: selectedMPIDObject
                  });
              }
          }

      }else{
         if(nextProps.offering){
                let id=this.props.match.path.split("details/")[1];
                  const selectedOfferingObject = {
                    value: nextProps.offering.ext_id,
                    label: nextProps.offering.name
                  }
                  this.setState({
                    selected_offeringId: selectedOfferingObject
                  });

                  if(nextProps.offering && nextProps.offering.final_price!==null){
                      this.setState({
                        offeringId:id,
                        final_price:nextProps.offering.final_price,
                        isEmailShow:false
                      })
                  }
                  if(nextProps.offering && nextProps.offering.click_shares!==null){
                    this.setState({
                      amountOfShares: nextProps.offering.click_shares
                    });
                  }
                  if(nextProps.offering.underwriter_concession_amount){
                    this.setState({
                      underwriter_concession: nextProps.offering.underwriter_concession_amount
                    });
                  }
          }

          if(nextProps.offering && nextProps.UserMpid){
              const selectedMPIDObject = {
                value: nextProps.offering.bd_underwriter_mpid_id,
                label: nextProps.offering.bd_underwriter_mpid_id
              }
              if(nextProps.offering.bd_underwriter_mpid_id){
                  this.setState({
                    selected_bd_underwriter_mpid_id: selectedMPIDObject
                  });
              }
         }

      }


    }



   handleAllChanges(){
    this.setState({
      // final_price:this.final_price.value,
      effectiveDate:this.state.effectiveDate,
      // amountOfShares:this.amountOfShares.value,
      // underwriter_concession:this.Underwriter.value,
      // asked_shares: this.asked_shares.value
    })

  }

  handlePriceChanges(){
    // this.props.fetchAllocation(this.selectOffering.value);
    this.setState({
        sendEmail:false,
        isPrice:false,
        // final_price: this.final_price.value,
        isChangeOffering:true

    })
    if(this.final_price.value !== ''){
       let priceRange=[];
      if(this.state.isChangeOffering){
         priceRange=getAllocationPriceRangeNum(this.props.AllocationData);
      }else{
         priceRange=getAllocationUpdatePriceRangeNum(this.props.offering);
      }

        //  offering_price_attributes
         let res=validatePrice(priceRange,parseInt(this.final_price.value))
            if(res){
                // this.props.fetchAllocation(this.selectOffering.value);
                this.setState({
                  sendEmail:false,
                  continueCount:2,
                  isPrice:false,
                  final_price: this.final_price.value
                })
            }
            else{
                this.setState({
                    sendEmail:true
                })
            }
     }
  }


  handleContinue(formData){
    let getFormData=formData.toJS();
      var errorVal=moment(this.state.effectiveDate,'MM/DD/YYYY').isValid();
      // if(!this.state.isEffectiveChecked){
          (!errorVal)?this.effective_dateErr.textContent="Please Select Valid Date": this.effective_dateErr.textContent="";

          if(this.state.isEffectiveDateChange){
              var diff=moment(this.state.effectiveDate,'MM/DD/YYYY').diff(new Date(),"days")
              if(diff>=0)
              {
                  if(errorVal){
                      this.setState({
                        continueCount:1,
                        effectiveDate:this.state.effectiveDate,
                      })
                  }
              }
              else{

                this.effective_dateErr.textContent="Please select future Date";
                return false
              }
          }
      // }else{
      //   this.effective_dateErr.textContent="";
      // }


        if(this.props.AllocationData.broker_dealers.length>0){
            var offering_bd_brokerages_attributes=this.props.AllocationData.broker_dealers.map((item)=>{
              return {"broker_dealer_name":item.name,"bd_commission_percentage":item.commission_percentage}
            })
        }else{
            var offering_bd_brokerages_attributes=[];
        }
        let FinalData={
          ext_id:this.state.selected_offeringId.value,
          final_price:getFormData.final_price,
          click_shares:getFormData.click_shares,
          effective_date:  moment(this.state.effectiveDate).format('MM/DD/YYYY'),
          underwriter_concession_amount:getFormData.underwriter_concession,
          underwriter_mpid_id:this.state.selected_bd_underwriter_mpid_id.value,
          offering_bd_brokerages_attributes,
          asked_shares: getFormData.asked_shares
        }
        this.props.postAllocation(FinalData);


  }

  mpIDhandleChange(event){

    this.setState({
      selected_bd_underwriter_mpid_id:event,
      bd_underwriter_mpid_id:event.value,
      isEmailShow:false
    })

    //this.handleAllChanges(this);
  }

  handleSendNotification(id){
      this.setState({
        continueCount:2
      })
        this.submit(()=>{
          this.props.sendEmailNotification(this.selectOffering.value,this.max_price.value)
        })
  }



  handleUserMpid(){
    this.props.fetchCommission(this.selectUserMpId.value)
  }

  changeBrokerValue(item,event){
    this.setState({ isUIRender:true})
    item.commission_percentage=event.target.value
    setTimeout(()=>{
       this.setState({ isUIRender:false})
    },100)

  }
  changeEffectiveDate(evant){
      this.setState({effectiveDate: evant.value,isEffectiveDateChange:true,isEffectiveChecked:false})
  }

  selectTBD(){
      if(this.state.isEffectiveChecked==false){
        this.setState({
          isEffectiveChecked: true,
          effectiveDate:'',

        });
      }else{
        this.setState({
          isEffectiveChecked: false,
        });
      }
      this.effective_dateErr.textContent="";
  }


  goToBack(){
      this.props.resetData(null);
      this.props.history.goBack()
  }
  render() {
    const { handleSubmit, submitting, handleDateTimeSubmit, offerings,offering,UserMpid,AllocationData,UnderwriterCommssion,history} = this.props;

    let {continueCount,sendEmail} =this.state;
    let DateTime="";
    if(offering){
        if(this.state.isPrice==true && this.state.isChangeOffering==true && offering.final_price !== undefined){
          this.final_price.value=offering.final_price;
          this.handlePriceChanges();
        }

    }

      DateTime=(<div className="col-8 col-sm-12" style={{ padding: '0px 0px 0px 0px' }}>
                    <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>

                      <label className="update-label">Effective Date</label>
                        <Calendar  readOnlyInput={true} minDate={this.state.minDate} value={this.state.effectiveDate} onChange={(e) => this.changeEffectiveDate(e)}></Calendar>

                      <label style={{color:"red",fontSize:14}} ref={(Elmt)=>{this.effective_dateErr=Elmt}}></label>
                    </div>
                    <div className="col-6 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                        <label className="update-label" htmlFor="final_price">Final Price</label>
                        <Field  name="final_price" className="update-input" type="text"  component='input'/>

                        {/*<input type="text" className="update-input" ref={(Elmt)=>{this.final_price=Elmt}}  value={this.state.final_price}  placeholder="" onChange={this.handlePriceChanges.bind(this)}/>
                        <label style={{color:"red",fontSize:14}} ref={(Elmt)=>{this.max_priceErr=Elmt}}></label>*/}

                    </div>

                  </div>)
    //}
    let UserMpidList=[];
    let typeAheadUserMpidList=[];

    let brokerDealers=[];
    if(UserMpid && UserMpid.length>0){
        UserMpidList=UserMpid.map((item,i)=>{
          typeAheadUserMpidList.push({value:item.executing_broker_mpid,label:item.executing_broker_mpid})
            return (<option value={item.executing_broker_mpid} id={item.id} key={i}>
              {item.executing_broker_mpid}
            </option>)
        })
    }
    if(AllocationData){

        if(AllocationData.broker_dealers&&AllocationData.broker_dealers.length>0)
        {
          brokerDealers=AllocationData.broker_dealers.map((item,i)=>{
              return (
                <div key={i} className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>

                      <label className="update-label">{item.name}</label>
                      <input type="text" id={item.broker_dealer_id} className="update-input BrokerCommission"  value={item.commission_percentage}  placeholder="commission"  onChange={(event)=>this.changeBrokerValue(item,event)}/>
                      <label style={{color:"red",fontSize:14}}></label>
                </div>
              )
          })
        }
      }
    let review="";
    let options=[];
    let checkPrice="";
      checkPrice=(
                    <div>


                        <div className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                              <label className="update-label" htmlFor="click_shares">Click IPO Shares</label>
                              <Field  name="click_shares" className="update-input" type="text"  component='input'/>
                              {/*<input type="text" className="update-input" ref={(Elmt)=>{this.amountOfShares=Elmt}} value={this.state.amountOfShares}  placeholder="" onChange={this.handleAllChanges.bind(this)}/>*/}
                              <label style={{color:"red",fontSize:14}} ref={(Elmt)=>{this.amoutOfShareErr=Elmt}}></label>
                          </div>
                          <div className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                            <label className="update-label">Underwriter MPID</label>
                            <Select
                                name="selected_bd_underwriter"
                                id="bd_underwriter_mpid_id"
                                value={this.state.selected_bd_underwriter_mpid_id}
                                ref={(select) => { this.selectUserMpId = select }}
                                onChange={this.mpIDhandleChange}
                                options={typeAheadUserMpidList}
                                isDisabled={true}
                              />

                          </div>
                          <div className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                              <label className="update-label" htmlFor="asked_shares">Asked Shares</label>
                              <Field name="asked_shares" className="update-input" type="text"  component='input'/>
                              {/*<input type="text" className="update-input" ref={(Elmt)=>{this.asked_shares=Elmt}}  value={this.state.asked_shares}   placeholder="" onChange={this.handleAllChanges.bind(this)}/>*/}

                          </div>

                      </div>)
      review =  (
                  <div>
                      <div className="row">
                          <div className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                            <label className="update-label" htmlFor="underwriter_concession">Underwriter concession</label>
                            <Field  name="underwriter_concession" className="update-input" type="text"  component='input'/>
                            {/*<input type="text" className="update-input" ref={(Elmt)=>{this.Underwriter=Elmt}} value={this.state.underwriter_concession} onChange={this.handleAllChanges.bind(this)} placeholder=""/>*/}
                          </div>

                            {
                              brokerDealers
                            }

                        </div>
                  </div>

               )

    let  typeAheadOfferingList=[];
    if(offerings && offerings.length>0){
        options=offerings.map((item) =>{
           typeAheadOfferingList.push({value:item.id,label:item.name})
          return(
          <option value={item.id} key={item.id}>
            {item.name}
          </option>
          )
        })

    }
    let submitButton=(
                      <input  type="submit" style={{ marginTop: '30px',background: "#8dc73f",
                      padding: "13px 28px",
                      color: "white"}}
                      value="Continue" />
                    )
    if(sendEmail)
    {
      submitButton=(
                    <div>
                        <label style={{color:"#E73536",fontSize:24}}>WARNING THE FINAL PRICE IS 20% HIGHER or LOWER THAN
                              THE PRICE RANGE! </label>
                        <input  type="button"  style={{ marginTop: '30px',background:"#CC2F3D",
                            padding: "13px 28px",
                            color: "white"}}
                            onClick={this.handleSendNotification}
                            value="Send reconfirmation emails" />
                      </div>
                    )
      }


    return (
      <form className="row" onSubmit={handleSubmit(this.handleContinue)}>
      <ToastContainer store={ToastStore}/>

        <div className="allocation_Detail_header">
          <div onTouchTap={() => this.goToBack()} >
            <div className="back_icon"></div>
          </div>
          <div className="Headingname">
            Allocation Detail
          </div>
        </div>


        <div style={{ padding: '0px 50px 0px 50px' }}>
          <div className="row">
                <div className="col-4 col-sm-12">
                  <label className="update-label">Offering name</label>

                  <Select
                      name="selected_offering"
                      id="selected_offeringId"
                      value={this.state.selected_offeringId}
                      ref={(select) => { this.selectOffering = select }}
                      onChange={this.handlefetchOffering}
                      options={typeAheadOfferingList}
                    />
                </div>
                {
                  DateTime
                }

            </div>
        <div style={{ padding: '20px 0px 0px 0px' }}>
          <div className="row" >
               {checkPrice }
          </div>
        </div>
            {
              review
            }
         </div>


            <div className="row">
              <div className="col-4 col-sm-12" style={{ marginBottom: '30px',float:"right" }}>
                {submitButton}
              </div>
            </div>

      </form>
    );
  }
}

UpdateAllocationForm.propTypes = {
  handleSubmit: React.PropTypes.func,
  handleContinue: React.PropTypes.func,
  submitting: React.PropTypes.bool,
  handleDateTimeSubmit: React.PropTypes.func,
  offerings: React.PropTypes.any,
  offering: React.PropTypes.any,
  dispatch: React.PropTypes.any,
  AllocationData : React.PropTypes.any,
};

const clickIPOCreateUpdateAllocationForm = reduxForm({
  form: 'clickIPOCreateUpdateAllocationForm',
  validate,
  // fields,
  destroyOnUnmount: false,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
  touchOnBlur: false,
})(UpdateAllocationForm);

const mapStateToProps = createStructuredSelector({
  initialValues: makeShowUpdatedAllocationData(),
  offerings: makeSelectOfferings(),
  offering: makeSelectOffering(),
  UserMpid:makeSelectUserMpid(),
  AllocationData:makeSelectUpdatedAllocationData(),
  UnderwriterCommssion:makeSelectUserMpidCommision(),

});

function mapDispatchToProps(dispatch) {
  return {
    fetchOffering:(id)=>dispatch(actions.fetchOffering(id)),
    sendEmailNotification: (id,final_price) => { dispatch(actions.sendNotificationEmailRequest(id, final_price)); },
    handleDateTimeSubmit: (data) => dispatch(actions.submitAllocationDateTime(data)),
    fetchUserMpid:()=>dispatch(actions.fetchUserMpid()),
    fetchAllocation:(id)=>dispatch(actions.fetchUpdatedAllocationData(id)),
    fetchCommission:(name)=>dispatch(actions.fetchUserMpidCommision(name)),
    postAllocation:(data)=>dispatch(actions.UpdateAllocation(data)),
    resetData:(data)=>dispatch(actions.fetchUpdatedAllocationDataSuccess(data)),

  };
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(clickIPOCreateUpdateAllocationForm));
