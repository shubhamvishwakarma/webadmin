export default function validate(values) {
  const errors = {};
  if (!values.get('id')) {
    errors.id = 'Please select offering';
  }
  if (!values.get('effective_date')) {
    errors.effective_date = 'Please enter data';
  }
  if (!values.get('allocation_mail_time')) {
    errors.allocation_mail_time = 'Please enter time';
  }
  return errors;
}
