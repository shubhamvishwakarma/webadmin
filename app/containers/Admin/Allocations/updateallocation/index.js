/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Switch, Route ,withRouter} from 'react-router-dom';

import AdminWrapper from 'components/AdminWrapper';
import UpdateAllocationForm from './updateallocationForm/updateallocationForm';


function AllocationDetail(props) {
  const { match, history } = props;

  return (
    <div>
        <div className="main_outer_wrapper">
            <Switch>
              <Route exact  path={`${match.url}`} component={UpdateAllocationForm} />
              {/*<Route exact path={`${match.url}/:id`} component={AllocationForm} />*/}
            </Switch>
          </div>
    </div>

  );
}

AllocationDetail.propTypes = {
  history: React.PropTypes.object,
  match: React.PropTypes.object,
};

// export default AllocationDetail;
export default withRouter(AllocationDetail)
