import React from 'react/lib/React';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { reduxForm, Field } from 'redux-form/immutable';
import { TimePicker, SelectField } from 'redux-form-material-ui';
import RaisedButton from 'material-ui/RaisedButton';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'redux-form-material-ui/lib/DatePicker';
import moment from 'moment';
import { mmddyy } from 'containers/App/Helpers';
import validate from './formValidation';
import * as actions from '../../actions';
import { makeSelectOfferings,makeSelectOffering,makeSelectUserMpid,makeSelectUpdatedAllocationData, makeSelectUserMpidCommision,makeSelectAllocationFormInitialValues } from '../../selectors';
import {ToastContainer, ToastStore} from 'react-toasts';
import { getOfferingPriceRangeNum,validatePrice,validateDate,getcreateAllocationPriceRangeNum } from 'containers/App/Helpers';
import { confirmAlert } from 'react-confirm-alert';
import './alert.css';
import Select from 'react-select';
import {Calendar} from 'primereact/components/calendar/Calendar.js';
 const fields = ['offering', 'final_shares', 'final_price','asked_shares','underwriter_concession_amount'];
 // eslint-disable-line react/prefer-stateless-function



class CreateAllocationForm extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props){
    super(props)


    let today = new Date();
    let month = today.getMonth();
    let year = today.getFullYear();
    let prevMonth = (month === 0) ? 11 : month - 1;
    let prevYear = (prevMonth === 11) ? year - 1 : year;

    let minDate = new Date();
    minDate.setMonth(prevMonth+1);
    minDate.setFullYear(prevYear);

    this.state={
      continueCount:0,
      sendEmail:false,
      selectedoffering_id:'',
      effective_date:'',
      effective_date_show: '',
      max_time:'',
      max_price:'',
      amountOfShare:'',
      UnderwriterValue:'',
      UnderwriterId:'',
      selected_bd_underwriter_mpid_id: '',
      bd_underwriter_mpid_id:'',
      isPrice:false,
      isEmailShow:false,
      isSubmit:false,
      offering_bd_brokerages_attributes:{},
      offering_bd_brokerages_attributes_submit:{},
      selected_offering:'',
      selected_offering_id:'',
      isBrokerDealer:false,
      minDate:minDate,
      isEffectiveChecked: false,
      asked_shares:''
    }
    this.handleChangesBD_attributes=this.handleChangesBD_attributes.bind(this);
    this.handleContinue=this.handleContinue.bind(this);
    this.handleSendNotification=this.handleSendNotification.bind(this);
    this.handlefetchUserMpid();


    this.submit=this.submit.bind(this);
    this.mpIDhandleChange = this.mpIDhandleChange.bind(this);
    this.offeringSelectChange=this.offeringSelectChange.bind(this);

    this.selectEffectiveDate=this.selectEffectiveDate.bind(this);
    this.selectTBD=this.selectTBD.bind(this);


  }
  mpIDhandleChange(event){
    this.setState({
      selected_bd_underwriter_mpid_id:event,
      bd_underwriter_mpid_id:event.value,
      isEmailShow:true
    })
  }

  offeringSelectChange(event){
    this.setState({
      selected_offering:event,
      selected_offering_id:event.value,
      isEmailShow:true,
      sendEmail:false,
      effective_date:this.effective_date,
      max_price:"",
      amountOfShare:'',
      isPrice:true,
      isBrokerDealer:true
    })
    //this.handleAllChanges(this);

    if(event.value){
       this.props.fetchOffering(event.value);
    }


  }

  handleChangesBD_attributes(editData,BD_info,i){

    let brokername=document.getElementById('BrokerDealerName'+i).value;

    let brokerCommision=document.getElementById('BrokerDealerCommission'+i).value;

    let TempBD_attribute=[];
    TempBD_attribute=this.state.offering_bd_brokerages_attributes;

    TempBD_attribute[i].broker_dealer_name=brokername;
    TempBD_attribute[i].bd_commission=brokerCommision;
    this.state.offering_bd_brokerages_attributes=TempBD_attribute;
    this.setState({
      offering_bd_brokerages_attributes_submit:TempBD_attribute
    })


    this.handleAllChanges();


  }



  submit = (callfunc) => {

      confirmAlert({
        title: 'Are you sure you want continue?',                        // Title dialog
        message: 'Please review data before submitting',               // Message dialog
        childrenElement: () => <div></div>,       // Custom UI or Component
        confirmLabel: 'Confirm',                           // Text button confirm
        cancelLabel: 'Cancel',                             // Text button cancel
        onConfirm: () =>callfunc(),    // Action after Confirm
        onCancel: () => {},      // Action after Cancel
      })
  };

  handlefetchUserMpid(){
    setTimeout(()=>{

      this.props.fetchUserMpid();
      this.setState({
      continueCount:0,
      sendEmail:false,
      selectedoffering_id:'',
      effective_date:'',
      max_time:'',
      max_price:'',
      amountOfShare:'',
      UnderwriterValue:'',
      UnderwriterId:'',
      selected_bd_underwriter_mpid_id: '',
      bd_underwriter_mpid_id:'',
      isPrice:false,
      isEmailShow:false,
      isSubmit:false,
      offering_bd_brokerages_attributes:{},
      offering_bd_brokerages_attributes_submit:{},
      selected_offering:'',
      selected_offering_id:'',
      isBrokerDealer:false,
      isLoad:true
    })
    },500)

  }

  componentWillReceiveProps(nextProps) {
              if(nextProps.offering){
                if(this.state.isLoad){
                  this.setState({isLoad: false})
                  if(nextProps.offering.underwriter_concession_amount!==null){
                    this.setState({
                      UnderwriterValue:nextProps.offering.underwriter_concession_amount,
                    })
                  }
                  if(nextProps.offering.final_price!==null || nextProps.offering.final_price<=0){
                    this.setState({
                      final_price:nextProps.offering.final_price
                    })
                  }

                //  this.setState({
                //     UnderwriterValue:nextProps.offering.underwriter_concession_amount,
                //     // UnderwriterId:nextProps.offering.bd_underwriter_mpid_id,
                //     final_price:nextProps.offering.final_price

                //   })
                  if ( nextProps.offering.bd_underwriter_mpid_id !==null ) {
                    const selectedMPIDObject = {
                      value: nextProps.offering.bd_underwriter_mpid_id,
                      label: nextProps.offering.bd_underwriter_mpid_id
                    }
                    this.setState({
                      selected_bd_underwriter_mpid_id: selectedMPIDObject,
                      bd_underwriter_mpid_id:nextProps.offering.bd_underwriter_mpid_id
                    });
                  }
                // if(nextProps.offering.offering_date_attributes!==null &&nextProps.offering.offering_date_attributes.effective_date!==null){
                //       if(nextProps.offering.offering_date_attributes&&nextProps.offering.offering_date_attributes.effective_date.substring(0,10) != this.state.effective_date) {
                //                 this.setState({
                //                   effective_date:nextProps.offering.offering_date_attributes.effective_date.substring(0,10),
                //                   max_time:nextProps.offering.offering_date_attributes.effective_date.substring(11,16),
                //                   max_price:(nextProps.offering.offering_price_attributes.max_price) ?  nextProps.offering.offering_price_attributes.max_price : '',
                //                   amountOfShare:nextProps.offering.totalPurchases
                //                 })
                //             }
                //       }else{
                //         this.setState({
                //           effective_date:this.effective_date.value,
                //           max_price:(nextProps.offering.offering_price_attributes && nextProps.offering.offering_price_attributes.max_price) ? nextProps.offering.offering_price_attributes.max_price : '',
                //           amountOfShare:nextProps.offering.totalPurchases
                //         })
                //       }
               }

            }

        }


   handleAllChanges(){
    this.setState({
      effective_date:this.state.effective_date,
      // final_price:this.final_price.value,
      // amountOfShare:this.amountOfShare.value,
      // UnderwriterValue:this.Underwriter.value,
      isEmailShow:true,
      selected_offering_id:this.state.selected_offering_id,
      offering_bd_brokerages_attributes:this.state.offering_bd_brokerages_attributes,
      selected_bd_underwriter_mpid_id:this.state.selected_bd_underwriter_mpid_id,
      bd_underwriter_mpid_id:this.state.bd_underwriter_mpid_id,
      // asked_shares: this.asked_shares.value
    })

  }
  handlePriceChanges(){
    this.setState({
        sendEmail:false,
        final_price:this.final_price.value,
        isPrice:false,
        isEmailShow:false
    })
    if(this.final_price.value !== ''){
        let priceRange=getcreateAllocationPriceRangeNum(this.props.offering.offering_price_attributes);
        let res=validatePrice(priceRange,parseInt(this.final_price.value))
            if(res){
               // this.props.fetchAllocation(this.state.selected_offering.value);
                this.setState({
                  final_price:this.final_price.value,
                  sendEmail:false,
                  continueCount:2,
                  isPrice:false,
                  isEmailShow:false
                })
            }
            else{
                this.setState({
                    sendEmail:true,
                    isEmailShow:true
                })
            }
     }
  }

  handleContinue(formData){
    let formDataValues = formData.toJS();
    validate(this.state);
    // e.preventDefault();

        this.setState({
          isSubmit:true
        })

      // if(!this.state.isEffectiveChecked){

          var errorVal=moment(this.state.effective_date,'MM/DD/YYYY').isValid();
          // var timeVal=moment(this.max_time.value,'hh:mm').isValid();
          (!errorVal)?this.effective_dateErr.textContent="Please Select Valid Date": this.effective_dateErr.textContent="";
          // (!timeVal)?this.timeValErr.textContent="Please Enter Valid Time":this.timeValErr.textContent="";
          var diff=moment(this.state.effective_date,'MM/DD/YYYY').diff(new Date(),"days")
          //console.log(diff(new Date(),"days"));
          if(diff>=0){
              if(errorVal){
                  this.setState({
                    continueCount:1,
                    effective_date:this.state.effective_date,
                  })
              }
          }
          else{
            this.effective_dateErr.textContent="Please Select Valid Date";
            return false
          }
      // }else{
      //   this.effective_dateErr.textContent="";
      // }


        if(this.state.offering_bd_brokerages_attributes.length>0){
            var offering_bd_brokerages_attributes=this.state.offering_bd_brokerages_attributes.map((item,i)=>{
              let brokername=document.getElementById('BrokerDealerName'+i).value;
              let brokerCommision=document.getElementById('BrokerDealerCommission'+i).value;
              return {"broker_dealer_name":brokername,"bd_commission_percentage":brokerCommision}
            })
        }else{
          var offering_bd_brokerages_attributes=[];
        }


        let FinalData={
          ext_id:this.state.selected_offering_id,
          final_price:formDataValues.final_price,
          click_shares:formDataValues.final_shares,
          effective_date : moment(this.state.effective_date).format('MM/DD/YYYY'),
          underwriter_mpid_id:this.state.bd_underwriter_mpid_id,
          underwriter_concession_amount:formDataValues.underwriter_concession_amount,
          offering_bd_brokerages_attributes,
          asked_shares:formDataValues.asked_shares
        }
        validate(this.state);
        const errors = validate(this.state);
        if(Object.keys(errors).length === 0){
            this.props.postAllocation(FinalData)
        }


  }

  handleSendNotification(id){
      this.setState({
        continueCount:2
      })
      this.submit(()=>{
        this.props.sendEmailNotification(this.state.selected_offering,this.state.final_price)
      })
  }


  handleUserMpid(){
    this.setState({
      UnderwriterValue:'',
      UnderwriterId:''
    })
    this.props.fetchCommission(this.selectUserMpId.value)
  }


    selectTBD(){
      if(this.state.isEffectiveChecked==false){
        this.setState({
          isEffectiveChecked: true,
          effective_date:'',
          effective_date_show:''

        });
      }else{
        this.setState({
          isEffectiveChecked: false,
        });
      }
      this.effective_dateErr.textContent="";
    }

    selectEffectiveDate(e){
        if(e.value){
          this.setState({
            effective_date: new Date(moment(e.value).format('MM/DD/YYYY')),
            isEffectiveChecked: false,
            effective_date_show:e.value
          })
        }else{
          this.setState({
            effective_date:'',
            effective_date_show: '',
            isEffectiveChecked: false,
          })
        }
        this.effective_dateErr.textContent="";
    }




  render() {
    const { handleSubmit, submitting, handleDateTimeSubmit, offerings,offering,UserMpid,UnderwriterCommssion,history} = this.props;
    const errors = validate(this.state);
    let {continueCount,sendEmail,isEmailShow,isBrokerDealer} =this.state;


    if(offering){

        if(this.state.isPrice==true && offering.final_price !== undefined){
        //  this.max_price.value=offering.final_price;
          this.state.max_price=offering.final_price;
          this.state.final_price=offering.final_price;
          // this.setState({
          //     sendEmail:false,
          //     max_price:offering.final_price,
          //     isPrice:false
          // })
         // this.handlePriceChanges();
        }

        if(offering.bd_data.length>0 && isBrokerDealer){
            this.state.offering_bd_brokerages_attributes=offering.bd_data;
          }else{
            this.state.offering_bd_brokerages_attributes={}
          }
        // console.log(this.state.offering_bd_brokerages_attributes);
    }


    //}
    let UserMpidList=[];
    let typeAheadUserMpidList=[];
    let brokerDealers="";
    if(UserMpid && UserMpid.length>0)
    {
      UserMpidList=UserMpid.map((item)=>{
         typeAheadUserMpidList.push({value:item.executing_broker_mpid,label:item.executing_broker_mpid})
          // if(offering){
          //   if(offering.bd_underwriter_mpid_id ==item.executing_broker_mpid){
          //         return (<option value={item.executing_broker_mpid} selected id={item.id} key={item.id}>
          //                     {item.executing_broker_mpid}
          //         </option>)
          //   }

          // }
          //   return (<option value={item.executing_broker_mpid} id={item.id} key={item.id}>
          //     {item.executing_broker_mpid}
          //   </option>)


        })
    }

    let typeAheadOfferingList=[];
    if(offerings && offerings.length>0){
       offerings.map((item)=>{
          typeAheadOfferingList.push({value:item.id,label:item.name})
            // return (<option value={item.id} id={item.id} key={item.id}>
            //   {item.name}
            // </option>)
        })
    }
    // let brokerDealers="";
    // if(offering.bd_data && offering.bd_data.length>0)
    // {
    //   brokerDealers=offering.bd_data.map((item,i)=>{
    //       return (
    //          //<div key={i} className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
    //         <div key={i} className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>

    //               <label className="update-label">{item.name}</label>
    //               <input type="text" id={item.broker_dealer_id} className="update-input BrokerCommission"  defaultValue={item.commission_percentage}  placeholder="commission"/>
    //               <label style={{color:"red",fontSize:14}}></label>
    //         </div>
    //       )
    //   })
    // }
    let review="";
    let options=[];
    let checkPrice="";
      checkPrice=(
                    <div>


                      </div>)
      review =  (
                  <div >
                      { /*<div className="row">
                          <div className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                                <label className="update-label">Click IPO Underwriter</label>
                                <input type="checkbox" className="update-input" style={{display:"block"}}ref={(Elmt)=>{this.FlagIPO=Elmt}} placeholder=""/>
                                <label style={{color:"red",fontSize:14}} ref={(Elmt)=>{this.max_priceErr=Elmt}}></label>
                          </div>

                        </div>*/}

                      <div className="row">
                          <div className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px'}}>
                              <label htmlFor="underwriter_concession_amount" className="update-label">Underwriter concession</label>
                               <Field  name="underwriter_concession_amount" className="update-input" type="text"  component='input'/>
                              {/*<input type="text" className="update-input" ref={(Elmt)=>{this.Underwriter=Elmt}} value={this.state.UnderwriterValue} onChange={this.handleAllChanges.bind(this)} placeholder=""/>*/}
                          </div>
                      </div>
                      {this.state.offering_bd_brokerages_attributes.length>0 && <div className="row">
                          {this.state.offering_bd_brokerages_attributes.map((res,i)=>
                          <div className="col-sm-12" key={i} style={{ padding: '0px 20px 0px 20px' ,marginTop:15}}>
                            <div className="row">
                              <div className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                                  <label className="update-label">Broker Dealer Name</label>
                                  <input type="text" id={`BrokerDealerName${i}`}  className="update-input " defaultValue={res.broker_dealer_name}  placeholder="" onKeyUp={(event)=>{this.handleChangesBD_attributes(event,res,i)}}  />
                              </div>
                              <div  className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                                <label className="update-label">Broker Dealer Commision</label>
                                <input type="text"  id={`BrokerDealerCommission${i}`}  className="update-input" defaultValue={res.bd_commission} onKeyUp={(event)=>{this.handleChangesBD_attributes(event,res,i)}}  placeholder=""/>
                              </div>
                            </div>
                          </div>)}
                      </div>}

                  </div>

               )
    if(offerings && offerings.length>0){
        options=offerings.map((item) =>{
          return(
          <option value={item.id} key={item.id}>
            {item.name}
          </option>
          )
        })

    }
    let submitButton=(
                      <input  type="submit" style={{ marginTop: '30px',background: "#8dc73f",
                      padding: "13px 28px",
                      color: "white"}}
                      value="Continue" />
                    )
    if(sendEmail && isEmailShow)
    {
      submitButton=(
                    <div>
                        <label style={{color:"#E73536",fontSize:24}}>WARNING THE FINAL PRICE IS 20% HIGHER or LOWER THAN
                              THE PRICE RANGE! </label>
                        <input  type="button"  style={{ marginTop: '30px',background:"#CC2F3D",
                            padding: "13px 28px",
                            color: "white"}}
                            onClick={this.handleSendNotification}
                            value="Send reconfirmation emails" />
                      </div>
                    )
      }


    return (
      <form className="row" onSubmit={handleSubmit(this.handleContinue)}>
      <ToastContainer store={ToastStore}/>

       <div className="allocation_Detail_header">
          <div onTouchTap={() => history.goBack()} >
            <div className="back_icon"></div>
          </div>
          <div className="Headingname" >
             Create new allocation
          </div>

        </div>

        <div style={{ padding: '0px 50px 0px 50px' }}>
          <div className="row">
                <div className="col-4 col-sm-12" >
                  <label className="update-label">Offering name</label>
                  <Select
                    name="selected_offering"
                    id="selectedoffering_id"
                    value={this.state.selected_offering}
                    ref={(select) => { this.selectOffering = select }}
                    onChange={this.offeringSelectChange}
                    options={typeAheadOfferingList}
                  />

                  {this.state.isSubmit && errors && <label style={{color:"red",fontSize:14}}>{errors.selectedoffering_id_Err}</label>}
                </div>
                <div className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <label className="update-label">Effective Date</label>
                    <Calendar minDate={this.state.minDate} value={this.state.effective_date_show} onChange={(e) => this.selectEffectiveDate(e)} />


                  {/*<input type="text" className="update-input" ref={(Elmt)=>{this.effective_date=Elmt}} value={this.state.effective_date}  placeholder="MM/DD/YYYY" onChange={this.handleAllChanges.bind(this)}/>*/}
                  <label style={{color:"red",fontSize:14}} ref={(Elmt)=>{this.effective_dateErr=Elmt}}></label>
                </div>

                <div className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                      <label htmlFor="final_price" className="update-label">Final Price</label>
                      <Field  name="final_price" className="update-input" type="text"  component='input'/>
                      {/*<input type="text" className="update-input" ref={(Elmt)=>{this.final_price=Elmt}}  value={this.state.final_price}  placeholder="" onChange={this.handlePriceChanges.bind(this)}/>*/}
                      <label style={{color:"red",fontSize:14}} ref={(Elmt)=>{this.max_priceErr=Elmt}}></label>
                </div>
                {/*<div className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <label className="update-label">Approximate Time</label>
                  <input type="text" className="update-input" ref={(Elmt)=>{this.max_time=Elmt}} value={this.state.max_time}  placeholder="HH:MM" onChange={this.handleAllChanges.bind(this)}/>
                  <label style={{color:"red",fontSize:14}} ref={(Elmt)=>{this.timeValErr=Elmt}}></label>

                </div>*/}

            </div>
            <div className="row">

                <div className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                      <label  htmlFor="final_shares"  className="update-label">Click IPO Shares</label>
                      <Field  name="final_shares" className="update-input" type="text"  component='input'/>
                     {/* <input type="text" className="update-input" ref={(Elmt)=>{this.amountOfShare=Elmt}}  value={this.state.amountOfShare}   placeholder="" onChange={this.handleAllChanges.bind(this)}/>*/}
                      <label style={{color:"red",fontSize:14}} ref={(Elmt)=>{this.amountOfShareErr=Elmt}}></label>
                </div>
                <div className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                  <label className="update-label">Underwriter MPID</label>
                  <Select
                      name="selected_bd_underwriter"
                      id="bd_underwriter_mpid_id"
                      value={this.state.selected_bd_underwriter_mpid_id}
                      ref={(select) => { this.bd_underwriter_mpid_id = select }}
                      onChange={(event)=>this.mpIDhandleChange(event)}
                      options={typeAheadUserMpidList}
                    />
                      {this.state.isSubmit && errors && <label style={{color:"red",fontSize:14}}>{errors.selected_bd_underwriter_mpid_Err}</label>}
                </div>
                <div className="col-4 col-sm-12" style={{ padding: '0px 20px 0px 20px' }}>
                      <label htmlFor="asked_shares" className="update-label">Asked Shares</label>

                     <Field  name="asked_shares" className="update-input" type="text"  component='input'/>
                      {/*<input type="text" className="update-input" ref={(Elmt)=>{this.asked_amount=Elmt}}  value={this.state.asked_amount}   placeholder="" onChange={this.handleAllChanges.bind(this)}/>*/}

                </div>
              </div>
            {
              review
            }
         </div>





            <div className="row">
              <div className="col-4 col-sm-12" style={{ marginBottom: '30px',float:"right" }}>
                {submitButton}
              </div>
            </div>

      </form>
    );
  }
}

CreateAllocationForm.propTypes = {
  handleSubmit: React.PropTypes.func,
  handleContinue: React.PropTypes.func,
  submitting: React.PropTypes.bool,
  handleDateTimeSubmit: React.PropTypes.func,
  offerings: React.PropTypes.any,
};

const clickIPOCreateCreateAllocationForm = reduxForm({
  form: 'clickIPOCreateCreateAllocationForm',
  validate,
  fields,
  destroyOnUnmount: false,
  enableReinitialize: true,
  keepDirtyOnReinitialize: true,
  touchOnBlur: false,

})(CreateAllocationForm);

const mapStateToProps = createStructuredSelector({
  offerings: makeSelectOfferings(),
  offering: makeSelectOffering(),
  UserMpid:makeSelectUserMpid(),
  UnderwriterCommssion:makeSelectUserMpidCommision(),
  initialValues: makeSelectAllocationFormInitialValues()
});

function mapDispatchToProps(dispatch) {
  return {
    fetchOffering:(id)=>dispatch(actions.fetchOffering(id)),
    sendEmailNotification: (id,final_price) => { dispatch(actions.sendNotificationEmailRequest(id, final_price)); },
    handleDateTimeSubmit: (data) => dispatch(actions.submitAllocationDateTime(data)),
    fetchUserMpid:()=>dispatch(actions.fetchUserMpid()),
    fetchAllocation:(id)=>dispatch(actions.fetchUpdatedAllocationData(id)),
    fetchCommission:(name)=>dispatch(actions.fetchUserMpidCommision(name)),
    postAllocation:(data)=>dispatch(actions.postAllocation(data))

  };
}

export default connect(mapStateToProps, mapDispatchToProps)(clickIPOCreateCreateAllocationForm);
