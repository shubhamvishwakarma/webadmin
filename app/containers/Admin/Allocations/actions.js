import {
  FETCH_ALLOCATION_LIST,
  FETCH_ALLOCATION_LIST_SUCCESS,

  SUBMIT_ALLOCATION_DATE_TIME,
  SUBMIT_DATE_TIME_SUCCESS,
  SUBMIT_DATE_TIME_FAILED,
  FETCH_UPDATED_ALLOCATION_DATA,
  FETCH_UPDATED_ALLOCATION_DATA_SUCCESS,
  FETCH_OFFERING_LIST,
  FETCH_OFFERING_LIST_SUCCESS,
  SUBMIT_CREATE_ALLOCATION_REQUEST,
  SUBMIT_CREATE_ALLOCATION_SUCCESS,
  SEND_NOTIFICATION_EMAIL,
  SEND_NOTIFICATION_EMAIL_SUCCESS,
  FETCH_OFFERING,
  FETCH_OFFERING_SUCCESS,
  FETCH_USER_MPID,
  FETCH_USER_MPID_SUCCESS,
  FETCH_USER_MPID_COMMISION,
  FETCH_USER_MPID_COMMISION_SUCCESS,
  SUBMIT_ALLOCATION,
  SUBMIT_ALLOCATION_SUCCESS,
  FETCH_SHOW_ALLOCATION_DATA,
  FETCH_SHOW_ALLOCATION_DATA_SUCCESS,
  FETCH_RUN_ALLOCATION_DATA,
  SAVE_ALLOCATION,
  SAVE_ALLOCATION_DATA_SUCCESS,
  COMPLETE_ALLOCATION,
  COMPLETE_ALLOCATION_DATA_SUCCESS,
  RUN_ALLOCATION_ERROR,
  FINAL_PROSPECTUS_ALLOCATION,
  FINAL_PROSPECTUS_SUCCESS,
  SIXTYMINUTEEMAIL,
  SIXTYMINUTEEMAIL_SUCCESS,
  ALLOCATION_EMAIL,
  ALLOCATION_EMAIL_SUCCESS,
  UPDATE_ALLOCATION,
  CALL_INTACT_FILE_UPLOAD,
  SUCCESSFULL_FILE_UPLOAD,

} from './constants';

export function callIntactUpload(ext_id){
    return{
        type:CALL_INTACT_FILE_UPLOAD,
        ext_id
    }
}
export function sucessFullFileUpload(){
    return{
        type:SUCCESSFULL_FILE_UPLOAD
    }
}
export function UpdateAllocation(data){
  return {
    type:UPDATE_ALLOCATION,
    data
  }
}

export function postAllocation(data){
  return {
    type:SUBMIT_ALLOCATION,
    data
  }
}
export function fetchUserMpidCommision(name) {
  return {
    type: FETCH_USER_MPID_COMMISION,
    name
  };
}
export function fetchUserMpidCommisionSuccess(data) {
  return {
    type: FETCH_USER_MPID_COMMISION_SUCCESS,
    data
  };
}
export function fetchUserMpid() {
  return {
    type: FETCH_USER_MPID,

  };
}

export function fetchUserMpidSuccess(data) {
  return {
    type: FETCH_USER_MPID_SUCCESS,
    data,
  };
}
export function fetchOffering(id) {
  return {
    type: FETCH_OFFERING,
    id
  };
}

export function fetchOfferingSuccess(data) {
  return {
    type: FETCH_OFFERING_SUCCESS,
    data,
  };
}

export function fetchAllocationList() {
  return {
    type: FETCH_ALLOCATION_LIST,
  };
}

export function fetchAllocationListSuccess(data) {
  return {
    type: FETCH_ALLOCATION_LIST_SUCCESS,
    data,
  };
}


// detail actions
export function submitAllocationDateTime(data) {
  return {
    type: SUBMIT_ALLOCATION_DATE_TIME,
    data,
  };
}

export function submitDateTimeSuccess(data) {
  return {
    type: SUBMIT_DATE_TIME_SUCCESS,
    data,
  };
}

export function submitDateTimeFailed() {
  return {
    type: SUBMIT_DATE_TIME_FAILED,
  };
}

export function fetchUpdatedAllocationData(id) {
  return {
    type: FETCH_UPDATED_ALLOCATION_DATA,
    id,
  };
}

export function fetchUpdatedAllocationDataSuccess(data) {
  return {
    type: FETCH_UPDATED_ALLOCATION_DATA_SUCCESS,
    data,
  };
}

export function fetchOfferingList() {
  return {
    type: FETCH_OFFERING_LIST,
  };
}

export function fetchOfferingListSuccess(data) {
  return {
    type: FETCH_OFFERING_LIST_SUCCESS,
    data,
  };
}

export function submitCreateAllocationRequest(data) {
  return {
    type: SUBMIT_CREATE_ALLOCATION_REQUEST,
    data,
  };
}

export function submitCreateAllocationSuccess() {
  return {
    type: SUBMIT_CREATE_ALLOCATION_SUCCESS,
  };
}

export function sendNotificationEmailRequest(id, final_price) {
  return {
    type: SEND_NOTIFICATION_EMAIL,
    id,
    final_price,
  };
}

export function sendNotificationEmailSuccess() {
  return {
    type: SEND_NOTIFICATION_EMAIL_SUCCESS,
  };
}


export function fetchShowAllocation(id) {
  return {
    type: FETCH_SHOW_ALLOCATION_DATA,
    id,
  };
}

export function fetchShowAllocationSuccess(data) {
  return {
    type: FETCH_SHOW_ALLOCATION_DATA_SUCCESS,
    data,
  };
}

export function fetchRunAllocation(id){
  return {
    type: FETCH_RUN_ALLOCATION_DATA,
    id,
  };
}
export function RunAllocationError(data) {
  return {
    type: RUN_ALLOCATION_ERROR,
    data,
  };
}

export function fetchSaveAllocation(data) {
  return {
    type: SAVE_ALLOCATION,
    data,
  };
}
export function fetchSaveAllocationSuccess(data) {
  return {
    type: SAVE_ALLOCATION_DATA_SUCCESS,
    data,
  };
}

export function fetchCompleteAllocation(id){
  return {
    type: COMPLETE_ALLOCATION,
    id,
  };
}


export function fetchCompleteAllocationSuccess(data) {
  return {
    type: COMPLETE_ALLOCATION_DATA_SUCCESS,
    data,
  };
}

export function sendProspectusAllocation(ext_id, prospectus_url) {
  return {
    type: FINAL_PROSPECTUS_ALLOCATION,
    ext_id,
    prospectus_url
  };
}
export function makeFinalProspectSuccess(data) {
  return {
    type: FINAL_PROSPECTUS_SUCCESS,
    data,
  };
}

export function sixtyMinuteEmailAllocation(ext_id) {
  return {
    type: SIXTYMINUTEEMAIL,
    ext_id
  };
}
export function makeSixtyMinuteSuccess(data) {
  return {
    type: SIXTYMINUTEEMAIL_SUCCESS,
    data,
  };
}


export function AllocationEmail(ext_id) {
  return {
    type: ALLOCATION_EMAIL,
    ext_id
  };
}
export function makeAllocationEmailSuccess(data) {
  return {
    type: ALLOCATION_EMAIL_SUCCESS,
    data,
  };
}





