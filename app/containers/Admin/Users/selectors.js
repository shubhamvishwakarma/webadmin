import { createSelector } from 'reselect';

const selectUsers = (state) => state.get('adminUsers');

const makeSelectBroderDealers = () => createSelector(
  selectUsers,
  (adminState) => {
    const list = adminState.get('brokerDealerList');
    if (list) {
      list.map((item) => {
        if (item.blocker_code === '77') {
          item.blocker_code = 'Full Allocation'; // eslint-disable-line
        } else if (item.blocker_code === '74') {
          item.blocker_code = 'X-Clearing'; // eslint-disable-line
        }
        return item;
      });
    }
    return list;
  }
);


export {
  selectUsers,
  makeSelectBroderDealers,
};
