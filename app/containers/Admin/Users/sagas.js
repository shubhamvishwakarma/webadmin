

import { call, put, all, takeLatest, select } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { BASE_URL } from 'containers/App/constants';
import { showGlobalLoader, closeGlobalLoader } from 'containers/App/actions';
import request from 'utils/request';
import * as actions from './actions';
import * as actionTypes from './constants';
import Cookies from 'universal-cookie';
import { LogStaging } from '../../App/Helpers';

const cookies = new Cookies();
const getToken = (state) => state.getIn(['global', 'currentUser', 'token']);



function* handleFetchBrokerDealerList() {
  LogStaging(4)
  const TOKEN = yield select(getToken);
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const URL = `${BASE_URL}/broker_dealers/get_broker_dealer`;
  let userData;
  try {
    userData = yield call(request, URL, {
      method: 'GET',
      headers: ajaxRequestHeaders,
    });
    if (userData) {
      yield put(actions.fetchBrokerDealersListSuccess(userData));
    }
  } catch (err) {
    console.log(err);
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
  } finally {
    yield put(closeGlobalLoader());
  }
}
function* watcherFetchBrokerDealerList() {
  yield takeLatest(actionTypes.FETCH_BROKERDEALERS_LIST, handleFetchBrokerDealerList);
}

function* handleCreateBrokerDealer(action) {
  LogStaging(5)
  const TOKEN = yield select(getToken);
  let ajaxRequestHeaders = new Headers({
    'Content-Type': 'application/json',
    Accept: 'application/json',
  });
  yield put(showGlobalLoader());
  ajaxRequestHeaders.append('Authorization', `${TOKEN}`);
  const actionData = action.data.toJS();
  if (actionData.logo) {
    actionData.logo = {
      lastModified: actionData.logo[0].lastModified,
      lastModifiedDate: actionData.logo[0].lastModifiedDate,
      name: actionData.logo[0].name,
      size: actionData.logo[0].size,
      type: actionData.logo[0].type,
      webkitRelativePath: actionData.logo[0].webkitRelativePath,
    };
  }
  const BODY = JSON.stringify({
    ...actionData,
  });
  const URL = `${BASE_URL}/broker_dealers/create_broker_dealer`;
  try {
    yield call(request, URL, {
      method: 'POST',
      headers: ajaxRequestHeaders,
      body: BODY,
    });
    yield put(actions.createBrokerDealerSuccess());
    yield put(actions.fetchBrokerDealersList());
    yield put(push('/users'));
  } catch (err) {
    console.log(err);
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
        window.location.href = '/login';
    }
    yield put(actions.createBrokerDealerFailed());
  } finally {
    yield put(closeGlobalLoader());
  }
}
function* watcherCreateBrokerDealer() {
  yield takeLatest(actionTypes.SUBMIT_CREATE_BROKER_DEALER_FORM, handleCreateBrokerDealer);
}


// Bootstrap sagas
export default function* allocationSagas() {
  yield all([
    watcherFetchBrokerDealerList(),
    watcherCreateBrokerDealer(),
  ]);
}
