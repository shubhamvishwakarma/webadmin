import {
  FETCH_BROKERDEALERS_LIST,
  FETCH_BROKERDEALERS_LIST_SUCCESS,
  SUBMIT_CREATE_BROKER_DEALER_FORM,
  CREATE_BROKER_DEALER_SUCCESS,
  CREATE_BROKER_DEALER_FAILED,
} from './constants';


export function fetchBrokerDealersList() {
  return {
    type: FETCH_BROKERDEALERS_LIST,
  };
}

export function fetchBrokerDealersListSuccess(data) {
  return {
    type: FETCH_BROKERDEALERS_LIST_SUCCESS,
    data,
  };
}

export function submitCreateBrokerDealerForm(data) {
  return {
    type: SUBMIT_CREATE_BROKER_DEALER_FORM,
    data,
  };
}

export function createBrokerDealerSuccess() {
  return {
    type: CREATE_BROKER_DEALER_SUCCESS,
  };
}

export function createBrokerDealerFailed() {
  return {
    type: CREATE_BROKER_DEALER_FAILED,
  };
}
