/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Link } from 'react-router-dom';

import { makeSelectCurrentUser } from 'containers/App/selectors';
import RaisedButton from 'material-ui/RaisedButton';
import AdminWrapper from 'components/AdminWrapper';
import { makeSelectBroderDealers } from '../selectors';

import {DataTable} from 'primereact/components/datatable/DataTable';
import {Column} from 'primereact/components/column/Column';
import {Button} from 'primereact/components/button/Button';
import {InputText} from 'primereact/components/inputtext/InputText';
import {ColumnGroup} from 'primereact/components/columngroup/ColumnGroup';
import {Row} from 'primereact/components/row/Row';

class UsersList extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  constructor(props) {
    super(props);
    this.state = {
      sort: {
        type: null,
        field: null,
      },
    };
  }

  render() {
    const { brokerdealers } = this.props;
    const Header = (<div className="offer_head">Users</div>);
    return (

      <AdminWrapper title="Admin" header={Header} >
        <div className="info">
          <div className="tabs">
            <input type="radio" name="tabs" id="tab-broker-dealers" defaultChecked="checked" />
            <label htmlFor="tab-broker-dealers" className="tab-left-label">BROKER DEALERS</label>
            <div className="tab">
              <div className="row">
                <div className="col-10" style={{paddingLeft:30,paddingRight:10,paddingTop:15}}>

                  <DataTable value={brokerdealers} editable={false}  responsive={true}   scrollable={true}  scrollHeight="90rem" resizableColumns={true}  columnResizeMode="expand">
                      <Column className="dataTableNameColumn" field="name" header="Name"  sortable={true} />
                      <Column className="tableCols"   field="clearing_account_id" header="ID"  sortable={true} />
                      <Column  className="tableCols"   field="mpid" header="MPID"  sortable={true} />
                      <Column className="tableCols" field="blocker_code" header="Type"  sortable={true} />
                      <Column className="tableCols"  field="commission_percentage" header="Commission" sortable={true} />
                  </DataTable>
                </div>
                <div className="col-2">
                  <Link to={'/users/create_broker_dealer'}>
                    <RaisedButton label="New Broker Dealer" backgroundColor="#8DC73F" labelColor="#fff" style={{ marginTop: '30px' }} />
                  </Link>
                </div>
              </div>
            </div>
            <input type="radio" name="tabs" id="tab-underwriters" />
            <label htmlFor="tab-underwriters" className="tab-label">UNDERWRITERS</label>
            <div className="tab">
              <div className="row">
                <div className="col-10">
                  {/* <UsersList orders={[]} /> */}
                </div>
                <div className="col-2">
                  <RaisedButton label="New Users" backgroundColor="#8DC73F" labelColor="#fff" style={{ marginTop: '30px' }} />
                </div>
              </div>
            </div>
            <input type="radio" name="tabs" id="tab-investors" />
            <label htmlFor="tab-investors" className="tab-label">INVESTORS</label>
            <div className="tab">
              <div className="row">
                <div className="col-10">
                  {/* <UsersList orders={[]} /> */}
                </div>
                <div className="col-2">
                  <RaisedButton label="New Users" backgroundColor="#8DC73F" labelColor="#fff" style={{ marginTop: '30px' }} />
                </div>
              </div>
            </div>
          </div>
        </div>
      </AdminWrapper>

    );
  }
}

UsersList.propTypes = {
  brokerdealers: PropTypes.any,
};

const mapStateToProps = createStructuredSelector({
  currentUser: makeSelectCurrentUser(),
  brokerdealers: makeSelectBroderDealers(),
});


export default connect(mapStateToProps, null)(UsersList);
