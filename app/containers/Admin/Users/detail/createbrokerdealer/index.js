/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { connect } from 'react-redux';
import AdminWrapper from 'components/AdminWrapper';
import { submitCreateBrokerDealerForm } from '../../actions';
import CreateBrokerDealerForm from './forms';

function CreateBrokerDealers(props) {
  const { goBack, handleFormSubmit } = props;
  const Header = (
    <div>
      <div className="offer_detail_head">
        <div onTouchTap={goBack} >
            Create new user
          </div>
      </div>
      <div onTouchTap={goBack} >
        <div className="back_icon"></div>
      </div>
    </div>
    );
  return (
    <AdminWrapper title="Admin" header={Header} >
      <div className="info">
        <CreateBrokerDealerForm handleFormSubmit={handleFormSubmit} initialValues={{ blocker_code: '74' }} />
      </div>
    </AdminWrapper>
  );
}

CreateBrokerDealers.propTypes = {
  goBack: React.PropTypes.func,
  handleFormSubmit: React.PropTypes.func,
};


function mapDispatchToProps(dispatch, ownProps) {
  return {
    goBack: () => { ownProps.history.goBack(); },
    handleFormSubmit: (data) => dispatch(submitCreateBrokerDealerForm(data)),
  };
}

export default connect(null, mapDispatchToProps)(CreateBrokerDealers);
