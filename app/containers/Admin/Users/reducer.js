/*
 * Investor Reducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import {
  FETCH_BROKERDEALERS_LIST_SUCCESS,
  CREATE_BROKER_DEALER_SUCCESS,
} from './constants';

const initialState = fromJS({
  brokerDealerList: null,
});


function adminUsersReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_BROKERDEALERS_LIST_SUCCESS:
      return state.set('brokerDealerList', action.data);
    case CREATE_BROKER_DEALER_SUCCESS:
      return state.set('brokerDealerList', null);
    default:
      return state;
  }
}


export default adminUsersReducer;
