import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';
import { Card, CardText, CardTitle } from 'material-ui/Card';
// import { reset, change } from 'redux-form/immutable';

import injectSaga from 'utils/injectSaga';
import ViewWrapper from 'components/ViewWrapper';
import CenteredContent from 'components/CenteredContent';
import Padding from 'components/PaddingWrapper';
import logo from 'images/logo-4.png';
import { makeSelectPasswordRecoveryMessage } from 'containers/App/selectors';
import RecoverPasswordForm from './form';
import { submitRecoveryForm } from './actions';
import saga from './sagas';

class PasswordRecovery extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  render() {
    const { handleFormSubmit, resetMessage } = this.props;
    return (
      <article>
        <Helmet
          title="ClickIPO Admin"
        />
        <ViewWrapper padding="10px 20px 80px" className="login">
          <div className="row">
            <CenteredContent className="col-5 col-m-10 col-sm-12 small-centered">
              <Padding value="40px">
                <img src={logo} alt="" height="62" width="200" />
              </Padding>
            </CenteredContent>
          </div>
          <div className="row">
            <div className="col-5 col-m-10 col-sm-12 small-centered">
              <Card>
                <CardTitle
                  style={{ paddingBottom: '0px' }}
                  title="Reset Password"
                  titleStyle={{ textAlign: 'center', fontSize: '34px', marginTop: '20px', marginBottom: '20px' }}
                >
                  <CenteredContent> {resetMessage} </CenteredContent>
                </CardTitle>
                <CardText style={{ paddingTop: '0px' }}>
                  <RecoverPasswordForm handleFormSubmit={handleFormSubmit} />
                </CardText>
              </Card>
            </div>
          </div>
          <div className="row">
            <CenteredContent className="col-5 col-m-10 col-sm-12 small-centered">
              <Padding value="25px">
                <a href="http://clickipo.com" className="visit-main">
                visit main site
              </a>
              </Padding>
            </CenteredContent>
          </div>
        </ViewWrapper>
      </article>
    );
  }
}

PasswordRecovery.propTypes = {
  handleFormSubmit: PropTypes.func,
  resetMessage: PropTypes.string,
};

function mapDispatchToProps(dispatch) {
  return {
    handleFormSubmit: (data) => dispatch(submitRecoveryForm(data)),
  };
}

const mapStateToProps = createStructuredSelector({
  resetMessage: makeSelectPasswordRecoveryMessage(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withSaga = injectSaga({ key: 'passwordRecovery', saga });

export default compose(
  withSaga,
  withConnect,
)(PasswordRecovery);
