/**
 * API calls for Ola
 */

import { call, put, takeLatest, all } from 'redux-saga/effects';
import { BASE_URL } from 'containers/App/constants';
import request from 'utils/request';
import { submitRecoveryFormSuccess, submitRecoveryFormFailure } from './actions';
import { SUBMIT_RECOVERY_FORM } from './constants';
import { LogStaging } from '../App/Helpers';

const ajaxRequestHeaders = new Headers({
  'Content-Type': 'application/json',
  Accept: 'application/json',
});

function* handleSubmitRecoveryForm(action) {
  LogStaging(26)
  const URL = `${BASE_URL}/auth/password`;
  let response;
  const BODY = JSON.stringify({
    ...action.data.toJS(),
  });
  try {
    response = yield call(request, URL, {
      method: 'POST',
      headers: ajaxRequestHeaders,
      body: BODY,
    });
    if (response === null) {
      yield put(submitRecoveryFormSuccess('Password reset instructions have been sent to the email provided'));
    } else {
      yield put(submitRecoveryFormFailure(response.error));
    }
  } catch (err) {
    console.log(err);
    console.log(err.response);
  }
}
function* watcherSubmitRecoveryForm() {
  yield takeLatest(SUBMIT_RECOVERY_FORM, handleSubmitRecoveryForm);
}

// Bootstrap sagas
export default function* passwordRecoverySaga() {
  yield all([
    watcherSubmitRecoveryForm(),
  ]);
}
