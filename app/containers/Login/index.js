import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Card, CardText, CardTitle } from 'material-ui/Card';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import ViewWrapper from 'components/ViewWrapper';
import CenteredContent from 'components/CenteredContent';
import Padding from 'components/PaddingWrapper';
import logo from 'images/logo-4.png';
import { makeSelectLoginError } from 'containers/App/selectors';
import LoginForm from './form';
import { submitLoginForm } from './actions';
import saga from './sagas';

class Login extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  render() {
    const { handleFormSubmit, loginErrorMessage } = this.props;
    return (
      <article>
        <Helmet
          title="ClickIPO Admin"
        />
        <ViewWrapper padding="10px 20px 80px" className="login">
          <div className="row">
            <CenteredContent className="col-5 col-m-10 col-sm-12 small-centered">
              <Padding value="40px">
                <img src={logo} alt="" height="62" width="200" />
              </Padding>
            </CenteredContent>
          </div>
          <div className="row">
            <div className="col-5 col-m-10 col-sm-12 small-centered">
              <Card>
                <CardTitle
                  style={{ paddingBottom: '0px' }}
                  title="Log In"
                  titleStyle={{ textAlign: 'center', fontSize: '34px', marginTop: '20px', marginBottom: '20px' }}
                >
                  <CenteredContent> {loginErrorMessage} </CenteredContent>
                </CardTitle>
                <CardText style={{ paddingTop: '0px' }}>
                  <LoginForm handleFormSubmit={handleFormSubmit} />
                </CardText>
              </Card>
            </div>
          </div>
          <div className="row">
            <CenteredContent className="col-5 col-m-10 col-sm-12 small-centered">
              <Padding value="25px">
                <a href="http://clickipo.com" className="visit-main">
                visit main site
              </a>
              </Padding>
            </CenteredContent>
          </div>
        </ViewWrapper>
      </article>
    );
  }
}

Login.propTypes = {
  handleFormSubmit: PropTypes.func,
  loginErrorMessage: PropTypes.string,
};

function mapDispatchToProps(dispatch) {
  return {
    handleFormSubmit: (data) => dispatch(submitLoginForm(data)),
  };
}

const mapStateToProps = createStructuredSelector({
  loginErrorMessage: makeSelectLoginError(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withSaga = injectSaga({ key: 'login', saga });

export default compose(
  withSaga,
  withConnect,
)(Login);
