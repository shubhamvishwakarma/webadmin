/**
 * API calls for Ola
 */

import { call, put, takeLatest, all } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { BASE_URL } from 'containers/App/constants';
import request from 'utils/request';
import { submitLoginFormSuccess, submitLoginFormFailure } from './actions';
import { SUBMIT_LOGIN_FORM } from './constants';
import {sha3_512} from 'js-sha3';
import { ToastStore } from 'react-toasts';
import Cookies from 'universal-cookie';
const ajaxRequestHeaders = new Headers({
  'Content-Type': 'application/json',
  Accept: 'application/json',
});
const cookies = new Cookies();

function* handleSubmitLoginForm(action) {
  console.log(2)
  let data=action.data.toJS();
  data.encrypted_password=sha3_512(data.password);
  const URL = `${BASE_URL}/auth/login`;
  //console.log(URL);
  let validateLogin;
  const BODY = JSON.stringify({
    admin: 'true',
    email:data.email,
    encrypted_password:data.encrypted_password
  });
  //console.log(BODY)
  try {
    validateLogin = yield call(request, URL, {
      method: 'POST',
      headers: ajaxRequestHeaders,
      body: BODY,
    });
    if (validateLogin.token) {
      // localStorage.setItem('currentUser', validateLogin.token);
      // localStorage.setItem('email', action.data.toJS().email);
      // localStorage.setItem('role', validateLogin.role_name);

      cookies.set('currentUser', validateLogin.token, { path: '/' });
      cookies.set('email', action.data.toJS().email, { path: '/' });
      cookies.set('role', validateLogin.role_name, { path: '/' });


      yield put(submitLoginFormSuccess(validateLogin));
      if (validateLogin.role_name === 'broker_dealer') {
        yield put(push('/offerings'));
      } else {
        yield put(push('/users'));
      }
    } else {
      yield put(submitLoginFormFailure(validateLogin.error));
    }
  } catch (err) {
    ToastStore.error("Invalid Login")
    console.log(err);
  }
}
function* watcherSubmitLoginForm() {
  yield takeLatest(SUBMIT_LOGIN_FORM, handleSubmitLoginForm);
}

// Bootstrap sagas
export default function* loginSaga() {
  yield all([
    watcherSubmitLoginForm(),
  ]);
}
