/**
 * API calls for Ola
 */

import { call, put, takeLatest, select, all } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { BASE_URL } from 'containers/App/constants';
import request from 'utils/request';
import { submitResetFormFailure, submitResetFormSuccess } from './actions';
import { SUBMIT_RESET_FORM } from './constants';
import Cookies from 'universal-cookie';
import { LogStaging } from '../App/Helpers';

const cookies = new Cookies();
const getToken = (state) => state.getIn(['global', 'passwordResetToken']);
const ajaxRequestHeaders = new Headers({
  'Content-Type': 'application/json',
  Accept: 'application/json',
});

function* handleSubmitRecoveryForm(action) {
  LogStaging(3)
  const URL = `${BASE_URL}/auth/password`;
  const TOKEN = yield select(getToken);
  let response;
  const BODY = JSON.stringify({
    reset_password_token: TOKEN,
    ...action.data.toJS(),
  });
  try {
    response = yield call(request, URL, {
      method: 'PUT',
      headers: ajaxRequestHeaders,
      body: BODY,
    });
    if (response) {
      yield put(submitResetFormSuccess());
      yield put(push('/login'));
    } else {
      yield put(submitResetFormFailure(response.error));
    }
  } catch (err) {
    console.log(err);
    if(err.response.status==401){
        cookies.remove('currentUser');
        cookies.remove('email');
        cookies.remove('role');
      window.location.href = '/login';
    }
  }
}
function* watcherSubmitRecoveryForm() {
  yield takeLatest(SUBMIT_RESET_FORM, handleSubmitRecoveryForm);
}

// Bootstrap sagas
export default function* resetPasswordSaga() {
  yield all([
    watcherSubmitRecoveryForm(),
  ]);
}
