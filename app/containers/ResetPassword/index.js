import React from 'react/lib/React';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { Card, CardText, CardTitle } from 'material-ui/Card';
import { compose } from 'redux';

import injectSaga from 'utils/injectSaga';
import ViewWrapper from 'components/ViewWrapper';
import CenteredContent from 'components/CenteredContent';
import Padding from 'components/PaddingWrapper';
import { makeSelectPasswordResetMessage } from 'containers/App/selectors';
import logo from 'images/logo-4.png';
import ResetPasswordForm from './form';
import { submitResetForm, setResetToken } from './actions';
import saga from './sagas';

class ResetPassword extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  componentDidMount() {
    this.props.resetPasswordToken(this.props.location.query.reset_password_token);
  }

  render() {
    const { handleFormSubmit, resetError } = this.props;
    return (
      <article>
        <Helmet
          title="ClickIPO Admin"
        />
        <ViewWrapper padding="10px 20px 80px" className="login">
          <div className="row">
            <CenteredContent className="col-5 col-m-10 col-sm-12 small-centered">
              <Padding value="40px">
                <img src={logo} alt="" height="62" width="200" />
              </Padding>
            </CenteredContent>
          </div>
          <div className="row">
            <div className="col-5 col-m-10 col-sm-12 small-centered">
              <Card>
                <CardTitle
                  style={{ paddingBottom: '0px' }}
                  title="Reset Password"
                  titleStyle={{ textAlign: 'center', fontSize: '34px', marginTop: '20px', marginBottom: '20px' }}
                >
                  <CenteredContent> { resetError } </CenteredContent>
                </CardTitle>
                <CardText style={{ paddingTop: '0px' }}>
                  <ResetPasswordForm handleFormSubmit={handleFormSubmit} />
                </CardText>
              </Card>
            </div>
          </div>
          <div className="row">
            <CenteredContent className="col-5 col-m-10 col-sm-12 small-centered">
              <Padding value="25px">
                <a href="http://clickipo.com" className="visit-main">
                visit main site
              </a>
              </Padding>
            </CenteredContent>
          </div>
        </ViewWrapper>
      </article>
    );
  }
}

ResetPassword.propTypes = {
  handleFormSubmit: React.PropTypes.func,
  resetPasswordToken: React.PropTypes.func,
  location: React.PropTypes.object,
  resetError: React.PropTypes.string,
};

function mapDispatchToProps(dispatch) {
  return {
    handleFormSubmit: (data) => dispatch(submitResetForm(data)),
    resetPasswordToken: (token) => dispatch(setResetToken(token)),
  };
}

const mapStateToProps = createStructuredSelector({
  resetError: makeSelectPasswordResetMessage(),
});

const withConnect = connect(mapStateToProps, mapDispatchToProps);

const withSaga = injectSaga({ key: 'resetPassword', saga });

export default compose(
  withSaga,
  withConnect,
)(ResetPassword);
