import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

import LoaderSpinner from 'containers/App/GlobalLoader';
import Header from 'components/Header';

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: '#CACACA',
    accent1Color: '#E18156',
  },
  tabs: {
    backgroundColor: '#00BB6E',
    textColor: '#008A4F',
    selectedTextColor: '#FFFFFF',
  },
});

export default class Wrapper extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { children, login } = this.props;

    if (login) {
      return (
        <div>
          <MuiThemeProvider muiTheme={muiTheme}>
            {children}
          </MuiThemeProvider>
        </div>
      );
    }
    return (
      <div className="full-height">
        <MuiThemeProvider muiTheme={muiTheme}>
          <div className="full-height">
            <Helmet
              titleTemplate="%s"
              defaultTitle="ClickIPO Admin"
              meta={[
              { name: 'description', content: 'ClickIPO Admin' },
              ]}
            />
            <LoaderSpinner />
            <div className="outer animation slideInTop in-view">
              <Header />
              { children }
              <div className="overlay"></div>
            </div>
          </div>
        </MuiThemeProvider>
      </div>
    );
  }
}

Wrapper.propTypes = {
  children: PropTypes.any,
  login: PropTypes.bool,
};
