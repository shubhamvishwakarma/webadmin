/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Switch, Route, withRouter, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import injectTapEventPlugin from 'react-tap-event-plugin';

import Login from 'containers/Login/Loadable';
import PasswordRecovery from 'containers/PasswordRecovery/Loadable';
import ResetPassword from 'containers/ResetPassword/Loadable';
import IntactFileUpload from 'containers/Admin/IntactFile/index';
import AdminUsers from 'containers/Admin/Users/Loadable';
import Allocations from 'containers/Admin/Allocations/Loadable';
import Messages from 'containers/Admin/Messages/Loadable';

import OfferingsAdmin from 'containers/Admin/Offerings/Loadable';
import  AddNewAdmin from 'containers/Admin/AddAdmin/Loadable';
import ConnectedCustomer from 'containers/Admin/ConnectedCustomer/Loadable';


import { makeSelectCurrentUser } from './selectors';
import Wrapper from './Wrapper';

injectTapEventPlugin();

class App extends React.PureComponent {

  render() {
    const { user } = this.props;
    if (user.token === null) {
      return (
        <Wrapper login>
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/password-recovery" component={PasswordRecovery} />
            <Route exact path="/reset-password" component={ResetPassword} />
            <Route render={() => (<Redirect to="/login" />)} />
          </Switch>
        </Wrapper>
      );
    } else if (user.role === 'super_admin') {
      return (
        <Wrapper>
          <Switch>
            <Route path="/users" component={AdminUsers} />
            <Route path="/users/create_broker_dealer" component={AdminUsers} />
            <Route path="/allocations" component={Allocations} />
            <Route path="/allocations/create_allocations" component={Allocations} />
            <Route path="/allocations/details/:id" component={Allocations} />
            <Route path="/admin_offerings" component={OfferingsAdmin} />
            <Route path="/admin_offerings/create" component={OfferingsAdmin} />
            <Route path="/admin_offerings/edit/:id" component={OfferingsAdmin} />
            <Route path="/admin_offerings/orders/:id" component={OfferingsAdmin} />
            <Route path="/admin_messages" component={Messages} />
            <Route path="/intact_file_upload" component={IntactFileUpload}/>
            <Route path="/add_new_admin" component={AddNewAdmin}/>
            <Route path="/add_new_admin/create_broker_admin" component={AddNewAdmin}/>
            <Route path="/connectedcustomer" component={ConnectedCustomer}/>
            <Route render={() => (<Redirect to="/users" />)} />
          </Switch>
        </Wrapper>
      );
    } return (null);
  }
}

App.propTypes = {
  user: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  user: makeSelectCurrentUser(),
});

export default withRouter(connect(mapStateToProps, null)(App));
