import moment from 'moment';
import Cookies from 'universal-cookie';
import { DEBUG_CONFIG } from '../constants';

const cookies = new Cookies();


export function getOfferingPrice(offering) {
  if (offering.status === 'active' && offering.offering_type_name === 'IPO' && offering.offering_price_attributes) {
    return `$${offering.offering_price_attributes.min_price.toFixed(2).replace(/[.,]00$/, '').toLocaleString()} - $${offering.offering_price_attributes.max_price.toFixed(2).replace(/[.,]00$/, '').toLocaleString()}`;
  } else if (offering.status === 'active' && offering.offering_type_name !== 'IPO' && offering.offering_price_attributes) {
    return `$${offering.offering_price_attributes.max_price.toFixed(2).replace(/[.,]00$/, '').toLocaleString()}`;
  } else if (offering.final_price) {
    return `$${offering.final_price.toFixed(2).replace(/[.,]00$/, '').toLocaleString()}`;
  } return '$0';
}

export function validatePrice(price,entry){
  if(price.max==null && price.min==null )
  {
      return false;
  }
  else if( price.min==null && price.max != null)
  {
      let percent=(price.max/100)*20;
      if((entry>price.max-percent) && (entry<price.max+percent))
      {
        return true;
      }
      else{
        return false;
      }
  }
  else{
    let Maxpercent=(price.max/100)*20;
    let MinPercent=(price.min/100)*20;
    if((entry>price.min-MinPercent) && (entry<price.max+Maxpercent))
    {
      return true;
    }
    else{
      return false;
    }
  }
}
export function getOfferingPriceRangeNum(offering) {
  if(offering.offering_type_name === 'IPO'){
    let max=0,min=0;
      if (offering.offering_price_attributes) {
        if(offering.offering_price_attributes.max_price==null)
        {
          max=null
        }
        else{
          max=offering.offering_price_attributes.max_price

        }
        if(offering.offering_price_attributes.min_price==null)
        {
          min=null
        }
        else{
          min= offering.offering_price_attributes.min_price

        }
        return {min,max};
      }

    }
  else
  {
    let min=0;
    if (offering.offering_price_attributes) {
      if(offering.offering_price_attributes.max_price==null)
      {
        return {min:null,max:null}
      }
      else{

         return {min:null,max:offering.offering_price_attributes.max_price}

      }
     }
  }

  return  {min:null,max:null};
}

export function getAllocationPriceRangeNum(allocation) {
    let max=0,min=0;
      if (allocation.anticipated_prices) {
        if(allocation.anticipated_prices.max_price==null)
        {
          max=null
        }
        else{
          max=allocation.anticipated_prices.max_price

        }
        if(allocation.anticipated_prices.min_price==null)
        {
          min=null
        }
        else{
          min= allocation.anticipated_prices.min_price

        }
        return {min,max};
      }

}

export function getAllocationUpdatePriceRangeNum(allocation) {
    let max=0,min=0;
      if (allocation.offering_price_attributes) {
        if(allocation.offering_price_attributes.max_price==null)
        {
          max=null
        }
        else{
          max=allocation.offering_price_attributes.max_price

        }
        if(allocation.offering_price_attributes.min_price==null)
        {
          min=null
        }
        else{
          min= allocation.offering_price_attributes.min_price

        }
        return {min,max};
      }

}

export function getcreateAllocationPriceRangeNum(allocation) {
    let max=0,min=0;
      if (allocation) {
        if(allocation.max_price==null)
        {
          max=null
        }
        else{
          max=allocation.max_price

        }
        if(allocation.min_price==null)
        {
          min=null
        }
        else{
          min= allocation.min_price

        }
        return {min,max};
      }

}
export function getOfferingPriceRange(offering) {
  if(offering.offering_type_name === 'IPO'){
    let max="",min="";
      if (offering.offering_price_attributes) {
        if(offering.offering_price_attributes.max_price==null || offering.offering_price_attributes.max_price<1)
        {
          max="TBD"
        }
        else{
          max= `$ ${offering.offering_price_attributes.max_price }`

        }
        if(offering.offering_price_attributes.min_price==null || offering.offering_price_attributes.min_price<1)
        {
          min="TBD"
        }
        else{
          min= `$ ${offering.offering_price_attributes.min_price}`

        }
        return `${min} - ${max}`;
      }

    }
  else
  {
    if (offering.offering_price_attributes) {
      if(offering.offering_price_attributes.max_price==null ||  offering.offering_price_attributes.max_price<1)
      {
        return `TBD`
      }
      else{

         return `$ ${offering.offering_price_attributes.max_price}`;

      }
     }
  }

  return '$0';
}

export function getPriceText(offering) {
  if (offering.offering_type_name === 'Secondary' && offering.status === 'active') {
    return 'Last Closing Price';
  } else if (offering.offering_type_name === 'IPO' && offering.status === 'active') {
    return 'Price Range';
  } return 'Final Price';
}

export function getDate(offering) {
  if (offering.anticipated_date!==null ) {
    return parseDate(offering.anticipated_date);
  } else if (offering.status === 'closed' && offering.effective_date!==null) {
    return mmddyy(offering.effective_date);
  }

  return 'TBD';
}


export function parseDate(date) {
  if (!date) return null;
  const dateStr = moment(date);
  const dateComponent = dateStr.utc().format(('MMM D, YYYY'));
  return dateComponent;
}

export function getPriceofActiveOffering(item) {
  if (item.offering_type === 'IPO' && item.offering_price_attributes) {
    return `Range: $${item.offering_price_attributes.min_price} - $${item.offering_price_attributes.max_price}`;
  } else if (item.offering_type !== 'IPO' && item.offering_price_attributes) {
    return `$${item.offering_price_attributes.max_price}`;
  } return '$0';
}

export function currency(num) {
  return parseFloat(Math.round(num * 100) / 100).toFixed(2);
}

export function convertToLocalString(item, value) {
  return item[value] ? parseInt(item[value], 10).toLocaleString() : 0;
}

export function mmddyy(value) {
  const dateStr = moment(value);
  const dateComponent = dateStr.format(('MM/DD/YYYY'));
  return dateComponent;
}

export function parseTime(value) {
  const dateStr = moment(value);
  const dateComponent = dateStr.format(('hh:mm a'));
  return dateComponent;
}

export function nFormatter(num) {
  if (num >= 1000000000) {
    return `${(num / 1000000000).toFixed(1).replace(/\.0$/, '')}G`;
  }
  if (num >= 1000000) {
    return `${(num / 1000000).toFixed(1).replace(/\.0$/, '')}M`;
  }
  if (num >= 1000) {
    return `${(num / 1000).toFixed(1).replace(/\.0$/, '')}K`;
  }
  return num;
}

export function logout() {
  cookies.remove('currentUser',{ path: '/' });
  cookies.remove('email',{ path: '/' });
  cookies.remove('role',{ path: '/' });
  window.location.href = '/login';
}

export function LogStaging(input) {
  if ( DEBUG_CONFIG == 1 ) {
    console.log(input);
  }
}
