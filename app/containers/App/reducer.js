/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { fromJS } from 'immutable';

import { SUBMIT_LOGIN_FORM_SUCCESS, SUBMIT_LOGIN_FORM_FAILURE, SUBMIT_LOGIN_FORM } from 'containers/Login/constants';
import { SUBMIT_RECOVERY_FORM_FAILURE, SUBMIT_RECOVERY_FORM_SUCCESS } from 'containers/PasswordRecovery/constants';
import { SET_RESET_TOKEN, SUBMIT_RESET_FORM_FAILURE, SUBMIT_RESET_FORM_SUCCESS } from 'containers/ResetPassword/constants';
import {
  OPEN_GLOBAL_LOADER,
  CLOSE_GLOBAL_LOADER,
} from './constants';
import Cookies from 'universal-cookie';
const cookies = new Cookies();



const userToken = cookies.get('currentUser') ? cookies.get('currentUser') : null;
const userEmail = cookies.get('email') ? cookies.get('email') : null;
const userRole = cookies.get('role') ? cookies.get('role') : null;

// const userToken = localStorage.getItem('currentUser') ? localStorage.getItem('currentUser') : null;
// const userEmail = localStorage.getItem('email') ? localStorage.getItem('email') : null;
// const userRole = localStorage.getItem('role') ? localStorage.getItem('role') : null;
const initialState = fromJS({
  currentUser: {
    token: userToken,
    email: userEmail,
    role: userRole,
  },
  loginError: null,
  passwordRecoveryMessage: null,
  passwordResetToken: null,
  resetPasswordErrorMessage: null,
  showLoader: false,
});


function appReducer(state = initialState, action) {
  switch (action.type) {
    case SUBMIT_LOGIN_FORM:
      return state.setIn(['currentUser', 'email'], action.data.email);
    case SUBMIT_LOGIN_FORM_SUCCESS:
      return state.setIn(['currentUser', 'token'], action.data.token).set('loginError', null).set('passwordRecoveryMessage', null).setIn(['currentUser', 'role'], action.data.role_name);
    case SUBMIT_LOGIN_FORM_FAILURE:
      return state.set('loginError', action.data).set('passwordRecoveryMessage', null);
    case SUBMIT_RECOVERY_FORM_FAILURE:
    case SUBMIT_RECOVERY_FORM_SUCCESS:
      return state.set('passwordRecoveryMessage', action.message);
    case SET_RESET_TOKEN:
      return state.set('SET_RESET_TOKEN', action.token);
    case SUBMIT_RESET_FORM_FAILURE:
      return state.set('resetPasswordErrorMessage', action.message);
    case SUBMIT_RESET_FORM_SUCCESS:
      return state.set('resetPasswordErrorMessage', null);
    case OPEN_GLOBAL_LOADER:
      return state.set('showLoader', true);
    case CLOSE_GLOBAL_LOADER:
      return state.set('showLoader', false);
    default:
      return state;
  }
}


export default appReducer;
