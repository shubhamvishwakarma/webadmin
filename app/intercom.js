import { INTERCOM_ID } from 'containers/App/constants';

let updated = false;

const intercomBoot = (currentUser) => {
  const user = currentUser;
  window.Intercom('boot', {
    app_id: INTERCOM_ID,
  });

  if (typeof document === 'undefined') return;

  if (user.email && !updated) {
    updated = true;
    window.Intercom('update', {
      email: user.email,
    });
  }
};

const intercomShutdown = () => {
  window.Intercom('shutdown');
};

export {
  intercomBoot,
  intercomShutdown,
};
